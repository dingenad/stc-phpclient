# LastSeatsGeneralObjectsEventCategoryInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_id** | **int** |  | [optional] 
**category_id** | **int** |  | [optional] 
**category_key** | **string** |  | [optional] 
**category_name** | **string** |  | [optional] 
**sub_category_id** | **int** |  | [optional] 
**sub_category_key** | **string** |  | [optional] 
**sub_category_name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

