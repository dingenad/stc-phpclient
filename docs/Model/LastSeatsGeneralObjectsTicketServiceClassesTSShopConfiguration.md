# LastSeatsGeneralObjectsTicketServiceClassesTSShopConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_key** | **string** |  | [optional] 
**theme** | **string** |  | [optional] 
**ticket_amount_type** | **string** |  | [optional] 
**basket_shop_type** | **string** |  | [optional] 
**show_regular_price** | **bool** |  | [optional] 
**styling_v4** | **bool** |  | [optional] 
**show_total_saved** | **bool** |  | [optional] 
**form_phone_number** | **bool** |  | [optional] 
**discount_code_collapsed** | **bool** |  | [optional] 
**google_tag_manager_id** | **string** |  | [optional] 
**gtm_enhanced_ecommerce** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

