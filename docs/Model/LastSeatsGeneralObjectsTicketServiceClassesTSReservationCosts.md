# LastSeatsGeneralObjectsTicketServiceClassesTSReservationCosts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fixed_amount** | **double** | A fixed amount, if applicable&lt;br /&gt;  Either &lt;strong&gt;FixedAmount&lt;/strong&gt; or &lt;strong&gt;Percentage&lt;/strong&gt; contains a value. &lt;strong&gt;MinAmount&lt;/strong&gt; and &lt;strong&gt;MaxAmount&lt;/strong&gt; can only contain a value if &lt;strong&gt;Percentage&lt;/strong&gt; applies. | [optional] 
**percentage** | **double** | A percentage, if applicable&lt;br /&gt;  Either &lt;strong&gt;FixedAmount&lt;/strong&gt; or &lt;strong&gt;Percentage&lt;/strong&gt; contains a value. &lt;strong&gt;MinAmount&lt;/strong&gt; and &lt;strong&gt;MaxAmount&lt;/strong&gt; can only contain a value if &lt;strong&gt;Percentage&lt;/strong&gt; applies. | [optional] 
**min_amount** | **double** | When a percentage applies, this value contains the minimum amount (&lt;code&gt;null&lt;/code&gt; if no minimum applies) | [optional] 
**max_amount** | **double** | When a percentage applies, this value contains the maximum amount (&lt;code&gt;null&lt;/code&gt; if no maximum applies) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

