# LastSeatsGeneralObjectsSubscriptionProductDynamicField

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The Id of the dynamic field. | [optional] 
**product_id** | **int** | The Id of the product this field belongs to. | [optional] 
**dynamic_field_id** | **int** | The dynamic field this product dynamic field refers to. | [optional] 
**sort_order** | **int** | The sort order of the field in the product. | [optional] 
**criteria** | **string** | The criteria. | [optional] 
**dynamic_field** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDynamicField**](LastSeatsGeneralObjectsSubscriptionDynamicField.md) |  | [optional] 
**required** | **bool** | Is the field required | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

