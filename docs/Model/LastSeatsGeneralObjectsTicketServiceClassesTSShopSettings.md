# LastSeatsGeneralObjectsTicketServiceClassesTSShopSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**google_tag_manager_container_id** | **string** |  | [optional] 
**google_analytics_tracking_id** | **string** |  | [optional] 
**show_calendar** | **bool** |  | [optional] 
**calendar_type** | **int** |  | [optional] 
**show_calendar_timeslot** | **bool** |  | [optional] 
**calendar_timeslot_type** | **int** |  | [optional] 
**enable_discount_code** | **bool** |  | [optional] 
**discount_code_type** | **int** |  | [optional] 
**show_valid_to_in_order_lines** | **bool** |  | [optional] 
**valid_to_in_order_lines_type** | **int** |  | [optional] 
**show_from_column_in_order_lines** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

