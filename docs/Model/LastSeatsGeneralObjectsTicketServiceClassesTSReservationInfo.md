# LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reservation_key** | **string** | The key of the reservation that must be used for further communication about this reservation | [optional] 
**nr_of_tickets** | **int** | The amount of tickets | [optional] 
**contact_key** | **string** | The key of the contact that is connected to this reservation | [optional] 
**affiliate** | **string** | The affiliate code for the reservation quote | [optional] 
**reservation_number** | **string** | The number of a reservation | [optional] 
**currency** | **string** | The currency used for the price and amounts | [optional] 
**external_reservation_number** | **string** | The external number of a reservation | [optional] 
**currency_code** | **string** | The currency code used for the price and amounts | [optional] 
**basket_key** | **string** | The key of the basket where this reservation is part of | [optional] 
**total_amount** | **double** | The total amount of the reservation quote | [optional] 
**status** | **string** | The status of a reservation | [optional] 
**ticket_amount** | **double** | The amount of the ticket | [optional] 
**reservation_date** | [**\DateTime**](\DateTime.md) | The date of the reservation | [optional] 
**original_ticket_amount** | **double** | The original ticket amount | [optional] 
**confirmed_date** | [**\DateTime**](\DateTime.md) | The date this reservation was confirmed | [optional] 
**reservation_costs_per_ticket** | **double** | The reservation costs per ticket (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**tickets_url** | **string** | The url to a file where the tickets can be downloaded | [optional] 
**reservation_costs_per_transaction** | **double** | The reservation costs per transaction (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**payment_method** | **string** | &lt;code&gt;true&lt;/code&gt; if paid; otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**payment_costs** | **double** | The reservation costs per transaction (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**is_paid** | **bool** | &lt;code&gt;true&lt;/code&gt; if paid; otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**discount** | **double** | The discount for this reservation quote (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**is_confirmed** | **bool** | &lt;code&gt;true&lt;/code&gt; if confirmed; otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**discount_title** | **string** | The discount that is applied for this reservation quote (&lt;code&gt;null&lt;/code&gt; if unknown)&lt;br /&gt;  Set when discountcode is applied and discount &amp;gt; 0 | [optional] 
**is_blocked** | **bool** | &lt;code&gt;true&lt;/code&gt; if blocked; otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**reservation_price_types** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeInfo.md) | A list with all the specific prices that were used when creating the reservation quote | [optional] 
**permission_id** | **int** | The performance section Id for permission. | [optional] 
**reservation_subscriptions** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionTSReservationSubscription[]**](LastSeatsGeneralObjectsSubscriptionTSReservationSubscription.md) | A list with all the subscriptions for this reservation | [optional] 
**performance_section_key** | **string** | The key of the performance section for this reservation | [optional] 
**section_key** | **string** | The key of the section for this reservation | [optional] 
**section_name** | **string** | The name of the section | [optional] 
**extra_info1** | **string** | Optional extra information-1 | [optional] 
**extra_info2** | **string** | Optional extra information-2 | [optional] 
**extra_info3** | **string** | Optional extra information-3 | [optional] 
**seats_together** | **bool** | Indicates for a NEW reservation if seats are next to each other (TRUE) or not (FALSE) or not applicable (&lt;code&gt;null&lt;/code&gt;) | [optional] 
**event_info** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo**](LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo.md) |  | [optional] 
**tickets** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo.md) | List of tickets of this reservation | [optional] 
**visit_date** | [**\DateTime**](\DateTime.md) | The VisitDate of the reservation | [optional] 
**continuation_link** | **string** | The ContinuationLink of the reservation | [optional] 
**total_vat** | **double** | Total amount of VAT | [optional] 
**seat_selection_link** | **string** | The SeatSelectionLink of the reservation | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

