# LastSeatsGeneralObjectsResellerResellerRegistrationResellerProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reseller_product** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSResellerProductInfo**](LastSeatsGeneralObjectsTicketServiceClassesTSResellerProductInfo.md) |  | [optional] 
**number_of_tickets** | **int** |  | [optional] 
**capacity_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**start_time_minutes_after_midnight** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

