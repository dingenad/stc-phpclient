# LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partner_key** | **string** |  | [optional] 
**partner_name** | **string** |  | [optional] 
**event_name** | **string** |  | [optional] 
**performance_name** | **string** |  | [optional] 
**section_name** | **string** |  | [optional] 
**price_type_names** | **string[]** |  | [optional] 
**subscription_template_name** | **string** |  | [optional] 
**product_name** | **string** |  | [optional] 
**claim_type** | **string** |  | [optional] 
**enable_kiosk_mode** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

