# LastSeatsGeneralObjectsSubscriptionMessagesSelectedSubscriptionTemplateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**template_id** | **int** | The Id of the subscription template. | [optional] 
**quantity** | **int** | The number of subscriptions of this template the user wants to have. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

