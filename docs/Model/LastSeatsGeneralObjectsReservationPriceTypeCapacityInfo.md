# LastSeatsGeneralObjectsReservationPriceTypeCapacityInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**reservation_price_type_id** | **int** |  | [optional] 
**capacity_id** | **int** |  | [optional] 
**nr_of_seats** | **int** |  | [optional] 
**date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**start_time_minutes_after_midnight** | **int** |  | [optional] 
**end_time_minutes_after_midnight** | **int** |  | [optional] 
**description** | **string** |  | [optional] 
**characteristic** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

