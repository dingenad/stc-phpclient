# LastSeatsGeneralObjectsTriggerTriggerSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**sales_channel_performance_section_id** | **int** |  | [optional] 
**subscription_template_id** | **int** |  | [optional] 
**trigger_timing_type** | **string** |  | [optional] 
**trigger_moment_type** | **string** |  | [optional] 
**trigger_type** | **string** |  | [optional] 
**trigger_moment** | **int** |  | [optional] 
**trigger_info1** | **string** |  | [optional] 
**trigger_info2** | **string** |  | [optional] 
**trigger_info3** | **string** |  | [optional] 
**trigger_info4** | **string** |  | [optional] 
**trigger_info5** | **string** |  | [optional] 
**trigger_info6** | **string** |  | [optional] 
**trigger_info7** | **string** |  | [optional] 
**last_updated_by** | **int** |  | [optional] 
**last_updated_on** | [**\DateTime**](\DateTime.md) |  | [optional] 
**created_on** | [**\DateTime**](\DateTime.md) |  | [optional] 
**disabled** | **bool** |  | [optional] 
**name** | **string** |  | [optional] 
**timing** | **string** |  | [optional] 
**mail_template** | **string** |  | [optional] 
**mail_address** | **string** |  | [optional] 
**url_url** | **string** |  | [optional] 
**url_user_id** | **string** |  | [optional] 
**url_password** | **string** |  | [optional] 
**url_body** | **string** |  | [optional] 
**url_footer** | **string** |  | [optional] 
**url_content_type** | **string** |  | [optional] 
**sanoma_sim_gate** | **string** |  | [optional] 
**sanoma_sim_extra_fields** | **string** |  | [optional] 
**tripolis_contact_group_id** | **string** |  | [optional] 
**tripolis_extra_fields** | **string** |  | [optional] 
**check_news_letter_preference** | **bool** |  | [optional] 
**event_id** | **int** |  | [optional] 
**sales_channel_id** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

