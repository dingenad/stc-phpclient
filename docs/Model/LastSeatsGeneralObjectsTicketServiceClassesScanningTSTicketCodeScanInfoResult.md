# LastSeatsGeneralObjectsTicketServiceClassesScanningTSTicketCodeScanInfoResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ticket_code_scan_info_list** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSTicketCodeScanInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesScanningTSTicketCodeScanInfo.md) |  | [optional] 
**remaining_records** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

