# LastSeatsGeneralObjectsTicketServiceClassesTSInvitationCodeDiscountInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**discount_code** | **string** |  | [optional] 
**minimum_discount_codes** | **int** |  | [optional] 
**maximum_discount_codes** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

