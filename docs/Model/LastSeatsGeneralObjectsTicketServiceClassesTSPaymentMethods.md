# LastSeatsGeneralObjectsTicketServiceClassesTSPaymentMethods

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_methods** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPaymentMethod[]**](LastSeatsGeneralObjectsTicketServiceClassesTSPaymentMethod.md) | List of payment methods (credit card, iDeal, etc.) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

