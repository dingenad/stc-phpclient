# LastSeatsGeneralObjectsTicketServiceClassesTSInvitationInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **string** |  | [optional] 
**partner_key** | **string** |  | [optional] 
**event_key** | **string** |  | [optional] 
**performance_key** | **string** |  | [optional] 
**performance_section_key** | **string** |  | [optional] 
**price_type_key** | **string** |  | [optional] 
**discount_amount_per_ticket** | **double** |  | [optional] 
**discount_percentage_per_ticket** | **double** |  | [optional] 
**valid_from_amount** | **int** |  | [optional] 
**valid_from_operator** | **string** |  | [optional] 
**minimum_tickets_invitation_code** | **int** |  | [optional] 
**maximum_tickets_invitation_code** | **int** |  | [optional] 
**invitation_code_discount_settings** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSInvitationCodeDiscountInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSInvitationCodeDiscountInfo.md) |  | [optional] 
**minimum_invitation_codes** | **int** |  | [optional] 
**maximum_invitation_codes** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

