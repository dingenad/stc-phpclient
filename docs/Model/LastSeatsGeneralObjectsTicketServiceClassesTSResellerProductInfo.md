# LastSeatsGeneralObjectsTicketServiceClassesTSResellerProductInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reseller_product_key** | **string** |  | [optional] 
**price_type_name** | **string** |  | [optional] 
**buying_price** | **double** |  | [optional] 
**selling_price** | **double** |  | [optional] 
**event_name** | **string** |  | [optional] 
**performance_name** | **string** |  | [optional] 
**performance_section_key** | **string** |  | [optional] 
**performance_section_name** | **string** |  | [optional] 
**reseller_volume_discount_info_list** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSResellerVolumeDiscountInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSResellerVolumeDiscountInfo.md) |  | [optional] 
**available_seats** | **int** |  | [optional] 
**available_seats_in_section** | **int** |  | [optional] 
**ticket_valid_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**ticket_valid_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**minimum_amount** | **int** |  | [optional] 
**maximum_amount** | **int** |  | [optional] 
**step** | **int** |  | [optional] 
**requires_capacity_slot** | **bool** |  | [optional] 
**sales_channel_performance_section_price_type_key** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

