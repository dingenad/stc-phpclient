# LastSeatsGeneralObjectsTicketServiceClassesTSConfigurationInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partner_name** | **string** |  | [optional] 
**event_name** | **string** |  | [optional] 
**performance_name** | **string** |  | [optional] 
**performance_section_name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

