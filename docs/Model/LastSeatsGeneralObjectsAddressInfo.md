# LastSeatsGeneralObjectsAddressInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**address_key** | **string** |  | [optional] 
**street** | **string** |  | [optional] 
**number** | **string** |  | [optional] 
**extra_address_line** | **string** |  | [optional] 
**postal_code** | **string** |  | [optional] 
**city_id** | **int** |  | [optional] 
**city_name** | **string** |  | [optional] 
**state_id** | **int** |  | [optional] 
**state_name** | **string** |  | [optional] 
**country_id** | **int** |  | [optional] 
**country_name** | **string** |  | [optional] 
**lat** | **double** |  | [optional] 
**lon** | **double** |  | [optional] 
**last_updated_by** | **int** |  | [optional] 
**last_updated_on** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

