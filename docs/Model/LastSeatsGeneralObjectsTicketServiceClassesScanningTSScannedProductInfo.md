# LastSeatsGeneralObjectsTicketServiceClassesScanningTSScannedProductInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**valid_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**valid_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**last_claim_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**currency_code** | **string** |  | [optional] 
**price** | **double** |  | [optional] 
**sales_channel_name** | **string** |  | [optional] 
**scanning_display_message** | **string** |  | [optional] 
**ticket_info** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScannedTicketInfo**](LastSeatsGeneralObjectsTicketServiceClassesScanningTSScannedTicketInfo.md) |  | [optional] 
**subscription_info** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScannedSubscriptionInfo**](LastSeatsGeneralObjectsTicketServiceClassesScanningTSScannedSubscriptionInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

