# LastSeatsGeneralObjectsTicketServiceClassesTSDiscountInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**remaining_tickets** | **int** |  | [optional] 
**discount_valid_from_operator** | **string** |  | [optional] 
**discount_valid_from_amount** | **int** |  | [optional] 
**discount_applies_from_operator** | **string** |  | [optional] 
**discount_applies_from_amount** | **int** |  | [optional] 
**discount_ticket_validities** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSDiscountTicketValidityInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSDiscountTicketValidityInfo.md) |  | [optional] 
**discount_partners** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPartnerInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPartnerInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

