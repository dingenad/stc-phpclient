# LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPriceInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price_key** | **string** |  | [optional] 
**discount_price** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

