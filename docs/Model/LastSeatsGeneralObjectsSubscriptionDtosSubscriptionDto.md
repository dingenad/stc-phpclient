# LastSeatsGeneralObjectsSubscriptionDtosSubscriptionDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The Id of the subscription. | [optional] 
**template_id** | **int** | The subscription template Id. | [optional] 
**valid_to** | [**\DateTime**](\DateTime.md) | The valid to date of the subscription. | [optional] 
**log** | **string** | The logbook filled in by a cashier. | [optional] 
**is_active** | **bool** | &lt;code&gt;true&lt;/code&gt; if this subscription is active; otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**subscription_products** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDtosSubscriptionProductDto[]**](LastSeatsGeneralObjectsSubscriptionDtosSubscriptionProductDto.md) | The products of the subscription. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

