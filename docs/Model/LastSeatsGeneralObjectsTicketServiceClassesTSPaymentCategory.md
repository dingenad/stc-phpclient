# LastSeatsGeneralObjectsTicketServiceClassesTSPaymentCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **string** | Enum used to identify category | [optional] 
**name** | **string** | Displayable category name | [optional] 
**image_url** | **string** | Url of category image | [optional] 
**sort_order** | **int** | Desired position when displayed among other payment categories | [optional] 
**payment_methods** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPaymentMethod[]**](LastSeatsGeneralObjectsTicketServiceClassesTSPaymentMethod.md) | List of payment methods that belong to this category | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

