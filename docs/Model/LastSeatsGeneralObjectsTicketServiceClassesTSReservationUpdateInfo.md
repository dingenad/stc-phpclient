# LastSeatsGeneralObjectsTicketServiceClassesTSReservationUpdateInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**external_reservation_number** | **string** | The external number of a reservation | [optional] 
**affiliate** | **string** | The name of the section | [optional] 
**extra_info1** | **string** | Optional extra information-1 | [optional] 
**extra_info2** | **string** | Optional extra information-2 | [optional] 
**extra_info3** | **string** | Optional extra information-3 | [optional] 
**ip_address** | **string** | Optional extra information-3 | [optional] 
**total_amount** | **double** | The total amount of the reservation quote | [optional] 
**reservation_costs_per_ticket** | **double** | The reservation costs per ticket (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**reservation_costs_per_transaction** | **double** | The reservation costs per transaction (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**payment_costs** | **double** | The reservation costs per transaction (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**discount** | **double** | The discount for this reservation quote (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**discount_title** | **string** | The discount that is applied for this reservation quote (&lt;code&gt;null&lt;/code&gt; if unknown)&lt;br /&gt;  Set when discountcode is applied and discount &amp;gt; 0 | [optional] 
**visit_date** | [**\DateTime**](\DateTime.md) | The visit date saved with the reservation | [optional] 
**continuation_link** | **string** | The continuation link saved with the reservation | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

