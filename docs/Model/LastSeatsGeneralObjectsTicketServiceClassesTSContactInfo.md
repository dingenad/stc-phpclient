# LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Contact ID. | [optional] 
**contact_key** | **string** | The key of the contact that must be used for further communication. | [optional] 
**company_name** | **string** | The name of the company | [optional] 
**vat_number** | **string** | The VAT number | [optional] 
**gender** | **string** | The gender (&#x27;M&#x27;, male or &#x27;F&#x27;, female) | [optional] 
**first_name** | **string** | The first name | [optional] 
**middle** | **string** | The middle name | [optional] 
**last_name** | **string** | The last name | [optional] 
**birth_date** | [**\DateTime**](\DateTime.md) | The date of birth | [optional] 
**phone_number** | **string** | The phone number | [optional] 
**mobile_number** | **string** | The mobile phone number | [optional] 
**mail_address** | **string** | The e-mail address | [optional] 
**receive_newsletter** | **bool** | &lt;code&gt;true&lt;/code&gt; when the contact wants to receive a newsletter; otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**receive_invoice** | **bool** | &lt;code&gt;true&lt;/code&gt; when the contact wants to receive the invoice; otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**optin1** | **bool** | &lt;code&gt;true&lt;/code&gt; when the contact opts in for custom option 1; otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**optin2** | **bool** | &lt;code&gt;true&lt;/code&gt; when the contact opts in for custom option 2; otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**optin3** | **bool** | &lt;code&gt;true&lt;/code&gt; when the contact opts in for custom option 3; otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**password** | **string** | The password (only if an account must be created) | [optional] 
**address** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSAddressInfo**](LastSeatsGeneralObjectsTicketServiceClassesTSAddressInfo.md) |  | [optional] 
**full_name** | **string** | The full name of a contact | [optional] 
**language_code** | **string** | The languagecode of a contact | [optional] 
**receive_passbooks** | **bool** | &lt;code&gt;true&lt;/code&gt; if the contact wants to receive Passbook tickets, otherwise &lt;code&gt;false&lt;/code&gt; | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

