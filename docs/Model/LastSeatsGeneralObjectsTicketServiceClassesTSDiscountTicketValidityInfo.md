# LastSeatsGeneralObjectsTicketServiceClassesTSDiscountTicketValidityInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**valid_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**valid_to** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

