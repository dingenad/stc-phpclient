# LastSeatsGeneralObjectsSubscriptionDynamicFieldOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The id of the option | [optional] 
**dynamic_field_id** | **int** | The dynamic field this option belongs to. | [optional] 
**sort_order** | **int** | The sort order this option is in the dynamic field. | [optional] 
**name** | **string** | The name / display value of the option. | [optional] 
**value** | **string** | The technical value. | [optional] 
**description** | **string** | The description of the value. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

