# LastSeatsGeneralObjectsSubscriptionMessagesCountriesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**countries** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSCountryInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSCountryInfo.md) | The countries that can be used when creating a address | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

