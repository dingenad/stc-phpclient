# LastSeatsGeneralObjectsTicketServiceClassesTSCountryInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The Id of the country | [optional] 
**name** | **string** | The translated name of the country | [optional] 
**zipcode_regex** | **string** | The regex to check the zipcode for | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

