# LastSeatsGeneralObjectsSubscriptionSubscriptionProductDynamicFieldValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The Id of the subscription template dynamic field combination. | [optional] 
**subscription_product_id** | **int** | The Id of the subscription product. | [optional] 
**product_dynamic_field_id** | **int** | The Id of the product dynamic field Id. | [optional] 
**string_value** | **string** | The string value (only if the type of the field is string/textarea). | [optional] 
**bool_value** | **bool** | The boolean value (only if the type of the field is boolean). | [optional] 
**date_value** | [**\DateTime**](\DateTime.md) | The date value (only if the type of the field is date). | [optional] 
**numeric_value** | **double** | The numeric value (only if the type of the field is numeric or dropdown). | [optional] 
**product_dynamic_field** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionProductDynamicField**](LastSeatsGeneralObjectsSubscriptionProductDynamicField.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

