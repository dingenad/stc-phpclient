# LastSeatsGeneralObjectsStatisticsMessagesSoldSubscriptionsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sold_subscriptions_info** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsStatisticsDtosTSSoldSubscriptionsInfo[]**](LastSeatsGeneralObjectsStatisticsDtosTSSoldSubscriptionsInfo.md) |  | [optional] 
**offset** | **int** |  | [optional] 
**result_count** | **int** |  | [optional] 
**succeeded** | **bool** | &lt;code&gt;true&lt;/code&gt; when the operation succeeded; otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**error_message** | **string** | The user readable errormessage. | [optional] 
**is_redirect** | **bool** | &lt;code&gt;true&lt;/code&gt; to indicate the user should be redirected; otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**redirect_url** | **string** | The url to redirect the user to. | [optional] 
**display_error** | **bool** | Indicates wether the error in de response should be displayed | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

