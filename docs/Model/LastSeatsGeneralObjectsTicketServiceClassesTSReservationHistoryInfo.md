# LastSeatsGeneralObjectsTicketServiceClassesTSReservationHistoryInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reservation_key** | **string** | The key of the reservation | [optional] 
**action_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**action** | **string** |  | [optional] 
**username** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

