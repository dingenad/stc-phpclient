# LastSeatsGeneralObjectsAccessControlSkiDataStatusMessageDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status_code** | **string** |  | 
**host** | **string** |  | 
**port** | **int** |  | 
**issuer** | **int** |  | 
**receiver** | **int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

