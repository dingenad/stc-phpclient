# LastSeatsGeneralObjectsSubscriptionSubscriptionRenewalTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The Id of the Subscription Template renewal item | [optional] 
**subscription_template_id** | **int** | The subscription template id where this renewal is for | [optional] 
**renewal_subscription_template_id** | **int** | The subscription template id where this subscription template renews to | [optional] 
**renewal_subscription_template_name** | **string** | The subscription template object where this subscription template renews to | [optional] 
**is_default** | **bool** | This is the default renewal template for this subscription template | [optional] 
**sort_order** | **int** | The ordering for this renewal template | [optional] 
**renewal_subscription_template_key** | **string** | The ordering for this renewal template | [optional] 
**cashier_renew** | **bool** | The channel | [optional] 
**online_renew** | **bool** | The channel | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

