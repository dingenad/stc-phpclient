# LastSeatsGeneralObjectsTicketServiceClassesTSQuoteInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nr_of_tickets** | **int** | The amount of tickets | [optional] 
**affiliate** | **string** | The affiliate code for the reservation quote | [optional] 
**currency** | **string** | The currency used for the price and amounts | [optional] 
**currency_code** | **string** | The currency code used for the price and amounts | [optional] 
**total_amount** | **double** | The total amount of the reservation quote | [optional] 
**ticket_amount** | **double** | The amount of the ticket | [optional] 
**original_ticket_amount** | **double** | The original ticket amount | [optional] 
**reservation_costs_per_ticket** | **double** | The reservation costs per ticket (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**reservation_costs_per_transaction** | **double** | The reservation costs per transaction (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**payment_costs** | **double** | The reservation costs per transaction (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**discount** | **double** | The discount for this reservation quote (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**discount_title** | **string** | The discount that is applied for this reservation quote (&lt;code&gt;null&lt;/code&gt; if unknown)&lt;br /&gt;  Set when discountcode is applied and discount &amp;gt; 0 | [optional] 
**reservation_price_types** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeInfo.md) | A list with all the specific prices that were used when creating the reservation quote | [optional] 
**reservation_subscriptions** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionTSReservationSubscription[]**](LastSeatsGeneralObjectsSubscriptionTSReservationSubscription.md) | A list with all the subscriptions for this reservation | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

