# LastSeatsGeneralObjectsSubscriptionDtosPictureCropCoordinates

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**x** | **double** | The X left coordinate. | [optional] 
**y** | **double** | The Y top coordinate. | [optional] 
**x2** | **double** | The X righh coordinate. | [optional] 
**y2** | **double** | The Y bottom coordinate. | [optional] 
**w** | **double** | The width of the crop. Not used. | [optional] 
**h** | **double** | The height of the crop. Not used. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

