# LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPerformanceInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**performance_key** | **string** |  | [optional] 
**remaining_tickets** | **int** |  | [optional] 
**discount_performance_sections** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPerformanceSectionInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPerformanceSectionInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

