# LastSeatsGeneralObjectsReservationPriceTypeInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**reservation_id** | **int** |  | [optional] 
**price_type_id** | **int** |  | [optional] 
**price_type_key** | **string** |  | [optional] 
**vat_percentage** | **string** |  | [optional] 
**sales_channel_performance_section_id** | **int** |  | [optional] 
**sales_channel_performance_section_price_type_id** | **int** |  | [optional] 
**sales_channel_performance_section_price_type_key** | **string** |  | [optional] 
**price_type_name** | **string** |  | [optional] 
**price_type_ticket_name** | **string** |  | [optional] 
**price_type_description** | **string** |  | [optional] 
**external_price_type_id** | **string** |  | [optional] 
**branche_price_id** | **string** |  | [optional] 
**group** | **int** |  | [optional] 
**nr_of_seats** | **int** |  | [optional] 
**original_price** | **double** |  | [optional] 
**price_per_seat** | **double** |  | [optional] 
**pass_number** | **string** |  | [optional] 
**ticket_template** | **string** |  | [optional] 
**ticket_valid_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**ticket_valid_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**reservation_price_type_capacities** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsReservationPriceTypeCapacityInfo[]**](LastSeatsGeneralObjectsReservationPriceTypeCapacityInfo.md) |  | [optional] 
**passbook_template** | **string** |  | [optional] 
**external_price_id** | **string** |  | [optional] 
**price_type_description_external_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

