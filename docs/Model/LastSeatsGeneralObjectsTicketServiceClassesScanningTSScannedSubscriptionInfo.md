# LastSeatsGeneralObjectsTicketServiceClassesScanningTSScannedSubscriptionInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subscription_template_name** | **string** |  | [optional] 
**product_name** | **string** |  | [optional] 
**subscription_product_key** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

