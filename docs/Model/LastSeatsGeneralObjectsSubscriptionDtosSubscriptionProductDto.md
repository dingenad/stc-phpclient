# LastSeatsGeneralObjectsSubscriptionDtosSubscriptionProductDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The Id of the subscription product. | [optional] 
**product_id** | **int** | The Id of the Product (template) | [optional] 
**dynamic_field_values** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionSubscriptionProductDynamicFieldValue[]**](LastSeatsGeneralObjectsSubscriptionSubscriptionProductDynamicFieldValue.md) | The dynamic fields that are filled in by the user for this product. | [optional] 
**form_values** | **map[string,string]** | The subscription form values filled in by the user.&lt;br /&gt;  This is a dictionary of [fieldname, value] | [optional] 
**picture_url** | **string** | If the subscriptionproduct was changed, then this is the old picture from the current contact. | [optional] 
**picture_id** | **string** | The Id of the picture that was uploaded for this product (if applicable). | [optional] 
**picture_crop_points** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDtosPictureCropCoordinates**](LastSeatsGeneralObjectsSubscriptionDtosPictureCropCoordinates.md) |  | [optional] 
**copied_id** | **int** | For internal usage. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

