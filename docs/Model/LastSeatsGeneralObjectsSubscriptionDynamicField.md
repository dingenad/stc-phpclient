# LastSeatsGeneralObjectsSubscriptionDynamicField

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The Id of the dynamic field. | [optional] 
**partner_id** | **int** | The partner Id this field belongs to. | [optional] 
**name** | **string** | The name of the dynamic field. | [optional] 
**dynamic_field_type** | **string** | The type of the dynamic field. Can be one of the following values:  &lt;code&gt;0&lt;/code&gt; &#x3D; String,  &lt;code&gt;1&lt;/code&gt; &#x3D; TextArea,  &lt;code&gt;2&lt;/code&gt; &#x3D; DateTime,  &lt;code&gt;3&lt;/code&gt; &#x3D; Numeric,  &lt;code&gt;4&lt;/code&gt; &#x3D; Boolean,  &lt;code&gt;5&lt;/code&gt; &#x3D; Dropdown | [optional] 
**options** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDynamicFieldOption[]**](LastSeatsGeneralObjectsSubscriptionDynamicFieldOption.md) | The options for this field. Only applies when the type of the field is Dropdown. | [optional] 
**description** | **string** | The description of the dynamic field. | [optional] 
**required** | **bool** | Indicates whether the dynamic field is mandatory. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

