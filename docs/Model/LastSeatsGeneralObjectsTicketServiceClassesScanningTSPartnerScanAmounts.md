# LastSeatsGeneralObjectsTicketServiceClassesScanningTSPartnerScanAmounts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**claim_success_amount** | **int** |  | [optional] 
**claim_failed_amount** | **int** |  | [optional] 
**check_success_amount** | **int** |  | [optional] 
**check_failed_amount** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

