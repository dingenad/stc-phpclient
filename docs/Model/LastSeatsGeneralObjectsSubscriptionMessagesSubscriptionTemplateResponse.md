# LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subscription_templates** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionSubscriptionTemplate[]**](LastSeatsGeneralObjectsSubscriptionSubscriptionTemplate.md) | A list of available subscriptions with the information about these subscriptions. | [optional] 
**holder_form_flags** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDtosFormFlagsSubscriptionHolder**](LastSeatsGeneralObjectsSubscriptionDtosFormFlagsSubscriptionHolder.md) |  | [optional] 
**dynamic_fields** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionSubscriptionTemplateDynamicField[]**](LastSeatsGeneralObjectsSubscriptionSubscriptionTemplateDynamicField.md) | The merged dynamic fields of the chosen subscription templates.&lt;br /&gt;  Dynamic fields are shown to the user and need to be filled in. | [optional] 
**succeeded** | **bool** | &lt;code&gt;true&lt;/code&gt; when the request succeeded; otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**error_message** | **string** | The error message when the request failed | [optional] 
**is_redirect** | **bool** | &lt;code&gt;true&lt;/code&gt; to indicate the user should be redirected to the redirecturl; otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**redirect_url** | **string** | The url the user should be redirected to. This is generally used to redirect the user to a payment provider. | [optional] 
**discount_pricing** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDtosDiscountPricingDto[]**](LastSeatsGeneralObjectsSubscriptionDtosDiscountPricingDto.md) | The list of discounts and amounts. | [optional] 
**reservation_costs_amount** | **double** | The reservation costs in the currency of the subscription template. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

