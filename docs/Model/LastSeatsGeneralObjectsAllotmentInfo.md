# LastSeatsGeneralObjectsAllotmentInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**performance_section_id** | **int** |  | [optional] 
**allotment_nr** | **int** |  | [optional] 
**row_number** | **string** |  | [optional] 
**seat_number** | **string** |  | [optional] 
**ticket_code** | **string** |  | [optional] 
**blocked** | **bool** |  | [optional] 
**reserved** | **bool** |  | [optional] 
**reservation_id** | **int** |  | [optional] 
**max_claim_amount** | **int** |  | [optional] 
**cached_claim_amount** | **int** |  | [optional] 
**last_claim_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**reservation_number** | **string** |  | [optional] 
**first_name** | **string** |  | [optional] 
**middle** | **string** |  | [optional] 
**last_name** | **string** |  | [optional] 
**mail_address** | **string** |  | [optional] 
**phone_number** | **string** |  | [optional] 
**mobile_number** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

