# LastSeatsGeneralObjectsTicketCodeResellerTicketCodeDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**ticket_code** | **string** |  | [optional] 
**event_name** | **string** |  | [optional] 
**performance_name** | **string** |  | [optional] 
**section_name** | **string** |  | [optional] 
**price_type_name** | **string** |  | [optional] 
**download_count** | **int** |  | [optional] 
**cancellable** | **bool** |  | [optional] 
**blocked** | **bool** |  | [optional] 
**reservation_id** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

