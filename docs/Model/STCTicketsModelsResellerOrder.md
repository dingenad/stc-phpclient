# STCTicketsModelsResellerOrder

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**basket_key** | **string** |  | [optional] 
**order_number** | **string** |  | [optional] 
**order_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**num_tickets** | **int** |  | [optional] 
**total_amount** | **double** |  | [optional] 
**tickets_download_url** | **string** |  | [optional] 
**invoice_download_url** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

