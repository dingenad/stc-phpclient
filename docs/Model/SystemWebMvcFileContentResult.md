# SystemWebMvcFileContentResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file_contents** | **string** |  | [optional] 
**content_type** | **string** |  | [optional] 
**file_download_name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

