# STCTicketsApiModelsCreateReservationModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price_type_list** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeRequestInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeRequestInfo.md) |  | [optional] 
**discount_code** | **string** |  | [optional] 
**discount_codes** | **string[]** |  | [optional] 
**affiliate** | **string** |  | [optional] 
**external_reservation_number** | **string** |  | [optional] 
**extra_info1** | **string** |  | [optional] 
**extra_info2** | **string** |  | [optional] 
**extra_info3** | **string** |  | [optional] 
**ip_address** | **string** |  | [optional] 
**visit_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**continuation_link** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

