# LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**review_key** | **string** | The unique key for this review | [optional] 
**rating** | **int** | The rating of this review | [optional] 
**review** | **string** | The text of this review | [optional] 
**review_date** | [**\DateTime**](\DateTime.md) | The date on which this review is entered | [optional] 
**review_contact_key** | **string** | The key of the contact that supplied this review (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**review_contact_name** | **string** | The full name of the contact that supplied this review (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**review_contact_mail_address** | **string** | The mail address of the contact that supplied this review (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**review_response** | **string** | The response that was supplied by an administrator on this review (&lt;code&gt;null&lt;/code&gt; if not available) | [optional] 
**review_response_date** | [**\DateTime**](\DateTime.md) | The date on which this response was supplied (&lt;code&gt;null&lt;/code&gt; if not available) | [optional] 
**review_response_contact_key** | **string** | The key of the contact that supplied this response (&lt;code&gt;null&lt;/code&gt; if not available) | [optional] 
**review_response_contact_name** | **string** | The full name of the contact that supplied this response (&lt;code&gt;null&lt;/code&gt; if not available) | [optional] 
**review_response_contact_mail_address** | **string** | The mail address of the contact that supplied this response (&lt;code&gt;null&lt;/code&gt; if not available) | [optional] 
**reservation_key** | **string** | The key of the reservation where the contact supplied his review for (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**reservation_number** | **string** | The number of the reservation (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**event_key** | **string** | The key of the event that this review was supplied for (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**parent_event_key** | **string** | The key of the parent event that this review was supplied for (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**event_name** | **string** | The name of the event that this review was supplied for (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**performer** | **string** | The name of the performer that this review was supplied for (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**performance_key** | **string** | The key of the performance (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**performance_date** | [**\DateTime**](\DateTime.md) | The date/time of the performance (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**performance_name** | **string** | The name of the performance (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**location_key** | **string** | The key of the location that this review was supplied for (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**location_name** | **string** | The name of the location (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 
**city_name** | **string** | The city name of the location (&lt;code&gt;null&lt;/code&gt; if unknown) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

