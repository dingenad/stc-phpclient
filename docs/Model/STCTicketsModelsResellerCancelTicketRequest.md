# STCTicketsModelsResellerCancelTicketRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ticket_code_ids** | **int[]** |  | [optional] 
**basket_key** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

