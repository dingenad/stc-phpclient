# LastSeatsGeneralObjectsTicketServiceClassesTSBasketSubscriptionReservationsInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**basket_key** | **string** | The unique key of this basket | [optional] 
**currency_code** | **string** | The currency that this basket must be paid in | [optional] 
**basket_number** | **string** | The (customer) number that this basket has | [optional] 
**external_basket_number** | **string** | The external number, as supplied by another system | [optional] 
**status** | **string** | The status of the basket | [optional] 
**confirmed_date** | [**\DateTime**](\DateTime.md) | The date this basket was confirmed | [optional] 
**payment_method** | **string** | The date this basket was confirmed | [optional] 
**reservation_amount** | **double** | The total amount of the reservations in the basket | [optional] 
**total_amount** | **double** | The total &#x27;value&#x27; of the basket (including costs and discount) | [optional] 
**transaction_costs** | **double** | The transaction costs for this basket | [optional] 
**payment_costs** | **double** | The payment costs for the basket | [optional] 
**discount** | **double** | The discount that applies | [optional] 
**reservations** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo.md) | A list with information about the reservations in the basket | [optional] 
**contact_id** | **int** | Optional: contact that is linked to the basket | [optional] 
**reservation_subscriptions** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionTSReservationSubscriptionInfo[]**](LastSeatsGeneralObjectsSubscriptionTSReservationSubscriptionInfo.md) | A list with information about the reservations in the basket | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

