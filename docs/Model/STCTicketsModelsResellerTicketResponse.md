# STCTicketsModelsResellerTicketResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tickets** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketCodeResellerTicketCodeDTO[]**](LastSeatsGeneralObjectsTicketCodeResellerTicketCodeDTO.md) |  | [optional] 
**succeeded** | **bool** |  | [optional] 
**error_message** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

