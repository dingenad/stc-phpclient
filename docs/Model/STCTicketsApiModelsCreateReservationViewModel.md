# STCTicketsApiModelsCreateReservationViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **string** | A composite of the type (T &#x3D; Ticket, S &#x3D; Subscription) and the Id of the item.  When T, the Id is a PriceKey &lt;code&gt;Guid&lt;/code&gt;. Example: &lt;code&gt;TD3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;  When S, the Id is a SubscriptionTemplateKey &lt;code&gt;Guid&lt;/code&gt;. Example: &lt;code&gt;SD3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt; | [optional] 
**previous_reservation_key** | **string** | Applies only to subscriptions. When this value is set, the subscription holder of the previous reservation is also set to the new reservation. | [optional] 
**amount** | **int** | When key type &#x3D; Ticket: number of seats.   When key type &#x3D; Subscription: number of subscriptions. | [optional] 
**capacity_date** | [**\DateTime**](\DateTime.md) | When key type &#x3D; Ticket: Date when this ticket can be used when capacity slots are used  When key type &#x3D; Subscription: No use | [optional] 
**capacity_time** | **int** | When key type &#x3D; Ticket: Time (in minutes after midnight) when this ticket can be used when capacity slots are used  When key type &#x3D; Subscription: No use | [optional] 
**ticket_codes** | **string[]** | When key type &#x3D; Ticket: These cached (offline) ticket codes will be used for this purchase. When filled the count must be the same as Amount  When key type &#x3D; Subscription : No use | [optional] 
**sell_date** | [**\DateTime**](\DateTime.md) | When key type &#x3D; Ticket: This date is the sell date of the ticket. When empty, current UTC datetime will be used  When key type &#x3D; Subscription : No use | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

