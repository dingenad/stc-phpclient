# LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionRegistrationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errors** | **string[]** | A list of human readable errors concerning the subscription registration. | [optional] 
**success** | **bool** | &lt;code&gt;true&lt;/code&gt; when the registration succeeded; otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**registration_id** | **string** | The Id of the created registration. | [optional] 
**total_amount** | **double** | The total amount that needs to be paid for this registration. | [optional] 
**payment_values** | **map[string,string]** | The key value list of values that need to be posted to the submiturl. | [optional] 
**submit_url** | **string** | The url the user should be redirected to. Use the &lt;strong&gt;PaymentValues&lt;/strong&gt; as hidden form fields. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

