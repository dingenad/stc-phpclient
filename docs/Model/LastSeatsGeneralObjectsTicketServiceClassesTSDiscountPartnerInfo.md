# LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPartnerInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partner_key** | **string** |  | [optional] 
**remaining_tickets** | **int** |  | [optional] 
**discount_events** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSDiscountEventInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSDiscountEventInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

