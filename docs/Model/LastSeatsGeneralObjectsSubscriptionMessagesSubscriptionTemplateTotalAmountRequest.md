# LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateTotalAmountRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**discount_codes** | **string[]** | An array of discountcodes entered by the user. Note: Currently, only a maximum of 1 discountcode is handled. | [optional] 
**selected_subscription_templates** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesSelectedSubscriptionTemplateDto[]**](LastSeatsGeneralObjectsSubscriptionMessagesSelectedSubscriptionTemplateDto.md) | The subscription templates the user has selected. | [optional] 
**template_id** | **int** | The subscription template Id to get the total amount for. | [optional] 
**quantity** | **int** | The quantity of subscriptions the user selected for the &lt;strong&gt;TemplateId&lt;/strong&gt;. | [optional] 
**anonymous_only** | **bool** | Filter for templates eligible for anonymous subscriptions | [optional] 
**template_category** | **string** | Filter for subscription temaplate selection | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

