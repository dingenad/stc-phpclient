# LastSeatsGeneralObjectsResellerMessagesResellerTotalPriceRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**registration_reseller_products** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsResellerResellerRegistrationResellerProduct[]**](LastSeatsGeneralObjectsResellerResellerRegistrationResellerProduct.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

