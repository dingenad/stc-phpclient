# LastSeatsGeneralObjectsTicketServiceClassesTSPriceTypeInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price_key** | **string** | The key of the price&lt;br /&gt;  This type has all the information about a specific price type and is used when retrieving price info for a specific section within a performance or when creating a reservation. | [optional] 
**price_type_key** | **string** | The key of the price type | [optional] 
**external_price_type_id** | **string** | The external price id (if available) | [optional] 
**vat_percentage** | **double** | The vat percentage for this price (if applicable) | [optional] 
**product_code** | **string** | The name of the price type | [optional] 
**total_seats** | **int** | The total number of seats for this specific price type | [optional] 
**available_seats** | **int** | The number of available seats for this specific price type | [optional] 
**price_type_name** | **string** | The name of the price type | [optional] 
**price_type_ticket_name** | **string** | The name of the price type to be printed on the ticket | [optional] 
**price_type_description** | **string** | A brief description of this price type | [optional] 
**price_type_image_url** | **string** | The url to an image of this price type | [optional] 
**branche_price_id** | **string** | The branche price id (ie: EAN code) | [optional] 
**external_price_id** | **string** | The external price id (if available) | [optional] 
**price** | **double** | The specific price per ticket | [optional] 
**from_price** | **double** | The original price so that a discounted price can be shown | [optional] 
**ticket_valid_from** | [**\DateTime**](\DateTime.md) | The date and time from when tickets are valid | [optional] 
**ticket_valid_to** | [**\DateTime**](\DateTime.md) | The date and time until when tickets are valid | [optional] 
**standard_price** | **bool** | Specifies if this is a standard (global available) price (so no CJP pass or other card holders prices). This can be used to determine a (fair) lowest price of an available performance. | [optional] 
**remarks** | **int** | Specifies if specific remarks exists for this price type (ie: PassNumberRequiredPerTicket &#x3D; 1, PassNumberRequiredPerTransaction &#x3D; 2, PhoneNumberRequired &#x3D; 4, FullAddressRequired &#x3D; 8, Sponsorship &#x3D; 16) | [optional] 
**slots_available** | **bool** | Specifies if slots are available for this price type in case of an availability request. If null, no capacity configuration is linked to this pricetype. | [optional] 
**minimum_amount** | **int** | Specifies the minimum amount of tickets that needs to be ordered for this pricetype | [optional] 
**maximum_amount** | **int** | Specifies the maximum amount of tickets that can be ordered for this pricetype | [optional] 
**step** | **int** | Specifies the step of the number of tickets. Step 2 means you can only buy MinimumAmount + 2, 4, 6, 8 etc tickets | [optional] 
**requires_capacity_slot** | **bool** | Specifies whether the price type requires a capacity slot | [optional] 
**available_as_passbook** | **bool** | Specifies whether the ticket is available in Passbook format | [optional] 
**price_type_extra_info** | **string** |  | [optional] 
**successor_of** | **string** | Specifies the price key to be sold before this price type is available | [optional] 
**show_from** | **int** | Specifies the stock count of the predecessor price key at which this price key is available | [optional] 
**is_min_optional** | **bool** | Specifies whether the price key is optional even when there is a minimum amount | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

