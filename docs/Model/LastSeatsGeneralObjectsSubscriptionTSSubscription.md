# LastSeatsGeneralObjectsSubscriptionTSSubscription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subscription_key** | **string** |  | [optional] 
**partner_key** | **string** |  | [optional] 
**external_id** | **string** |  | [optional] 
**valid_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**valid_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**contact_key** | **string** |  | [optional] 
**subscription_products** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionTSSubscriptionProduct[]**](LastSeatsGeneralObjectsSubscriptionTSSubscriptionProduct.md) |  | [optional] 
**subscription_template** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionSubscriptionTemplate**](LastSeatsGeneralObjectsSubscriptionSubscriptionTemplate.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

