# LastSeatsGeneralObjectsPaymentPaymentCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**method_category** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**image** | **string** |  | [optional] 
**sort_order** | **int** |  | [optional] 
**methods** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsPaymentPaymentMethod[]**](LastSeatsGeneralObjectsPaymentPaymentMethod.md) |  | [optional] 
**budget** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

