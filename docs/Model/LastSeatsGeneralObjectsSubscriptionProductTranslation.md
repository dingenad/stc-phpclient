# LastSeatsGeneralObjectsSubscriptionProductTranslation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_id** | **int** |  | [optional] 
**language_id** | **int** | The Id of the language this translation is in. | [optional] 
**description** | **string** | The description of the product. | [optional] 
**plural_name** | **string** | The plural name of this product. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

