# LastSeatsGeneralObjectsReservationSubscriptionInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reservation_key** | **string** |  | [optional] 
**id** | **int** |  | [optional] 
**reservation_id** | **int** |  | [optional] 
**reservation_method_id** | **int** |  | [optional] 
**subscription_id** | **int** |  | [optional] 
**subscription_key** | **string** |  | [optional] 
**price** | **double** |  | [optional] 
**original_price** | **double** |  | [optional] 
**valid_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**valid_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**contact_id** | **int** |  | [optional] 
**contact_first_name** | **string** |  | [optional] 
**contact_middle** | **string** |  | [optional] 
**contact_last_name** | **string** |  | [optional] 
**subscription_template_id** | **int** |  | [optional] 
**subscription_template_key** | **string** |  | [optional] 
**subscription_template_name** | **string** |  | [optional] 
**is_renewal** | **bool** |  | [optional] 
**nr_of_subscription_products** | **int** |  | [optional] 
**reservation_status** | **string** |  | [optional] 
**total_amount** | **double** |  | [optional] 
**discount_code** | **string** |  | [optional] 
**renewal_from** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

