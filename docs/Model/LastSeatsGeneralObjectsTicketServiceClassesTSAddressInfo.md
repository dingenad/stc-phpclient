# LastSeatsGeneralObjectsTicketServiceClassesTSAddressInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**street** | **string** | The street. At least Street or Number or PostalCode or CityName must contain a value. | [optional] 
**number** | **string** | The house number. At least Street or Number or PostalCode or CityName must contain a value. | [optional] 
**extra_address_line** | **string** | Extra address info | [optional] 
**postal_code** | **string** | The postal code. At least Street or Number or PostalCode or CityName must contain a value. | [optional] 
**city_name** | **string** | The name of the city. At least Street or Number or PostalCode or CityName must contain a value. | [optional] 
**state_name** | **string** | The name of the state | [optional] 
**country_name** | **string** | The name of the country | [optional] 
**country_id** | **int** | The id of the country | [optional] 
**lat** | **double** | Latitude of this address | [optional] 
**lon** | **double** | Longitude of this address | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

