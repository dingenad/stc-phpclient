# LastSeatsGeneralObjectsResellerMessagesResellerCreateReservationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reseller_registration_reseller_products** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsResellerResellerRegistrationResellerProduct[]**](LastSeatsGeneralObjectsResellerResellerRegistrationResellerProduct.md) |  | [optional] 
**selected_payment_category** | **int** |  | [optional] 
**selected_payment_method** | **string** |  | [optional] 
**receive_invoice_on_confirm** | **bool** |  | [optional] 
**extra_info1** | **string** |  | [optional] 
**extra_info2** | **string** |  | [optional] 
**extra_info3** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

