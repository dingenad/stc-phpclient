# LastSeatsGeneralObjectsSubscriptionMessagesLoginResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error_messages** | **string[]** | A list of error messages that occurred during login. | [optional] 
**succeeded** | **bool** | &lt;code&gt;true&lt;/code&gt; when the login succeeded; otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**user_details** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsContactInfo**](LastSeatsGeneralObjectsContactInfo.md) |  | [optional] 
**user_token** | **string** | The user token to use to finish the subscription. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

