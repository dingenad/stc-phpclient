# LastSeatsGeneralObjectsTicketServiceClassesTSCapacitySlotInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | Unique code which can be used to identify the slot | [optional] 
**date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**start_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**start_time_minutes_after_midnight** | **int** |  | [optional] 
**end_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**end_time_minutes_after_midnight** | **int** |  | [optional] 
**total_capacity** | **int** |  | [optional] 
**available_capacity** | **int** |  | [optional] 
**characteristic** | **string** |  | [optional] 
**available_price_keys** | **map[string,double]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

