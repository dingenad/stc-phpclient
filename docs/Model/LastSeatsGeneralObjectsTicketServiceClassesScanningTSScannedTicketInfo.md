# LastSeatsGeneralObjectsTicketServiceClassesScanningTSScannedTicketInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_name** | **string** |  | [optional] 
**performance_name** | **string** |  | [optional] 
**section_name** | **string** |  | [optional] 
**price_type_name** | **string** |  | [optional] 
**row_number** | **string** |  | [optional] 
**seat_number** | **string** |  | [optional] 
**product_code** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

