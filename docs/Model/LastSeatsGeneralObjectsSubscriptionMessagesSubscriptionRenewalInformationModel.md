# LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionRenewalInformationModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**renewal_template_id** | **int** |  | [optional] 
**key** | **string** |  | [optional] 
**subscription_template_key** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**tooltip** | **string** |  | [optional] 
**valid_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**valid_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**price** | **double** |  | [optional] 
**original_price** | **double** |  | [optional] 
**easy_renewal** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

