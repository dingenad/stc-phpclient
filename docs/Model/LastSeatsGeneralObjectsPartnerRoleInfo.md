# LastSeatsGeneralObjectsPartnerRoleInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partner_id** | **int** |  | [optional] 
**partner_name** | **string** |  | [optional] 
**role** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

