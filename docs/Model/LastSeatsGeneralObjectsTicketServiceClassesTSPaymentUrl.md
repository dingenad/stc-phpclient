# LastSeatsGeneralObjectsTicketServiceClassesTSPaymentUrl

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_url** | **string** | Url to which customer should be redirected | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

