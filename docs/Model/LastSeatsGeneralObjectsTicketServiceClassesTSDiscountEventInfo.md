# LastSeatsGeneralObjectsTicketServiceClassesTSDiscountEventInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_key** | **string** |  | [optional] 
**remaining_tickets** | **int** |  | [optional] 
**discount_performances** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPerformanceInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPerformanceInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

