# LastSeatsGeneralObjectsSubscriptionSubscriptionTemplateProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The Id of the product. | [optional] 
**subscription_template_id** | **int** | The Id of the subscription template that this product belongs to. | [optional] 
**product_id** | **int** | The product this subscription product refers to. | [optional] 
**minimum_number_of_product** | **int** | The minimum amount of products to select for this subscription template product. | [optional] 
**maximum_number_of_product** | **int** | The maximum amount of products to select for this subscription template product. | [optional] 
**sort_order** | **int** | The sort order in the subscription template. | [optional] 
**product** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionProduct**](LastSeatsGeneralObjectsSubscriptionProduct.md) |  | [optional] 
**form_fields** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsFormFlagSetting[]**](LastSeatsGeneralObjectsFormFlagSetting.md) | A list of user input fields that the user needs to fill in for each product. | [optional] 
**form_flags** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDtosFormFlagsSubscriber**](LastSeatsGeneralObjectsSubscriptionDtosFormFlagsSubscriber.md) |  | [optional] 
**form_fields_required** | **int** | The required formfields bitwise combination. Use &lt;strong&gt;FormFields&lt;/strong&gt; to check the formfields. | [optional] 
**form_fields_visible** | **int** | The visible formfields bitwise combination. Use &lt;strong&gt;FormFields&lt;/strong&gt; to check the formfields. | [optional] 
**form_values** | **map[string,string]** | Not used. | [optional] 
**subscription_layout_id** | **int** | The layout Id when printing the product pass. When &lt;code&gt;null&lt;/code&gt;, the default layout of the product is used. | [optional] 
**is_in_use** | **bool** |  | [optional] 
**send_card_with_confirmation_mail** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

