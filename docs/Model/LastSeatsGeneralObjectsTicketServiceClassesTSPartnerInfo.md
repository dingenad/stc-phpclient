# LastSeatsGeneralObjectsTicketServiceClassesTSPartnerInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The Id of the partner | [optional] 
**partner_key** | **string** | The unique key of the partner | [optional] 
**name** | **string** | The name of the partner | [optional] 
**access_control** | **string** | One of the following values:&lt;br /&gt;&lt;code&gt;NoAccessControl&lt;/code&gt;,&lt;br /&gt;&lt;code&gt;SkiData&lt;/code&gt;&lt;br /&gt;  See also enumeration AccessControl | [optional] 
**access_control_setting1** | **string** | Access control setting 1 of the partner | [optional] 
**access_control_setting2** | **string** | Access control setting 2 of the partner | [optional] 
**access_control_setting3** | **string** | Access control setting 3 of the partner | [optional] 
**access_control_setting4** | **string** | Access control setting 4 of the partner | [optional] 
**access_control_expire_days** | **int** | The number of days before the access control expires | [optional] 
**send_profile_photo_to_ski_data** | **bool** | Send photos to skidata | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

