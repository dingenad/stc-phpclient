# LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ticket_key** | **string** | The key of the ticket | [optional] 
**currency** | **string** | The currency of the price of this ticket e.g. EUR | [optional] 
**price** | **double** | The price of this ticket | [optional] 
**price_type_key** | **string** | The key of the price type | [optional] 
**price_type_name** | **string** | The name of the price type | [optional] 
**external_price_type_id** | **string** | The external price type Id | [optional] 
**branche_price_id** | **string** | The branche Id | [optional] 
**section_name** | **string** | The name of the section of this ticket | [optional] 
**ticket_code** | **string** | The code of the ticket (if applicable) | [optional] 
**barcode_type** | **string** | The type of barcode that must be used (if applicable)&lt;br /&gt;  Barcode types that can be returned are: &lt;br /&gt;&lt;strong&gt;Mostly used:&lt;/strong&gt; 2DDataMatrix, CODE39, CODE93, CODE128, CODE128A, CODE128B, CODE128C, EAN13, QRCode &lt;br /&gt;&lt;strong&gt;Least used&lt;/strong&gt;: CODABAR, CODE11, CODE2OF5,   CODE39EX, COMPOSITE_CCA, COMPOSITE_CCB, COMPOSITE_CCC, EAN128, EAN8, EAN8_2, EAN8_5, EAN13_2, EAN13_5, INTERLEAVED25, ISBN,   ISBN_2, ISBN_5, ISSN, ISSN_2, ISSN_5, ITF14, IDENTCODE, LEITCODE, MSI, MSI10, MSI11, MSI1010, MSI1110, ONECODE, PLANET, POSTNET,   RM4SCC, RSS14, RSS14STACKED, RSS14STACKEDOMNI, RSSEXPANDED, RSSEXPANDEDSTACKED, RSSLIMITED, UPCA, UPCA_2, UPCA_5, UPCE, UPCE_2, UPCE_5 | [optional] 
**row_number** | **string** | The number of the row (if applicable) | [optional] 
**seat_number** | **string** | The number of the seat (if applicable) | [optional] 
**ticket_valid_from** | [**\DateTime**](\DateTime.md) | The date and time from when this ticket is valid (&lt;code&gt;null&lt;/code&gt; if not available) | [optional] 
**ticket_valid_to** | [**\DateTime**](\DateTime.md) | The date and time until this ticket is valid (&lt;code&gt;null&lt;/code&gt; if not available) | [optional] 
**last_claim_date** | [**\DateTime**](\DateTime.md) | The date when this ticket was last scanned | [optional] 
**subscription_product_key** | **string** | Subscription product key | [optional] 
**scanning_display_message** | **string** | Display message shown when scanning | [optional] 
**ticket_text** | **string** |  | [optional] 
**sales_channel_name** | **string** |  | [optional] 
**external_price_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

