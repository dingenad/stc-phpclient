# LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price_type_key** | **string** | The key of the price type (&lt;code&gt;null&lt;/code&gt; when creating a reservation) | [optional] 
**price_key** | **string** | The key of the price&lt;br /&gt;  This type has all the information about a specific price type within a reservation and is used when creating a reservation or when retrieving information about an existing reservation. | [optional] 
**price_type_name** | **string** | The name of the price type (&lt;code&gt;null&lt;/code&gt; when creating a reservation) | [optional] 
**nr_of_seats** | **int** | The total number of reserved seats | [optional] 
**product_code** | **string** | The branche price id (ie: EAN code) | [optional] 
**pass_number** | **string** | The pass number that is entered when reserving | [optional] 
**branche_price_id** | **string** | The branche price id (ie: EAN code) | [optional] 
**capacity_date** | [**\DateTime**](\DateTime.md) | The date (with time: 00:00) of the capacity | [optional] 
**price** | **double** | The ticket price that was paid (&lt;code&gt;null&lt;/code&gt; when creating a reservation) | [optional] 
**start_time_minutes_after_midnight** | **int** | The start time of the capacity in minutes after midnight | [optional] 
**original_price** | **double** | The ticket price that was paid without discount (&lt;code&gt;null&lt;/code&gt; when creating a reservation) | [optional] 
**custom_price** | **double** | The price (only allowed if SalesChannel.AllowCustomPrice OR PriceType.Remarks contains &#x27;free amount allowed&#x27;) | [optional] 
**ticket_valid_from** | [**\DateTime**](\DateTime.md) | The valid from datetime of the tickets | [optional] 
**custom_valid_from** | [**\DateTime**](\DateTime.md) | The valid from date (only allowed if SalesChannel.AllowCustomValidity) | [optional] 
**ticket_valid_to** | [**\DateTime**](\DateTime.md) | The valid to datetime of the tickets | [optional] 
**custom_valid_to** | [**\DateTime**](\DateTime.md) | The valid to date (only allowed if SalesChannel.AllowCustomValidity) | [optional] 
**end_time_minutes_after_midnight** | **int** | The end time of the capacity in minutes after midnight | [optional] 
**start_time** | [**\DateTime**](\DateTime.md) | The full start time including the date | [optional] 
**end_time** | [**\DateTime**](\DateTime.md) | The full end time including the date | [optional] 
**available_as_passbook** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

