# LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPerformanceSectionInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**performance_section_key** | **string** |  | [optional] 
**remaining_tickets** | **int** |  | [optional] 
**discount_price_types** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPriceTypeInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPriceTypeInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

