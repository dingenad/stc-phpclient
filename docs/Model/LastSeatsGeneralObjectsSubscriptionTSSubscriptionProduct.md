# LastSeatsGeneralObjectsSubscriptionTSSubscriptionProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subscription_product_key** | **string** |  | [optional] 
**subscription_key** | **string** |  | [optional] 
**area_code** | **string** |  | [optional] 
**external_id** | **string** |  | [optional] 
**picture_url** | **string** |  | [optional] 
**subscription_layout_id** | **int** |  | [optional] 
**ticket_codes** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo.md) |  | [optional] 
**description** | **string** |  | [optional] 
**contact_key** | **string** |  | [optional] 
**contact** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo**](LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo.md) |  | [optional] 
**permission_id** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

