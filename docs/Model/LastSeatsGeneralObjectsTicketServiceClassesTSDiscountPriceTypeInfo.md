# LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPriceTypeInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price_type_key** | **string** |  | [optional] 
**remaining_tickets** | **int** |  | [optional] 
**discount_prices** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPriceInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPriceInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

