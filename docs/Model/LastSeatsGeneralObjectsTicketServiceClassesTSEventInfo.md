# LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_key** | **string** | The key of the event | [optional] 
**event_name** | **string** | The name of the event | [optional] 
**performer** | **string** | The performer of the event (empty if same as name) | [optional] 
**tagline** | **string** | Specific title or words for promotional purposes | [optional] 
**description** | **string** | The description of the event (if requested) | [optional] 
**image_urls** | **string[]** | An array of url&#x27;s for all the images of this event | [optional] 
**image_tags** | **string[]** | An array of tags for all the images of this event (i.e. credits, photographer, etc.) | [optional] 
**movie_link** | **string** | Code for YouTube movie | [optional] 
**blocked_countries_for_sales** | **string** | The ISO codes of the countries where sales are NOT allowed | [optional] 
**rating** | **double** | The average rating of this event (empty if no ratings available yet) | [optional] 
**review_count** | **int** | The number of ratings/reviews for this event (empty if no ratings/reviews available yet) | [optional] 
**currency** | **string** | The currency symbol for the amount | [optional] 
**currency_code** | **string** | The currency code for the amount | [optional] 
**lowest_price** | **double** | The overall lowest price for a performance | [optional] 
**categories** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSCategoryInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSCategoryInfo.md) | List of categories of this event (at least one) | [optional] 
**performances** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPerformanceInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSPerformanceInfo.md) | List of performances of this event | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

