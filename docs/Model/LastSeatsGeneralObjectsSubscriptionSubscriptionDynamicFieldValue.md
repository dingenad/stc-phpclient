# LastSeatsGeneralObjectsSubscriptionSubscriptionDynamicFieldValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The Id of the subscription dynamic field. | [optional] 
**subscription_id** | **int** | The Id of the subscription. | [optional] 
**subscription_dynamic_field_id** | **int** | The Id of the subscription Dynamic field. | [optional] 
**subscription_dynamic_field** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionSubscriptionTemplateDynamicField**](LastSeatsGeneralObjectsSubscriptionSubscriptionTemplateDynamicField.md) |  | [optional] 
**string_value** | **string** | The string value (only if the type of the field is string/textarea). | [optional] 
**bool_value** | **bool** | The boolean value (only if the type of the field is boolean). | [optional] 
**date_value** | [**\DateTime**](\DateTime.md) | The date value (only if the type of the field is date). | [optional] 
**numeric_value** | **double** | The numeric value (only if the type of the field is numeric or dropdown). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

