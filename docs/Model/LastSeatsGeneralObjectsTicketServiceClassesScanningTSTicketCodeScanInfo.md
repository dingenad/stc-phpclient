# LastSeatsGeneralObjectsTicketServiceClassesScanningTSTicketCodeScanInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ticket_code** | **string** |  | [optional] 
**timestamp** | [**\DateTime**](\DateTime.md) |  | [optional] 
**blocked** | **bool** |  | [optional] 
**scan_dates** | [**\DateTime[]**](\DateTime.md) |  | [optional] 
**max_claim_amount** | **int** |  | [optional] 
**max_claim_amount_period** | **int** |  | [optional] 
**max_claim_amount_period_duration** | **int** |  | [optional] 
**max_claim_amount_period_duration_type** | **string** |  | [optional] 
**claim_interval** | **int** |  | [optional] 
**claim_interval_type** | **string** |  | [optional] 
**ticket_scan_info** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSTicketScanInfo**](LastSeatsGeneralObjectsTicketServiceClassesScanningTSTicketScanInfo.md) |  | [optional] 
**subscription_scan_info** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSSubscriptionScanInfo**](LastSeatsGeneralObjectsTicketServiceClassesScanningTSSubscriptionScanInfo.md) |  | [optional] 
**scanned_product_info** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScannedProductInfo**](LastSeatsGeneralObjectsTicketServiceClassesScanningTSScannedProductInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

