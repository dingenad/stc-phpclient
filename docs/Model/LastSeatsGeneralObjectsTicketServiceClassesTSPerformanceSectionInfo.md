# LastSeatsGeneralObjectsTicketServiceClassesTSPerformanceSectionInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_key** | **string** | The key of the event | [optional] 
**performance_key** | **string** | The key of the performance | [optional] 
**performance_section_key** | **string** | The unique key of the section within this performance | [optional] 
**section_key** | **string** | The key of the section | [optional] 
**section_name** | **string** | The name of this section | [optional] 
**area_code** | **string** | The section area code | [optional] 
**available_seats** | **int** | The total number of available seats | [optional] 
**seated_section** | **bool** | &lt;code&gt;true&lt;/code&gt; when seated (row &amp;amp; seat numbers); otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**discount_rules_apply** | **bool** | &lt;code&gt;true&lt;/code&gt; when a discount applies (for instance with a code); otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**order_link** | **string** | The direct link to an external order process | [optional] 
**currency_code** | **string** | The currency code for the section | [optional] 
**currency** | **string** | The currency symbol for the section | [optional] 
**price_types** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPriceTypeInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSPriceTypeInfo.md) | The list of price types for this section | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

