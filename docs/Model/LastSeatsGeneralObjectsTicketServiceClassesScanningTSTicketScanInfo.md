# LastSeatsGeneralObjectsTicketServiceClassesScanningTSTicketScanInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_key** | **string** |  | [optional] 
**performance_key** | **string** |  | [optional] 
**performance_section_key** | **string** |  | [optional] 
**price_type_key** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

