# LastSeatsGeneralObjectsTicketServiceClassesTSPaymentMethod

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | String used to identify payment method, depends on PSP | [optional] 
**category** | **string** | Enum used to identify category | [optional] 
**name** | **string** | Displayable method name | [optional] 
**image_url** | **string** |  | [optional] 
**sort_order** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

