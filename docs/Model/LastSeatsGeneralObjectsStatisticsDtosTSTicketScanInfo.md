# LastSeatsGeneralObjectsStatisticsDtosTSTicketScanInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scan_id** | **int** | Unique id of the scan, when manually checked in, this value is 0 | [optional] 
**ticket_code** | **string** | Ticketcode of the ticket/subscription | [optional] 
**type** | **string** | Type of the scan, ie. Ticket or Subscription | [optional] 
**reservation_key** | **string** | Unique key of the reservation where this ticket belongs to | [optional] 
**subscription_key** | **string** | Unique key of the subscription | [optional] 
**subscription_product_key** | **string** | Unique key of the subscription product | [optional] 
**scan_date** | [**\DateTime**](\DateTime.md) | The date when this ticketcode is scanned | [optional] 
**subscription_template_key** | **string** | Unique key of the subscription template | [optional] 
**product_name** | **string** | Name of the subscription template | [optional] 
**internal_id** | **string** | Subscription Template Internal id. This field can be filled in the Subscription template page and can differ from purpose per partner | [optional] 
**product_internal_id** | **string** | Product internal id. This field contains the information filled in the product page for internal id | [optional] 
**name** | **string** | Name of the subscription holder | [optional] 
**e_mail** | **string** | Email address of the subscription holder | [optional] 
**street** | **string** | Street of the subscriptionholder | [optional] 
**house_number** | **string** | House number of the subscription holder | [optional] 
**postal_code** | **string** | Postalcode of the subscription holder | [optional] 
**city_name** | **string** | City of the subscription holder | [optional] 
**country_name** | **string** | Country of the subscription holder | [optional] 
**lat** | **double** |  | [optional] 
**lon** | **double** |  | [optional] 
**event_key** | **string** |  | [optional] 
**performance_key** | **string** |  | [optional] 
**performance_section_key** | **string** |  | [optional] 
**valid_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**valid_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**company_name** | **string** |  | [optional] 
**first_name** | **string** |  | [optional] 
**middle** | **string** |  | [optional] 
**last_name** | **string** |  | [optional] 
**phone_number** | **string** |  | [optional] 
**reservation_number** | **string** |  | [optional] 
**price_key** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

