# LastSeatsGeneralObjectsTicketServiceClassesTSPerformanceInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**performance_key** | **string** | The key of the performance.&lt;br /&gt;  Either &lt;strong&gt;StartDateTime&lt;/strong&gt; or &lt;strong&gt;PerformanceName&lt;/strong&gt; has a value. For instance a ticket for an amusement park doesn&#x27;t have a specific start date and time but is only valid within a specific period. When reservation info is returned, only the &lt;strong&gt;PerformanceKey&lt;/strong&gt; and the &lt;strong&gt;StartDateTime&lt;/strong&gt; or the &lt;strong&gt;PerformanceName&lt;/strong&gt; is returned. All the other members are empty. | [optional] 
**external_performance_id** | **string** | The external performance Id | [optional] 
**start_date_time** | [**\DateTime**](\DateTime.md) | The start date/time. | [optional] 
**start_date_time_text** | **string** | The formatted start date/time | [optional] 
**performance_name** | **string** | The name of the performance | [optional] 
**performance_description** | **string** | The description of the performance | [optional] 
**remarks** | **int** | A combination of remarks for this performance as described in the table below.&lt;br /&gt;&lt;code&gt;0&lt;/code&gt; No remarks;&lt;br /&gt;&lt;code&gt;1&lt;/code&gt; Not bookable;&lt;br /&gt;&lt;code&gt;2&lt;/code&gt; Premiere;&lt;br /&gt;&lt;code&gt;4&lt;/code&gt; Try-out;&lt;br /&gt;&lt;code&gt;8&lt;/code&gt; Derniere;&lt;br /&gt;&lt;code&gt;16&lt;/code&gt; Extended;&lt;br /&gt;&lt;code&gt;32&lt;/code&gt; Last tickets;&lt;br /&gt;&lt;code&gt;64&lt;/code&gt; Sold out;&lt;br /&gt;&lt;code&gt;128&lt;/code&gt; Moved;&lt;br /&gt;&lt;code&gt;256&lt;/code&gt; Cancelled;&lt;br /&gt;&lt;code&gt;512&lt;/code&gt; Invitation only;&lt;br /&gt;&lt;code&gt;1024&lt;/code&gt; &#x27;Special&#x27; remark. Can be used for specific campaigns&lt;br /&gt;&lt;br /&gt;    So a &#x27;Remark&#x27; of 66 means that this performance concerns a Premiere (2) but it is Sold out (64) | [optional] 
**rating** | **double** | The average rating of this performance (empty if no ratings available yet) | [optional] 
**review_count** | **int** | The number of ratings/reviews for this performance (empty if no ratings/reviews available yet) | [optional] 
**sale_date_to** | [**\DateTime**](\DateTime.md) | The date until the performance can be sold online | [optional] 
**currency** | **string** | The currency symbol for the amount | [optional] 
**currency_code** | **string** | The currency code for the amount | [optional] 
**lowest_price** | **double** | The lowest price of section within this specific performance | [optional] 
**max_seat_amount_per_order** | **int** | The maximum amount of seats that can be sold in one order (&lt;code&gt;null&lt;/code&gt; if not applicable) | [optional] 
**reservation_costs_per_ticket** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationCosts**](LastSeatsGeneralObjectsTicketServiceClassesTSReservationCosts.md) |  | [optional] 
**reservation_costs_per_transaction** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationCosts**](LastSeatsGeneralObjectsTicketServiceClassesTSReservationCosts.md) |  | [optional] 
**location** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSLocationInfo**](LastSeatsGeneralObjectsTicketServiceClassesTSLocationInfo.md) |  | [optional] 
**order_link** | **string** | The direct link to an external order process (if only 1 section with 1 external link exists) | [optional] 
**sort_order** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

