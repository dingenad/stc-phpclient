# LastSeatsGeneralObjectsBasketInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**basket_key** | **string** |  | [optional] 
**language_id** | **int** |  | [optional] 
**sales_channel_id** | **int** |  | [optional] 
**currency_id** | **int** |  | [optional] 
**basket_number** | **string** |  | [optional] 
**external_basket_number** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**basket_created** | [**\DateTime**](\DateTime.md) |  | [optional] 
**basket_confirmed** | [**\DateTime**](\DateTime.md) |  | [optional] 
**basket_cancelled** | [**\DateTime**](\DateTime.md) |  | [optional] 
**payment_method** | **string** |  | [optional] 
**reservation_amount** | **double** |  | [optional] 
**total_amount** | **double** |  | [optional] 
**transaction_costs** | **double** |  | [optional] 
**payment_costs** | **double** |  | [optional] 
**discount** | **double** |  | [optional] 
**refund_costs_contact** | **double** |  | [optional] 
**refund_costs_partner** | **double** |  | [optional] 
**reservations** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsReservationOverviewInfo[]**](LastSeatsGeneralObjectsReservationOverviewInfo.md) |  | [optional] 
**last_updated_by** | **int** |  | [optional] 
**reservation_subscriptions** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsReservationSubscriptionInfo[]**](LastSeatsGeneralObjectsReservationSubscriptionInfo.md) |  | [optional] 
**last_updated_on** | [**\DateTime**](\DateTime.md) |  | [optional] 
**contact_id** | **int** |  | [optional] 
**partner_id** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

