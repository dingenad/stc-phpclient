# LastSeatsGeneralObjectsTicketServiceClassesTSPartnerNameInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The Id of the partner | [optional] 
**partner_key** | **string** | The key of the partner | [optional] 
**name** | **string** | The name of the partner | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

