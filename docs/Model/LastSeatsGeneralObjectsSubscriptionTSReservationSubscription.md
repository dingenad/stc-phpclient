# LastSeatsGeneralObjectsSubscriptionTSReservationSubscription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reservation_key** | **string** |  | [optional] 
**subscription_key** | **string** |  | [optional] 
**subscription_template_key** | **string** |  | [optional] 
**price** | **double** |  | [optional] 
**original_price** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

