# LastSeatsGeneralObjectsTicketServiceClassesTSResellerVolumeDiscountInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**required_volume** | **int** |  | [optional] 
**buying_price** | **double** |  | [optional] 
**sold_tickets** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

