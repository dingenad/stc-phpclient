# LastSeatsGeneralObjectsTicketServiceClassesTSStartPaymentRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_category** | **string** | Chosen payment category | [optional] 
**payment_method_id** | **string** | Chosen payment method | [optional] 
**return_url** | **string** | Url to which user should be redirected after payment process (might be transformed using portal settings) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

