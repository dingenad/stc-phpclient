# LastSeatsGeneralObjectsSubscriptionDtosDiscountPricingDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**discount_id** | **int** | The id of the discount. | [optional] 
**code** | **string** | The discount code. | [optional] 
**title** | **string** | The discount name. | [optional] 
**amount** | **double** | The discount amount in the currency of the subscription template. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

