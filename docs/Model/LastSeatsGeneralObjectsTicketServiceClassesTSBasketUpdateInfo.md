# LastSeatsGeneralObjectsTicketServiceClassesTSBasketUpdateInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**external_basket_number** | **string** | The external number, as supplied by another system | [optional] 
**transaction_costs** | **double** | The transaction costs for this basket | [optional] 
**payment_costs** | **double** | The payment costs for the basket | [optional] 
**discount** | **double** | The discount that applies | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

