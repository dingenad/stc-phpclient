# LastSeatsGeneralObjectsSubscriptionMessagesRenewalSubscriptionResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**renewal_template_id** | **int** |  | [optional] 
**error_code** | **string** |  | [optional] 
**error_renew_message** | **string** |  | [optional] 
**reservation** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsReservationInfo**](LastSeatsGeneralObjectsReservationInfo.md) |  | [optional] 
**subscription_renewal_list** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionRenewalInformationModel[]**](LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionRenewalInformationModel.md) |  | [optional] 
**basket** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsBasketInfo**](LastSeatsGeneralObjectsBasketInfo.md) |  | [optional] 
**price** | **double** |  | [optional] 
**original_price** | **double** |  | [optional] 
**description** | **string** |  | [optional] 
**tooltip** | **string** |  | [optional] 
**valid_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**valid_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**mail_address** | **string** |  | [optional] 
**easy_renewal** | **bool** |  | [optional] 
**succeeded** | **bool** | &lt;code&gt;true&lt;/code&gt; when the operation succeeded; otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**error_message** | **string** | The user readable errormessage. | [optional] 
**is_redirect** | **bool** | &lt;code&gt;true&lt;/code&gt; to indicate the user should be redirected; otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional] 
**redirect_url** | **string** | The url to redirect the user to. | [optional] 
**display_error** | **bool** | Indicates wether the error in de response should be displayed | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

