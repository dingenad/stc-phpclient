# LastSeatsGeneralObjectsSubscriptionDtosFormFlagsSubscriber

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**photo** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsFormFlagSetting**](LastSeatsGeneralObjectsFormFlagSetting.md) |  | [optional] 
**id** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsFormFlagSetting**](LastSeatsGeneralObjectsFormFlagSetting.md) |  | [optional] 
**first_name** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsFormFlagSetting**](LastSeatsGeneralObjectsFormFlagSetting.md) |  | [optional] 
**middle_name** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsFormFlagSetting**](LastSeatsGeneralObjectsFormFlagSetting.md) |  | [optional] 
**last_name** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsFormFlagSetting**](LastSeatsGeneralObjectsFormFlagSetting.md) |  | [optional] 
**gender** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsFormFlagSetting**](LastSeatsGeneralObjectsFormFlagSetting.md) |  | [optional] 
**birth_date** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsFormFlagSetting**](LastSeatsGeneralObjectsFormFlagSetting.md) |  | [optional] 
**street** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsFormFlagSetting**](LastSeatsGeneralObjectsFormFlagSetting.md) |  | [optional] 
**number** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsFormFlagSetting**](LastSeatsGeneralObjectsFormFlagSetting.md) |  | [optional] 
**postal_code** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsFormFlagSetting**](LastSeatsGeneralObjectsFormFlagSetting.md) |  | [optional] 
**city** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsFormFlagSetting**](LastSeatsGeneralObjectsFormFlagSetting.md) |  | [optional] 
**country** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsFormFlagSetting**](LastSeatsGeneralObjectsFormFlagSetting.md) |  | [optional] 
**mail_address** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsFormFlagSetting**](LastSeatsGeneralObjectsFormFlagSetting.md) |  | [optional] 
**mail_address_confirm** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsFormFlagSetting**](LastSeatsGeneralObjectsFormFlagSetting.md) |  | [optional] 
**phone_number** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsFormFlagSetting**](LastSeatsGeneralObjectsFormFlagSetting.md) |  | [optional] 
**mobile_number** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsFormFlagSetting**](LastSeatsGeneralObjectsFormFlagSetting.md) |  | [optional] 
**company_name** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsFormFlagSetting**](LastSeatsGeneralObjectsFormFlagSetting.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

