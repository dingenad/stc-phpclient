# LastSeatsGeneralObjectsTicketServiceClassesTSPaymentVariable

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **string** | The key | [optional] 
**value** | **string** | The value | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

