# STCTicketsApiModelsProductViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **string** |  | [optional] 
**friendly_name** | **string** |  | [optional] 
**price** | **double** |  | [optional] 
**currency** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

