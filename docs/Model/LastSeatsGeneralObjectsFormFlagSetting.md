# LastSeatsGeneralObjectsFormFlagSetting

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**visible** | **bool** | Controls whether this field should be visible to the user. | [optional] 
**mandatory** | **bool** | Controls whether this field is mandatory (required) to be filled in by the user. | [optional] 
**field_name** | **string** | The name of the field. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

