# LastSeatsGeneralObjectsTicketServiceClassesTSLocationInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**location_key** | **string** | The key of the location | [optional] 
**location_name** | **string** | The name of the location | [optional] 
**website** | **string** | The website of the location | [optional] 
**phone_number** | **string** | The phone number of the location | [optional] 
**mail_address** | **string** | The mail address for customers of the location | [optional] 
**rating** | **double** | The average rating of this location (empty if no ratings available yet) | [optional] 
**review_count** | **int** | The number of ratings/reviews for this location (empty if no ratings/reviews available yet) | [optional] 
**address** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSAddressInfo**](LastSeatsGeneralObjectsTicketServiceClassesTSAddressInfo.md) |  | [optional] 
**general_info** | **string** | General information of this location | [optional] 
**opening_hours_info** | **string** | The opening hours of this location | [optional] 
**route_info** | **string** | Route info of how to get to this location by car | [optional] 
**parking_info** | **string** | Parking information for this specific location | [optional] 
**public_transport_info** | **string** | Information on how to get to this location by public transport | [optional] 
**restaurant_info** | **string** | Restaurant information inside or around this location | [optional] 
**handicapped_info** | **string** | Information for disabled customers | [optional] 
**image_url** | **string** | The url to the image of this location (null if not available) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

