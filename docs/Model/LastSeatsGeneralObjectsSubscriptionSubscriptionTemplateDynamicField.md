# LastSeatsGeneralObjectsSubscriptionSubscriptionTemplateDynamicField

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The Id of the subscription template dynamic field combination. | [optional] 
**subscription_template_id** | **int** | The Id of the subscription template. | [optional] 
**dynamic_field_id** | **int** | The Id of the dynamic field. | [optional] 
**sort_order** | **int** | The order in which to show this dynamic field in the subscription template. | [optional] 
**criteria** | **string** | The criteria. | [optional] 
**dynamic_field** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDynamicField**](LastSeatsGeneralObjectsSubscriptionDynamicField.md) |  | [optional] 
**required** | **bool** | Is the field required | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

