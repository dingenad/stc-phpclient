# STCTicketsApiModelsOnePageCheckoutReservationModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price_type_list** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeInfo[]**](LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeInfo.md) |  | [optional] 
**contact_info** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo**](LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo.md) |  | [optional] 
**discount_code** | **string** |  | [optional] 
**affiliate** | **string** |  | [optional] 
**external_reservation_number** | **string** |  | [optional] 
**extra_info1** | **string** |  | [optional] 
**extra_info2** | **string** |  | [optional] 
**extra_info3** | **string** |  | [optional] 
**ip_address** | **string** |  | [optional] 
**capacity_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**capacity_time** | **int** |  | [optional] 
**payment_brand** | **string** |  | [optional] 
**payment_method_id** | **int** |  | [optional] 
**amount_paid** | **double** |  | [optional] 
**app_namespace** | **string** |  | [optional] 
**result_url** | **string** |  | [optional] 
**notify_url** | **string** |  | [optional] 
**succes_url** | **string** |  | [optional] 
**partner_key** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

