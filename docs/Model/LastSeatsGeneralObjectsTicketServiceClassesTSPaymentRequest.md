# LastSeatsGeneralObjectsTicketServiceClassesTSPaymentRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**submit_url** | **string** | The url to redirect (with a post) the user to | [optional] 
**payment_variables** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPaymentVariable[]**](LastSeatsGeneralObjectsTicketServiceClassesTSPaymentVariable.md) | The data to be posted to the submiturl | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

