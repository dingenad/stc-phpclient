# LastSeatsGeneralObjectsSubscriptionDtosSubscriptionRegistrationDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**discounts** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDtosDiscountPricingDto[]**](LastSeatsGeneralObjectsSubscriptionDtosDiscountPricingDto.md) | An array of discountcodes entered by the user. Note: Currently, only a maximum of 1 discountcode is handled. | [optional] 
**subscriptions** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDtosSubscriptionDto[]**](LastSeatsGeneralObjectsSubscriptionDtosSubscriptionDto.md) | The subscriptions of the registration. | [optional] 
**dynamic_field_values** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionSubscriptionDynamicFieldValue[]**](LastSeatsGeneralObjectsSubscriptionSubscriptionDynamicFieldValue.md) | The generic dynamic values filled in by the user. | [optional] 
**holder_form_values** | **map[string,string]** | The subscription holder form values filled in by the user.&lt;br /&gt;  This is a dictionary of [fieldname, value] | [optional] 
**payment_category** | **int** | The selected payment category Id to do the payment. | [optional] 
**payment_method** | **string** | The selected payment method Id to do the payment. | [optional] 
**user_token** | **string** | Optional: If the user (e-mail address) already exists in the system, the user should login. When logged in, a user token is returned.   This token should be given here. If it is not present and the e-mail address exists, registration is not possible. | [optional] 
**is_anonymous** | **bool** | Optional: Flag for creating anonymous subscriptions | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

