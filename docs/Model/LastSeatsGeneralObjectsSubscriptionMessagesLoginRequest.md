# LastSeatsGeneralObjectsSubscriptionMessagesLoginRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email_address** | **string** | The e-mail address. | [optional] 
**password** | **string** | The password. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

