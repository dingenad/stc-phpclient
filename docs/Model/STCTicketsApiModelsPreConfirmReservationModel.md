# STCTicketsApiModelsPreConfirmReservationModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**visit_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**continuation_link** | **string** |  | [optional] 
**payment_method** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

