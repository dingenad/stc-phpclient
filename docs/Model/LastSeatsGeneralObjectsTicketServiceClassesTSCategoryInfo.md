# LastSeatsGeneralObjectsTicketServiceClassesTSCategoryInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category_key** | **string** | The key of the category | [optional] 
**category_name** | **string** | The name of the category | [optional] 
**sub_category_key** | **string** | The key of the sub category | [optional] 
**sub_category_name** | **string** | The name of the sub category | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

