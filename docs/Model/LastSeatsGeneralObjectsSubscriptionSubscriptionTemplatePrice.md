# LastSeatsGeneralObjectsSubscriptionSubscriptionTemplatePrice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The Id of the subscription template price | [optional] 
**subscription_template_id** | **int** | The ID of the subscription template this price belongs to | [optional] 
**original_price** | **double** |  | [optional] 
**price** | **double** |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) | The start date when this record should be used | [optional] 
**type** | **string** | What type of subscription template this price should be used for | [optional] 
**channel** | **string** | Which channel this price should be used in | [optional] 
**start_date_as_string** | **string** | The start date when this record should be used as string. This property is used for saving the StartDate. Warning: setting this property overrides the getter for StartDate. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

