# LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanDeviceInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**manufacturer** | **string** |  | [optional] 
**model** | **string** |  | [optional] 
**device_name** | **string** |  | [optional] 
**scanner_name** | **string** |  | [optional] 
**operating_system** | **string** |  | [optional] 
**operating_system_version** | **string** |  | [optional] 
**connection_records** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanDeviceConnectionRecord[]**](LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanDeviceConnectionRecord.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

