# LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeRequestInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price_key** | **string** | The key of the price&lt;br /&gt;  This type has all the information about a specific price type within a reservation and is used when creating a reservation or when retrieving information about an existing reservation. | [optional] 
**nr_of_seats** | **int** | The total number of reserved seats | [optional] 
**pass_number** | **string** | The pass number that is entered when reserving | [optional] 
**capacity_date** | [**\DateTime**](\DateTime.md) | The date (with time: 00:00) of the capacity | [optional] 
**start_time_minutes_after_midnight** | **int** | The start time of the capacity in minutes after midnight | [optional] 
**custom_price** | **double** | The price (only allowed if SalesChannel.AllowCustomPrice OR PriceType.Remarks contains &#x27;free amount allowed&#x27;) | [optional] 
**custom_valid_from** | [**\DateTime**](\DateTime.md) | The valid from date (only allowed if SalesChannel.AllowCustomValidity) | [optional] 
**custom_valid_to** | [**\DateTime**](\DateTime.md) | The valid to date (only allowed if SalesChannel.AllowCustomValidity) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

