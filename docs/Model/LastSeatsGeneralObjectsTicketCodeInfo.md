# LastSeatsGeneralObjectsTicketCodeInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**performance_section_id** | **int** |  | [optional] 
**subscription_product_validity_id** | **int** |  | [optional] 
**section_name** | **string** |  | [optional] 
**ticket_code** | **string** |  | [optional] 
**price_type_id** | **int** |  | [optional] 
**price_type_name** | **string** |  | [optional] 
**blocked** | **bool** |  | [optional] 
**reserved** | **bool** |  | [optional] 
**max_claim_amount** | **int** |  | [optional] 
**cached_claim_amount** | **int** |  | [optional] 
**last_claim_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**contact_id** | **int** |  | [optional] 
**reservation_id** | **int** |  | [optional] 
**reservation_price_type_id** | **int** |  | [optional] 
**reservation_price_type_key** | **string** |  | [optional] 
**reservation_price_type_name** | **string** |  | [optional] 
**reservation_price_per_seat** | **double** |  | [optional] 
**reservation_original_price** | **double** |  | [optional] 
**claimed_reservation_id** | **int** |  | [optional] 
**claimed_reservation_price_type_id** | **int** |  | [optional] 
**claimed_reservation_price_type_key** | **string** |  | [optional] 
**claimed_reservation_price_type_name** | **string** |  | [optional] 
**claimed_reservation_price_per_seat** | **double** |  | [optional] 
**claimed_reservation_original_price** | **double** |  | [optional] 
**reservation_number** | **string** |  | [optional] 
**reservation_confirmed** | [**\DateTime**](\DateTime.md) |  | [optional] 
**reservation_contact_id** | **int** |  | [optional] 
**gender** | **string** |  | [optional] 
**first_name** | **string** |  | [optional] 
**middle** | **string** |  | [optional] 
**last_name** | **string** |  | [optional] 
**mail_address** | **string** |  | [optional] 
**phone_number** | **string** |  | [optional] 
**mobile_number** | **string** |  | [optional] 
**event_name** | **string** |  | [optional] 
**performance_name** | **string** |  | [optional] 
**group** | **int** |  | [optional] 
**general_selection_id** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

