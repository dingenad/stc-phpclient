# LastSeatsGeneralObjectsTicketServiceClassesTSChangePassword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**old_password** | **string** | The current password | [optional] 
**new_password** | **string** | The new password | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

