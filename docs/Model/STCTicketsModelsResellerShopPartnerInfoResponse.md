# STCTicketsModelsResellerShopPartnerInfoResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invoice_on_order_allowed** | **bool** |  | [optional] 
**extra_info1_label** | **string** |  | [optional] 
**extra_info2_label** | **string** |  | [optional] 
**extra_info3_label** | **string** |  | [optional] 
**currency** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsCurrencyInfo**](LastSeatsGeneralObjectsCurrencyInfo.md) |  | [optional] 
**succeeded** | **bool** |  | [optional] 
**error_message** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

