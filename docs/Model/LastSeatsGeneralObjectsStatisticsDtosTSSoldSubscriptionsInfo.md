# LastSeatsGeneralObjectsStatisticsDtosTSSoldSubscriptionsInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subscription_key** | **string** | Unique key of the subscriptoin | [optional] 
**type** | **string** | Type of the purchase | [optional] 
**reservation_key** | **string** | Unique key of the reservation. Multiple subscriptions can be bought in one reservation | [optional] 
**subscription_template_key** | **string** | Unique key of the template. | [optional] 
**product_name** | **string** | Name of the subscription template | [optional] 
**sale_date** | [**\DateTime**](\DateTime.md) | Date when the subscription is bought | [optional] 
**price** | **double** | Sale price of the subscription | [optional] 
**renewal** | **bool** | When true, this subscription is a renewal from an other subscription | [optional] 
**channel** | **string** | Where is this subscription bought, online or at the cash register | [optional] 
**subscription_holder_key** | **string** | Unique key of the subscriptionholder. A holder can have more then one subscription | [optional] 
**internal_id** | **string** | Subscription Template Internal id. This field can be filled in the Subscription template page and can differ from purpose per partner | [optional] 
**name** | **string** | Name of the subscription holder | [optional] 
**e_mail** | **string** | Email address of the subscription holder | [optional] 
**street** | **string** | Street of the subscriptionholder | [optional] 
**house_number** | **string** | House number of the subscription holder | [optional] 
**postal_code** | **string** | Postalcode of the subscription holder | [optional] 
**city_name** | **string** | City of the subscription holder | [optional] 
**country_name** | **string** | Country of the subscription holder | [optional] 
**lat** | **double** |  | [optional] 
**lon** | **double** |  | [optional] 
**valid_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**valid_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**company_name** | **string** |  | [optional] 
**first_name** | **string** |  | [optional] 
**middle** | **string** |  | [optional] 
**last_name** | **string** |  | [optional] 
**phone_number** | **string** |  | [optional] 
**receive_news_letter** | **bool** |  | [optional] 
**payment_method** | **string** |  | [optional] 
**nr_of_subscription_products** | **int** |  | [optional] 
**cancel_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**reservation_number** | **string** |  | [optional] 
**language** | **string** |  | [optional] 
**language_code** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

