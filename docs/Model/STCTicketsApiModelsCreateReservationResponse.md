# STCTicketsApiModelsCreateReservationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reservation_id** | **string** | Returns the reservationId. | [optional] 
**total_amount** | **double** | Returns the totalamount of this reservation | [optional] 
**subscription_keys** | **string[]** | Only applies on subscription reservations: gives a list of subscriptionkeys of this reservation | [optional] 
**edit_subscription_url** | **string** | Only applies on subscription reservations: gives the url where the subscription can be filled in on the portal | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

