# LastSeatsGeneralObjectsSubscriptionProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The product Id | [optional] 
**product_key** | **string** | The product key | [optional] 
**partner_id** | **int** | The partner this product belongs to. | [optional] 
**name** | **string** | The name of the product. Example: &lt;code&gt;Strawberry&lt;/code&gt; | [optional] 
**plural_name** | **string** | The plural name of this product. Example: &lt;code&gt;Strawberries&lt;/code&gt; | [optional] 
**create_contact** | **bool** | Does a contact need to be created for this product? This generally indicates that this product is a person product. | [optional] 
**cash_register_product** | **bool** | Product can only be bought from a cashier. | [optional] 
**translations** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionProductTranslation[]**](LastSeatsGeneralObjectsSubscriptionProductTranslation.md) | The description and pluralname translations | [optional] 
**dynamic_fields** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionProductDynamicField[]**](LastSeatsGeneralObjectsSubscriptionProductDynamicField.md) | The dynamic fields that need to filled in by the user. | [optional] 
**description** | **string** | The general description of the product | [optional] 
**subscription_layout_id** | **int** | The layout of the product pass print. | [optional] 
**external_id** | **string** | The external Id | [optional] 
**area_code** | **string** | The code of the area this product belongs to. | [optional] 
**ticket_code_batch_id** | **int** | The ticketcode batch this product uses to obtain a new ticketcode. | [optional] 
**minimum_age** | **int** | The minimum age for the product contact. Only applies when this product is a contact product. | [optional] 
**maximum_age** | **int** | The maximum age for the product contact. Only applies when this product is a contact product. | [optional] 
**internal_id** | **string** | The internal Id | [optional] 
**max_claim_amount** | **int** |  | [optional] 
**max_claim_amount_period** | **int** |  | [optional] 
**max_claim_amount_period_duration** | **int** |  | [optional] 
**max_claim_amount_period_duration_type** | **string** |  | [optional] 
**claim_interval** | **int** |  | [optional] 
**claim_interval_type** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

