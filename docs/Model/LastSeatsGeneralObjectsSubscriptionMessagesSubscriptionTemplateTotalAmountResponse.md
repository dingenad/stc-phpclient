# LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateTotalAmountResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subscription_template_id** | **int** | The Id of the requested subscription template. | [optional] 
**subscription_total_amount** | **double** | The total amount of all subscriptions with this template in the current currency. | [optional] 
**holder_form_flags** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDtosFormFlagsSubscriptionHolder**](LastSeatsGeneralObjectsSubscriptionDtosFormFlagsSubscriptionHolder.md) |  | [optional] 
**dynamic_fields** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionSubscriptionTemplateDynamicField[]**](LastSeatsGeneralObjectsSubscriptionSubscriptionTemplateDynamicField.md) | The merged dynamic fields of the chosen subscription templates | [optional] 
**subscription_templates** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionSubscriptionTemplate[]**](LastSeatsGeneralObjectsSubscriptionSubscriptionTemplate.md) | The new list of subscription templates | [optional] 
**discount_pricing** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDtosDiscountPricingDto[]**](LastSeatsGeneralObjectsSubscriptionDtosDiscountPricingDto.md) | The list of discounts and amounts. | [optional] 
**reservation_costs_amount** | **double** | The reservation costs in the currency of the subscription template. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

