# STCTicketsApiModelsConfirmReservationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**portal_edit_subscription_url** | **string** |  | [optional] 
**key** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

