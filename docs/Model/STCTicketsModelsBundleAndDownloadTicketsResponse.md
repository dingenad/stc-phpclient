# STCTicketsModelsBundleAndDownloadTicketsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | [**\Swagger\Client\Model\SystemWebMvcFileContentResult**](SystemWebMvcFileContentResult.md) |  | [optional] 
**bytes** | **string** |  | [optional] 
**filename** | **string** |  | [optional] 
**succeeded** | **bool** |  | [optional] 
**error_message** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

