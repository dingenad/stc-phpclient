# Swagger\Client\ScanApiApi

All URIs are relative to *https://ticketstest.ticketcounter.nl/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**scanApiClaimTicketCode**](ScanApiApi.md#scanApiClaimTicketCode) | **POST** /api/v1/ticketcode/{ticketCode}/claim/{languageCode} | Claim ticket code
[**scanApiClaimTicketCode_0**](ScanApiApi.md#scanApiClaimTicketCode_0) | **POST** /api/v1/ticketcode/{ticketCode}/check/{languageCode} | Check ticket code
[**scanApiGetPartnerScanAmounts**](ScanApiApi.md#scanApiGetPartnerScanAmounts) | **GET** /api/v1/scanner/{deviceId}/partner/scans/today | Get today&#x27;s scan amounts for partner (across all sources)
[**scanApiGetScanConfiguration**](ScanApiApi.md#scanApiGetScanConfiguration) | **GET** /api/v1/scanner/{deviceId}/configuration/{configurationKey}/{languageCode} | Connect scanner to configuration and get configuration details
[**scanApiGetTicketCodeScanInfo**](ScanApiApi.md#scanApiGetTicketCodeScanInfo) | **GET** /api/v1/scanner/{deviceId}/ticketcode/info/{languageCode} | Retrieve ticket code data for local validation
[**scanApiInsertTicketScan**](ScanApiApi.md#scanApiInsertTicketScan) | **POST** /api/v1/scanner/{deviceId}/ticketcode/{ticketCode}/offline/scan | Inform system about locally validated ticket scan
[**scanApiScanDeviceCheckTicketCode**](ScanApiApi.md#scanApiScanDeviceCheckTicketCode) | **POST** /api/v1/scanner/{deviceId}/ticketcode/{ticketCode}/check/{languageCode} | Check ticket code using scan device
[**scanApiScanDeviceClaimTicketCode**](ScanApiApi.md#scanApiScanDeviceClaimTicketCode) | **POST** /api/v1/scanner/{deviceId}/ticketcode/{ticketCode}/claim/{languageCode} | Claim ticket code using scan device
[**scanApiUpdateScanDeviceInfo**](ScanApiApi.md#scanApiUpdateScanDeviceInfo) | **POST** /api/v1/scanner/{deviceId}/info | Send scan device information

# **scanApiClaimTicketCode**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanTicketCodeResult scanApiClaimTicketCode($ticket_code, $language_code)

Claim ticket code

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ScanApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ticket_code = "ticket_code_example"; // string | Ticket code to claim
$language_code = "language_code_example"; // string | Desired language for translatable information. Example: <code>nl-NL</code>

try {
    $result = $apiInstance->scanApiClaimTicketCode($ticket_code, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ScanApiApi->scanApiClaimTicketCode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticket_code** | [**string**](../Model/.md)| Ticket code to claim |
 **language_code** | [**string**](../Model/.md)| Desired language for translatable information. Example: &lt;code&gt;nl-NL&lt;/code&gt; |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanTicketCodeResult**](../Model/LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanTicketCodeResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **scanApiClaimTicketCode_0**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanTicketCodeResult scanApiClaimTicketCode_0($ticket_code, $language_code)

Check ticket code

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ScanApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ticket_code = "ticket_code_example"; // string | Ticket code to check
$language_code = "language_code_example"; // string | Desired language for translatable information. Example: <code>nl-NL</code>

try {
    $result = $apiInstance->scanApiClaimTicketCode_0($ticket_code, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ScanApiApi->scanApiClaimTicketCode_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticket_code** | [**string**](../Model/.md)| Ticket code to check |
 **language_code** | [**string**](../Model/.md)| Desired language for translatable information. Example: &lt;code&gt;nl-NL&lt;/code&gt; |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanTicketCodeResult**](../Model/LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanTicketCodeResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **scanApiGetPartnerScanAmounts**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSPartnerScanAmounts scanApiGetPartnerScanAmounts($device_id)

Get today's scan amounts for partner (across all sources)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ScanApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$device_id = "device_id_example"; // string | UID of scan device

try {
    $result = $apiInstance->scanApiGetPartnerScanAmounts($device_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ScanApiApi->scanApiGetPartnerScanAmounts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **device_id** | [**string**](../Model/.md)| UID of scan device |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSPartnerScanAmounts**](../Model/LastSeatsGeneralObjectsTicketServiceClassesScanningTSPartnerScanAmounts.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **scanApiGetScanConfiguration**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanConfiguration scanApiGetScanConfiguration($device_id, $configuration_key, $language_code)

Connect scanner to configuration and get configuration details

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ScanApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$device_id = "device_id_example"; // string | UID of scan device
$configuration_key = "configuration_key_example"; // string | UID of configuration
$language_code = "language_code_example"; // string | Desired language for translatable information. Example: <code>nl-NL</code>

try {
    $result = $apiInstance->scanApiGetScanConfiguration($device_id, $configuration_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ScanApiApi->scanApiGetScanConfiguration: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **device_id** | [**string**](../Model/.md)| UID of scan device |
 **configuration_key** | [**string**](../Model/.md)| UID of configuration |
 **language_code** | [**string**](../Model/.md)| Desired language for translatable information. Example: &lt;code&gt;nl-NL&lt;/code&gt; |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanConfiguration**](../Model/LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanConfiguration.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **scanApiGetTicketCodeScanInfo**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSTicketCodeScanInfoResult scanApiGetTicketCodeScanInfo($device_id, $language_code, $oldest_timestamp, $newest_timestamp)

Retrieve ticket code data for local validation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ScanApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$device_id = "device_id_example"; // string | UID of scan device
$language_code = "language_code_example"; // string | Desired language for translatable information. Example: <code>nl-NL</code>
$oldest_timestamp = "oldest_timestamp_example"; // string | Timestamp of oldest ticket code record present on device
$newest_timestamp = "newest_timestamp_example"; // string | Timestamp of newest ticket code record present on device

try {
    $result = $apiInstance->scanApiGetTicketCodeScanInfo($device_id, $language_code, $oldest_timestamp, $newest_timestamp);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ScanApiApi->scanApiGetTicketCodeScanInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **device_id** | [**string**](../Model/.md)| UID of scan device |
 **language_code** | [**string**](../Model/.md)| Desired language for translatable information. Example: &lt;code&gt;nl-NL&lt;/code&gt; |
 **oldest_timestamp** | [**string**](../Model/.md)| Timestamp of oldest ticket code record present on device | [optional]
 **newest_timestamp** | [**string**](../Model/.md)| Timestamp of newest ticket code record present on device | [optional]

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSTicketCodeScanInfoResult**](../Model/LastSeatsGeneralObjectsTicketServiceClassesScanningTSTicketCodeScanInfoResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **scanApiInsertTicketScan**
> object scanApiInsertTicketScan($body$device_id, $ticket_code)

Inform system about locally validated ticket scan

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ScanApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSTicketScan(); // \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSTicketScan | Additional information about ticket scan
$device_id = "device_id_example"; // string | UID of scan device
$ticket_code = "ticket_code_example"; // string | Scanned ticket code

try {
    $result = $apiInstance->scanApiInsertTicketScan($body$device_id, $ticket_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ScanApiApi->scanApiInsertTicketScan: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSTicketScan**](../Model/LastSeatsGeneralObjectsTicketServiceClassesScanningTSTicketScan.md)| Additional information about ticket scan |
 **device_id** | [**string**](../Model/.md)| UID of scan device |
 **ticket_code** | [**string**](../Model/.md)| Scanned ticket code |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **scanApiScanDeviceCheckTicketCode**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanTicketCodeResult scanApiScanDeviceCheckTicketCode($device_id, $ticket_code, $language_code)

Check ticket code using scan device

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ScanApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$device_id = "device_id_example"; // string | UID of scan device
$ticket_code = "ticket_code_example"; // string | Ticket code to check
$language_code = "language_code_example"; // string | Desired language for translatable information. Example: <code>nl-NL</code>

try {
    $result = $apiInstance->scanApiScanDeviceCheckTicketCode($device_id, $ticket_code, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ScanApiApi->scanApiScanDeviceCheckTicketCode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **device_id** | [**string**](../Model/.md)| UID of scan device |
 **ticket_code** | [**string**](../Model/.md)| Ticket code to check |
 **language_code** | [**string**](../Model/.md)| Desired language for translatable information. Example: &lt;code&gt;nl-NL&lt;/code&gt; |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanTicketCodeResult**](../Model/LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanTicketCodeResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **scanApiScanDeviceClaimTicketCode**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanTicketCodeResult scanApiScanDeviceClaimTicketCode($device_id, $ticket_code, $language_code)

Claim ticket code using scan device

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ScanApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$device_id = "device_id_example"; // string | UID of scan device
$ticket_code = "ticket_code_example"; // string | Ticket code to claim
$language_code = "language_code_example"; // string | Desired language for translatable information. Example: <code>nl-NL</code>

try {
    $result = $apiInstance->scanApiScanDeviceClaimTicketCode($device_id, $ticket_code, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ScanApiApi->scanApiScanDeviceClaimTicketCode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **device_id** | [**string**](../Model/.md)| UID of scan device |
 **ticket_code** | [**string**](../Model/.md)| Ticket code to claim |
 **language_code** | [**string**](../Model/.md)| Desired language for translatable information. Example: &lt;code&gt;nl-NL&lt;/code&gt; |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanTicketCodeResult**](../Model/LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanTicketCodeResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **scanApiUpdateScanDeviceInfo**
> object scanApiUpdateScanDeviceInfo($body$device_id)

Send scan device information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ScanApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanDeviceInfo(); // \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanDeviceInfo | Information about scan device
$device_id = "device_id_example"; // string | UID of scan device

try {
    $result = $apiInstance->scanApiUpdateScanDeviceInfo($body$device_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ScanApiApi->scanApiUpdateScanDeviceInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanDeviceInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesScanningTSScanDeviceInfo.md)| Information about scan device |
 **device_id** | [**string**](../Model/.md)| UID of scan device |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

