# Swagger\Client\TicketApiApi

All URIs are relative to *https://ticketstest.ticketcounter.nl/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ticketApiAddDiscountCodeToBasket**](TicketApiApi.md#ticketApiAddDiscountCodeToBasket) | **POST** /api/v1/basket/discount/{discountCode}/add | Add discount to the reservations in the basket if discount is valid.
[**ticketApiAddExtendSubscriptionToBasket**](TicketApiApi.md#ticketApiAddExtendSubscriptionToBasket) | **POST** /api/v1/basket/{basketKey}/reservations/extendsubscription | Create a reservation for n extend subscription and add it to the basket.
[**ticketApiAddPriceToReservation**](TicketApiApi.md#ticketApiAddPriceToReservation) | **POST** /api/v1/reservation/{reservationKey}/addprice | Add a price to an existing reservation
[**ticketApiAddReservationsToBasket**](TicketApiApi.md#ticketApiAddReservationsToBasket) | **POST** /api/v1/basket/{basketKey}/reservations/add | Add one or more reservations to a basket
[**ticketApiCancelBasket**](TicketApiApi.md#ticketApiCancelBasket) | **POST** /api/v1/basket/{basketKey}/cancel/{languageCode} | Cancel a basket
[**ticketApiCancelReservation**](TicketApiApi.md#ticketApiCancelReservation) | **POST** /api/v1/reservation/{reservationKey}/cancel/{languageCode} | Cancel a reservation
[**ticketApiCashierSSOUrl**](TicketApiApi.md#ticketApiCashierSSOUrl) | **GET** /api/v1/CashierSSOUrl | 
[**ticketApiChangePassword**](TicketApiApi.md#ticketApiChangePassword) | **POST** /api/v1/contact/{contactKey}/password/change | Change a contact password
[**ticketApiCheckCanRenewSubscription**](TicketApiApi.md#ticketApiCheckCanRenewSubscription) | **POST** /api/v1/renewsubscription/{languageCode}/check | Check if a subscription can be renewed.
[**ticketApiCheckInvitationCodes**](TicketApiApi.md#ticketApiCheckInvitationCodes) | **POST** /api/v1/partner/{partnerKey}/invitation/check | Checks a list of invitation codes for a specific partner
[**ticketApiCheckSalesAllowed**](TicketApiApi.md#ticketApiCheckSalesAllowed) | **GET** /api/v1/event/{eventKey}/sales/allowed | Check if sales are allowed
[**ticketApiCheckSeatsSelected**](TicketApiApi.md#ticketApiCheckSeatsSelected) | **GET** /api/v1/reservation/{reservationKey}/seatsSelected | Check if the client has selected the right amount of seats yet in case of placed seating
[**ticketApiCheckTicketCodeByEvent**](TicketApiApi.md#ticketApiCheckTicketCodeByEvent) | **POST** /api/v1/event/{eventKey}/ticketcode/check/{languageCode} | Check the ticketcode by event
[**ticketApiCheckTicketCodeByEventAndPerformance**](TicketApiApi.md#ticketApiCheckTicketCodeByEventAndPerformance) | **POST** /api/v1/event/{eventKey}/performance/{performanceKey}/ticketcode/check/{languageCode} | Check the ticketcode
[**ticketApiCheckTicketCodeByEventAndPerformanceAndSection**](TicketApiApi.md#ticketApiCheckTicketCodeByEventAndPerformanceAndSection) | **POST** /api/v1/event/{eventKey}/performance/{performanceKey}/section/{performanceSectionKey}/ticketcode/check/{languageCode} | Check the ticketcode
[**ticketApiCheckTicketCodeByPartner**](TicketApiApi.md#ticketApiCheckTicketCodeByPartner) | **POST** /api/v1/partner/{partnerKey}/ticketcode/check/{languageCode} | Check the ticketcode by partner
[**ticketApiCheckTicketCodeByPerformance**](TicketApiApi.md#ticketApiCheckTicketCodeByPerformance) | **POST** /api/v1/performance/{performanceKey}/ticketcode/check/{languageCode} | Check the ticketcode by performance
[**ticketApiCheckTicketCodeBySection**](TicketApiApi.md#ticketApiCheckTicketCodeBySection) | **POST** /api/v1/section/{performanceSectionKey}/ticketcode/check/{languageCode} | Check the ticketcode by performance section
[**ticketApiClaimInvitationCodes**](TicketApiApi.md#ticketApiClaimInvitationCodes) | **POST** /api/v1/partner/{partnerKey}/invitation/{reservationKey}/claim | Claims a list of invitation codes for a specific reservation
[**ticketApiClaimTicketCodeByEvent**](TicketApiApi.md#ticketApiClaimTicketCodeByEvent) | **POST** /api/v1/event/{eventKey}/ticketcode/claim/{languageCode} | Claim/scan a ticket code for a specific event
[**ticketApiClaimTicketCodeByEventAndPerformance**](TicketApiApi.md#ticketApiClaimTicketCodeByEventAndPerformance) | **POST** /api/v1/event/{eventKey}/performance/{performanceKey}/ticketcode/claim/{languageCode} | Claim/scan a ticket code for a specific event and performance
[**ticketApiClaimTicketCodeByEventAndPerformanceAndSection**](TicketApiApi.md#ticketApiClaimTicketCodeByEventAndPerformanceAndSection) | **POST** /api/v1/event/{eventKey}/performance/{performanceKey}/section/{performanceSectionKey}/ticketcode/claim/{languageCode} | Claim/scan a ticket code for a specific event, performance and performance section
[**ticketApiClaimTicketCodeByPartner**](TicketApiApi.md#ticketApiClaimTicketCodeByPartner) | **POST** /api/v1/partner/{partnerKey}/ticketcode/claim/{languageCode} | Claim/scan a ticket code for a specific partner
[**ticketApiClaimTicketCodeByPerformance**](TicketApiApi.md#ticketApiClaimTicketCodeByPerformance) | **POST** /api/v1/performance/{performanceKey}/ticketcode/claim/{languageCode} | Claim/scan a ticket code for a specific performance
[**ticketApiClaimTicketCodeBySection**](TicketApiApi.md#ticketApiClaimTicketCodeBySection) | **POST** /api/v1/section/{performanceSectionKey}/ticketcode/claim/{languageCode} | Claim/scan a ticket code for a specific performance
[**ticketApiConfirmBasket**](TicketApiApi.md#ticketApiConfirmBasket) | **POST** /api/v1/basket/{basketKey}/confirm | Confirm a basket
[**ticketApiConfirmReservation**](TicketApiApi.md#ticketApiConfirmReservation) | **POST** /api/v1/reservation/{reservationKey}/confirm | Confirm a reservation
[**ticketApiCreateBasket**](TicketApiApi.md#ticketApiCreateBasket) | **POST** /api/v1/basket/{languageCode} | Create a basket
[**ticketApiCreateEventReview**](TicketApiApi.md#ticketApiCreateEventReview) | **PUT** /api/v1/event/{eventKey}/review | Post a review for a specific event
[**ticketApiCreateLocationReview**](TicketApiApi.md#ticketApiCreateLocationReview) | **PUT** /api/v1/location/{locationKey}/review | Post a review for a specific location
[**ticketApiCreateOnePageCheckOutReservation**](TicketApiApi.md#ticketApiCreateOnePageCheckOutReservation) | **POST** /api/v1/event/{eventKey}/performance/{performanceKey}/section/{performanceSectionKey}/onepagecheckoutreservation/{languageCode} | Create a reservation with a payment for the onepage checkout
[**ticketApiCreateOnePageCheckOutReservationWithoutPayment**](TicketApiApi.md#ticketApiCreateOnePageCheckOutReservationWithoutPayment) | **POST** /api/v1/event/{eventKey}/performance/{performanceKey}/section/{performanceSectionKey}/onepagecheckoutreservationwithoutpayment/{languageCode} | Create a reservation without creating a payment for the onepage checkout
[**ticketApiCreatePerformanceReview**](TicketApiApi.md#ticketApiCreatePerformanceReview) | **PUT** /api/v1/event/{eventKey}/performance/{performanceKey}/review | Post a review for a specific performance
[**ticketApiCreateReservation**](TicketApiApi.md#ticketApiCreateReservation) | **POST** /api/v1/reservations/{languageCode} | Create a reservation
[**ticketApiCreateReservationByPriceKeys**](TicketApiApi.md#ticketApiCreateReservationByPriceKeys) | **POST** /api/v1/pricekeys/reservation/{languageCode} | Create a reservation for one or more price keys
[**ticketApiCreateReservationBySection**](TicketApiApi.md#ticketApiCreateReservationBySection) | **POST** /api/v1/event/{eventKey}/performance/{performanceKey}/section/{performanceSectionKey}/reservation/{languageCode} | Create a reservation
[**ticketApiGetAllEvents**](TicketApiApi.md#ticketApiGetAllEvents) | **GET** /api/v1/events/all/{languageCode} | Retrieve all events without their descriptions
[**ticketApiGetAllEventsWithDescriptions**](TicketApiApi.md#ticketApiGetAllEventsWithDescriptions) | **GET** /api/v1/events/all/full/{languageCode} | Retrieve all events with their descriptions
[**ticketApiGetAvailableEvents**](TicketApiApi.md#ticketApiGetAvailableEvents) | **GET** /api/v1/events/available/{languageCode} | Retrieve available events without their descriptions
[**ticketApiGetAvailableEventsWithDescriptions**](TicketApiApi.md#ticketApiGetAvailableEventsWithDescriptions) | **GET** /api/v1/events/available/full/{languageCode} | Retrieve available events with their descriptions
[**ticketApiGetAvailableSeats**](TicketApiApi.md#ticketApiGetAvailableSeats) | **GET** /api/v1/event/{eventKey}/performance/{performanceKey}/section/{performanceSectionKey}/seats/available | Retrieve number of available seats
[**ticketApiGetAvailableSections**](TicketApiApi.md#ticketApiGetAvailableSections) | **GET** /api/v1/event/{eventKey}/performance/{performanceKey}/sections/{languageCode} | Retrieve sections
[**ticketApiGetBasket**](TicketApiApi.md#ticketApiGetBasket) | **GET** /api/v1/basket/{basketKey} | Get the basket info and basic info of the reservations inside the basket
[**ticketApiGetBasketForUser**](TicketApiApi.md#ticketApiGetBasketForUser) | **GET** /api/v1/basket/foruser | Get the basket info coupled to the current logged in user, if there is any.
[**ticketApiGetCapacitySlotsByPriceKeys**](TicketApiApi.md#ticketApiGetCapacitySlotsByPriceKeys) | **POST** /api/v1/capacityslots | Get capacity slots for one or more price keys
[**ticketApiGetContactByExternalContactID**](TicketApiApi.md#ticketApiGetContactByExternalContactID) | **GET** /api/v1/contact/externalcontactid/{externalAuthenticationType}/{externalContactId} | Get a contact by a contact id from an external source (Facebook, Google, etc.)
[**ticketApiGetContactInfo**](TicketApiApi.md#ticketApiGetContactInfo) | **GET** /api/v1/contact/{contactKey}/{languageCode} | Get a contact
[**ticketApiGetContactInformation**](TicketApiApi.md#ticketApiGetContactInformation) | **GET** /api/v1/contact | Get info about the current logged in user.
[**ticketApiGetContactLocationReviews**](TicketApiApi.md#ticketApiGetContactLocationReviews) | **GET** /api/v1/contact/{contactKey}/locations/reviews/{languageCode} | Get location reviews of a contact
[**ticketApiGetContactReservationReviews**](TicketApiApi.md#ticketApiGetContactReservationReviews) | **GET** /api/v1/contact/{contactKey}/events/reviews/{languageCode} | Get reviews of a specific contact
[**ticketApiGetDiscountInformation**](TicketApiApi.md#ticketApiGetDiscountInformation) | **GET** /api/v1/discount/{discountCode} | Get usage information for discount code
[**ticketApiGetEvent**](TicketApiApi.md#ticketApiGetEvent) | **GET** /api/v1/events/{eventKey}/{languageCode} | Event in the specified language
[**ticketApiGetEventCategories**](TicketApiApi.md#ticketApiGetEventCategories) | **GET** /api/v1/eventcategories/{languageCode} | Retrieve all event categories
[**ticketApiGetEventDescription**](TicketApiApi.md#ticketApiGetEventDescription) | **GET** /api/v1/events/{eventKey}/description/{languageCode} | Event description in the specified language
[**ticketApiGetExternalContactID**](TicketApiApi.md#ticketApiGetExternalContactID) | **GET** /api/v1/externalcontact/{externalAuthenticationType}/{contactKey} | Get the external contact id from an external source (Facebook, Google, etc.) by the key of the contact
[**ticketApiGetLocations**](TicketApiApi.md#ticketApiGetLocations) | **GET** /api/v1/locations/{languageCode} | Get all the locations for this sales channel
[**ticketApiGetMaximumSeatsTogether**](TicketApiApi.md#ticketApiGetMaximumSeatsTogether) | **GET** /api/v1/event/{eventKey}/performance/{performanceKey}/section/{performanceSectionKey}/seats/maxtogether | Retrieve number of available seats together
[**ticketApiGetOfflineTicketCodes**](TicketApiApi.md#ticketApiGetOfflineTicketCodes) | **GET** /api/v1/price/{priceKey}/offline/ticketcodes/{count} | Get a list of tickets that can be sold offline.
[**ticketApiGetPartnerInfo**](TicketApiApi.md#ticketApiGetPartnerInfo) | **GET** /api/v1/partner/{partnerKey} | Get info of a partner
[**ticketApiGetPaymentMethods**](TicketApiApi.md#ticketApiGetPaymentMethods) | **GET** /api/v1/event/{eventKey}/paymentmethods | Get available payment methods for event
[**ticketApiGetPriceByBranchePriceId**](TicketApiApi.md#ticketApiGetPriceByBranchePriceId) | **GET** /api/v1/price/{branchePriceId}/{languageCode} | Retrieve event, performance, section and price info by the supplied branche price id
[**ticketApiGetProducts**](TicketApiApi.md#ticketApiGetProducts) | **GET** /api/v1/products/{languageCode} | Get tickets and subscription products of the event of the saleschannel of the logged in user
[**ticketApiGetProfilePicture**](TicketApiApi.md#ticketApiGetProfilePicture) | **GET** /api/v1/subscription/profilepicture/partner/{partnerKey}/ticketcode/{ticketCode} | Get profile picture of a subscription holder
[**ticketApiGetQoute**](TicketApiApi.md#ticketApiGetQoute) | **POST** /api/v1/event/{eventKey}/performance/{performanceKey}/section/{performanceSectionKey}/quote/{languageCode} | Get reservation quote
[**ticketApiGetReservation**](TicketApiApi.md#ticketApiGetReservation) | **GET** /api/v1/reservation/{reservationKey} | Get reservation and tickets
[**ticketApiGetReservationHistory**](TicketApiApi.md#ticketApiGetReservationHistory) | **GET** /api/v1/reservationhistory/{reservationKey} | Get the history info of a reservation
[**ticketApiGetReservationTickets**](TicketApiApi.md#ticketApiGetReservationTickets) | **GET** /api/v1/reservation/{reservationKey}/tickets | Get the tickets of a reservation after the reservation is confirmed
[**ticketApiGetReservationTicketsBeforeConfirm**](TicketApiApi.md#ticketApiGetReservationTicketsBeforeConfirm) | **GET** /api/v1/reservation/{reservationKey}/unconfirmed/tickets | Get the tickets of a reservation before the reservation is confirmed
[**ticketApiGetReservations**](TicketApiApi.md#ticketApiGetReservations) | **GET** /api/v1/contact/{contactKey}/reservations/{languageCode} | Get reservations for a contact
[**ticketApiGetScannerConfiguration**](TicketApiApi.md#ticketApiGetScannerConfiguration) | **GET** /api/v1/scannerconfiguration/partner/{partnerKey}/event/{eventKey}/performance/{performanceKey}/performancesection/{performanceSectionKey}/{languageCode} | Get info of a partner
[**ticketApiGetSellerForPartners**](TicketApiApi.md#ticketApiGetSellerForPartners) | **GET** /api/v1/sellerforpartners/{sellerContactKey} | Gets seller information
[**ticketApiGetShopConfiguration**](TicketApiApi.md#ticketApiGetShopConfiguration) | **GET** /api/v1/event/{slug}/webshop/configuration | Get shop configuration settings
[**ticketApiGetShopSettings**](TicketApiApi.md#ticketApiGetShopSettings) | **GET** /api/v1/event/{eventKey}/webshop/settings | Get shop settings
[**ticketApiGetSubscriptionInformation**](TicketApiApi.md#ticketApiGetSubscriptionInformation) | **GET** /api/v1/subscription/information/reservation/{reservationKey}/subscription/{subscriptionKey}/ticketcode/{ticketCode} | 
[**ticketApiGetTop5Events**](TicketApiApi.md#ticketApiGetTop5Events) | **GET** /api/v1/events/top5/{languageCode} | Get the top 5 events in the specified language
[**ticketApiGetTop5Locations**](TicketApiApi.md#ticketApiGetTop5Locations) | **GET** /api/v1/locations/top5/{languageCode} | Get the top 5 locations
[**ticketApiGetUnAvailableEvents**](TicketApiApi.md#ticketApiGetUnAvailableEvents) | **GET** /api/v1/events/unavailable/{languageCode} | Retrieve unavailable events without their descriptions
[**ticketApiGetUnAvailableEventsWithDescriptions**](TicketApiApi.md#ticketApiGetUnAvailableEventsWithDescriptions) | **GET** /api/v1/events/unavailable/full/{languageCode} | Retrieve unavailable events with their descriptions
[**ticketApiLoginContact**](TicketApiApi.md#ticketApiLoginContact) | **GET** /api/v1/login | Login to the system using a mail address and a password
[**ticketApiNotifyFromSkiData**](TicketApiApi.md#ticketApiNotifyFromSkiData) | **POST** /api/v1/partner/{partnerKey}/ticketcode/{ticketCode}/skidata/transaction | Callback after a ticket code (or profile picture) has been sent to SkiData
[**ticketApiPing**](TicketApiApi.md#ticketApiPing) | **GET** /api/v1/ping | Use Ping only for connectionchecking
[**ticketApiPreConfirmReservation**](TicketApiApi.md#ticketApiPreConfirmReservation) | **POST** /api/v1/reservation/{reservationKey}/preconfirm | Preconfirm a reservation
[**ticketApiRemovePriceFromReservation**](TicketApiApi.md#ticketApiRemovePriceFromReservation) | **POST** /api/v1/reservation/{reservationKey}/removeprice | Remove a price from an existing reservation
[**ticketApiRemoveReservationsFromBasket**](TicketApiApi.md#ticketApiRemoveReservationsFromBasket) | **POST** /api/v1/basket/{basketKey}/reservations/remove | Remove one or more reservations from a basket
[**ticketApiRenewSubscription**](TicketApiApi.md#ticketApiRenewSubscription) | **POST** /api/v1/renewsubscription/{languageCode}/renew | Renew a subscription. Result is a &lt;code&gt;CreateReservationResponse&lt;/code&gt;.
[**ticketApiRequestPaymentUrl**](TicketApiApi.md#ticketApiRequestPaymentUrl) | **POST** /api/v1/reservation/{reservationKey}/payment/{languageCode} | Start payment for reservation and request payment url
[**ticketApiSaveContactInfo**](TicketApiApi.md#ticketApiSaveContactInfo) | **POST** /api/v1/contact/{languageCode} | Create contact
[**ticketApiSaveExternalContactID**](TicketApiApi.md#ticketApiSaveExternalContactID) | **POST** /api/v1/externalcontact/{externalAuthenticationType}/{contactKey}/{externalContactId} | Connects the external contact id from an external source (Facebook, Google, etc.) to the key of the contact
[**ticketApiSetBasketContact**](TicketApiApi.md#ticketApiSetBasketContact) | **POST** /api/v1/basket/{basketKey}/contact | Connect a contact to a basket and to all the reservations within the basket that have no contact connected (yet)
[**ticketApiSetBasketContactByContactKey**](TicketApiApi.md#ticketApiSetBasketContactByContactKey) | **POST** /api/v1/basket/{basketKey}/contact/{contactKey} | Connect an existing contact to a basket and to all the reservations within the basket that have no contact connected (yet)
[**ticketApiSetReservationContact**](TicketApiApi.md#ticketApiSetReservationContact) | **POST** /api/v1/reservation/{reservationKey}/contact | Connect a contact to a reservation
[**ticketApiSetReservationContactByKey**](TicketApiApi.md#ticketApiSetReservationContactByKey) | **POST** /api/v1/reservation/{reservationKey}/contact/{contactKey} | Connect an existing contact to a reservation
[**ticketApiSkiDataStatusUpdate**](TicketApiApi.md#ticketApiSkiDataStatusUpdate) | **POST** /api/v1/SkiDataStatusUpdate | Update the status of the SkiData connection
[**ticketApiStartBasketPayment**](TicketApiApi.md#ticketApiStartBasketPayment) | **POST** /api/v1/basket/{basketKey}/payment | Start payment for a basket
[**ticketApiStartReservationPayment**](TicketApiApi.md#ticketApiStartReservationPayment) | **POST** /api/v1/reservation/{reservationKey}/payment | Start payment for a reservation
[**ticketApiUpdateBasket**](TicketApiApi.md#ticketApiUpdateBasket) | **PUT** /api/v1/basket/{basketKey} | Update a basket
[**ticketApiUpdateContactInfo**](TicketApiApi.md#ticketApiUpdateContactInfo) | **PUT** /api/v1/contact/{contactKey}/{languageCode} | Update a contact
[**ticketApiUpdateReservation**](TicketApiApi.md#ticketApiUpdateReservation) | **PUT** /api/v1/reservation/{reservationKey} | Change a reservation
[**ticketApiUpdateReview**](TicketApiApi.md#ticketApiUpdateReview) | **POST** /api/v1/review/{reviewKey} | Change the rating and/or review text for a specific review

# **ticketApiAddDiscountCodeToBasket**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo ticketApiAddDiscountCodeToBasket($discount_code)

Add discount to the reservations in the basket if discount is valid.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$discount_code = "discount_code_example"; // string | Discount code to validate on the present reservations in the basket

try {
    $result = $apiInstance->ticketApiAddDiscountCodeToBasket($discount_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiAddDiscountCodeToBasket: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **discount_code** | [**string**](../Model/.md)| Discount code to validate on the present reservations in the basket |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiAddExtendSubscriptionToBasket**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo ticketApiAddExtendSubscriptionToBasket($basket_key, $subscription_id, $renewal_subscription_template_key, $language_code)

Create a reservation for n extend subscription and add it to the basket.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$basket_key = "basket_key_example"; // string | The key of the basket. Example: <code>5242A995-D891-4DDB-AB97-5387BCE638CC</code>.
$subscription_id = 56; // int | The subscription that will be extended
$renewal_subscription_template_key = "renewal_subscription_template_key_example"; // string | 
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiAddExtendSubscriptionToBasket($basket_key, $subscription_id, $renewal_subscription_template_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiAddExtendSubscriptionToBasket: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **basket_key** | [**string**](../Model/.md)| The key of the basket. Example: &lt;code&gt;5242A995-D891-4DDB-AB97-5387BCE638CC&lt;/code&gt;. |
 **subscription_id** | [**int**](../Model/.md)| The subscription that will be extended |
 **renewal_subscription_template_key** | [**string**](../Model/.md)|  | [optional]
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. | [optional]

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiAddPriceToReservation**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo ticketApiAddPriceToReservation($body$reservation_key)

Add a price to an existing reservation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeRequestInfo(); // \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeRequestInfo | The data of the price type that must be added to this reservation.
$reservation_key = "reservation_key_example"; // string | The key of the reservation. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.

try {
    $result = $apiInstance->ticketApiAddPriceToReservation($body$reservation_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiAddPriceToReservation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeRequestInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeRequestInfo.md)| The data of the price type that must be added to this reservation. |
 **reservation_key** | [**string**](../Model/.md)| The key of the reservation. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiAddReservationsToBasket**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo ticketApiAddReservationsToBasket($body$basket_key, $include_full_reservation_info)

Add one or more reservations to a basket

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = array("body_example"); // string[] | The list of keys of the reservations that must be added to the list.
$basket_key = "basket_key_example"; // string | The key of the basket. Example: <code>5242A995-D891-4DDB-AB97-5387BCE638CC</code>.
$include_full_reservation_info = True; // bool | Optional: get the full reservation info of the items in the basket.

try {
    $result = $apiInstance->ticketApiAddReservationsToBasket($body$basket_key, $include_full_reservation_info);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiAddReservationsToBasket: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**string[]**](../Model/string.md)| The list of keys of the reservations that must be added to the list. |
 **basket_key** | [**string**](../Model/.md)| The key of the basket. Example: &lt;code&gt;5242A995-D891-4DDB-AB97-5387BCE638CC&lt;/code&gt;. |
 **include_full_reservation_info** | [**bool**](../Model/.md)| Optional: get the full reservation info of the items in the basket. | [optional]

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCancelBasket**
> object ticketApiCancelBasket($basket_key, $language_code, $allow_confirmed_reservations, $allow_used_tickets)

Cancel a basket

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$basket_key = "basket_key_example"; // string | The key of the basket. Example: <code>5242A995-D891-4DDB-AB97-5387BCE638CC</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.
$allow_confirmed_reservations = True; // bool | Indicate whether the basket should be cancelled even if it is already confirmed. Requires additional permissions.
$allow_used_tickets = True; // bool | Indicate whether the basket should be cancelled even if (some of) its tickets have been used. Requires additional permissions.

try {
    $result = $apiInstance->ticketApiCancelBasket($basket_key, $language_code, $allow_confirmed_reservations, $allow_used_tickets);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCancelBasket: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **basket_key** | [**string**](../Model/.md)| The key of the basket. Example: &lt;code&gt;5242A995-D891-4DDB-AB97-5387BCE638CC&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |
 **allow_confirmed_reservations** | [**bool**](../Model/.md)| Indicate whether the basket should be cancelled even if it is already confirmed. Requires additional permissions. | [optional]
 **allow_used_tickets** | [**bool**](../Model/.md)| Indicate whether the basket should be cancelled even if (some of) its tickets have been used. Requires additional permissions. | [optional]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCancelReservation**
> object ticketApiCancelReservation($reservation_key, $language_code, $allow_confirmed_reservations, $allow_used_tickets)

Cancel a reservation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$reservation_key = "reservation_key_example"; // string | The key of the reservation. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.
$allow_confirmed_reservations = True; // bool | Indicate whether the reservation should be cancelled even if it is already confirmed. Requires additional permissions.
$allow_used_tickets = True; // bool | Indicate whether the reservation should be cancelled even if (some of) its tickets have been used. Requires additional permissions.

try {
    $result = $apiInstance->ticketApiCancelReservation($reservation_key, $language_code, $allow_confirmed_reservations, $allow_used_tickets);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCancelReservation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reservation_key** | [**string**](../Model/.md)| The key of the reservation. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |
 **allow_confirmed_reservations** | [**bool**](../Model/.md)| Indicate whether the reservation should be cancelled even if it is already confirmed. Requires additional permissions. | [optional]
 **allow_used_tickets** | [**bool**](../Model/.md)| Indicate whether the reservation should be cancelled even if (some of) its tickets have been used. Requires additional permissions. | [optional]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCashierSSOUrl**
> string ticketApiCashierSSOUrl()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->ticketApiCashierSSOUrl();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCashierSSOUrl: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiChangePassword**
> object ticketApiChangePassword($body$contact_key)

Change a contact password

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSChangePassword(); // \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSChangePassword | The old and new password
$contact_key = "contact_key_example"; // string | The key of the contact. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.

try {
    $result = $apiInstance->ticketApiChangePassword($body$contact_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiChangePassword: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSChangePassword**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSChangePassword.md)| The old and new password |
 **contact_key** | [**string**](../Model/.md)| The key of the contact. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCheckCanRenewSubscription**
> \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesRenewalSubscriptionResponse ticketApiCheckCanRenewSubscription($body$language_code)

Check if a subscription can be renewed.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel(); // \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel | the ticketcode information of the subscription to check
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiCheckCanRenewSubscription($body$language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCheckCanRenewSubscription: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel**](../Model/STCTicketsApiModelsTicketCodeModel.md)| the ticketcode information of the subscription to check |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesRenewalSubscriptionResponse**](../Model/LastSeatsGeneralObjectsSubscriptionMessagesRenewalSubscriptionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCheckInvitationCodes**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSInvitationInfo ticketApiCheckInvitationCodes($body$partner_key)

Checks a list of invitation codes for a specific partner

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = array("body_example"); // string[] | The list of codes that must be claimed
$partner_key = "partner_key_example"; // string | The key of the partner that we want to check the invitation for

try {
    $result = $apiInstance->ticketApiCheckInvitationCodes($body$partner_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCheckInvitationCodes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**string[]**](../Model/string.md)| The list of codes that must be claimed |
 **partner_key** | [**string**](../Model/.md)| The key of the partner that we want to check the invitation for |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSInvitationInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSInvitationInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCheckSalesAllowed**
> bool ticketApiCheckSalesAllowed($event_key, $body)

Check if sales are allowed

Checks if sales are allowed for a specific country using the IP adddress

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$event_key = "event_key_example"; // string | The key of the event. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$body = "body_example"; // string | The ip address that we want to check. Example: <code>192.168.0.1</code>.

try {
    $result = $apiInstance->ticketApiCheckSalesAllowed($event_key, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCheckSalesAllowed: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **body** | [**string**](../Model/.md)| The ip address that we want to check. Example: &lt;code&gt;192.168.0.1&lt;/code&gt;. |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCheckSeatsSelected**
> bool ticketApiCheckSeatsSelected($reservation_key)

Check if the client has selected the right amount of seats yet in case of placed seating

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$reservation_key = "reservation_key_example"; // string | The key of the reservation. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.

try {
    $result = $apiInstance->ticketApiCheckSeatsSelected($reservation_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCheckSeatsSelected: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reservation_key** | [**string**](../Model/.md)| The key of the reservation. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCheckTicketCodeByEvent**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo ticketApiCheckTicketCodeByEvent($body$event_key, $language_code)

Check the ticketcode by event

Check if a ticket code of a specific event is still valid

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel(); // \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel | The ticketcode data
$event_key = "event_key_example"; // string | The key of the event. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiCheckTicketCodeByEvent($body$event_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCheckTicketCodeByEvent: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel**](../Model/STCTicketsApiModelsTicketCodeModel.md)| The ticketcode data |
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCheckTicketCodeByEventAndPerformance**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo ticketApiCheckTicketCodeByEventAndPerformance($body$event_key, $performance_key, $language_code)

Check the ticketcode

Check if a ticket code of a specific performance is still valid

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel(); // \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel | The ticketcode data
$event_key = "event_key_example"; // string | The key of the event. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$performance_key = "performance_key_example"; // string | The key of the performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiCheckTicketCodeByEventAndPerformance($body$event_key, $performance_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCheckTicketCodeByEventAndPerformance: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel**](../Model/STCTicketsApiModelsTicketCodeModel.md)| The ticketcode data |
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **performance_key** | [**string**](../Model/.md)| The key of the performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCheckTicketCodeByEventAndPerformanceAndSection**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo ticketApiCheckTicketCodeByEventAndPerformanceAndSection($body$event_key, $performance_key, $performance_section_key, $language_code)

Check the ticketcode

Check if a ticket code of a specific performance and section is still valid

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel(); // \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel | The ticketcode data
$event_key = "event_key_example"; // string | The key of the event. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$performance_key = "performance_key_example"; // string | The key of the performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.
$performance_section_key = "performance_section_key_example"; // string | The key of the section within a performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiCheckTicketCodeByEventAndPerformanceAndSection($body$event_key, $performance_key, $performance_section_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCheckTicketCodeByEventAndPerformanceAndSection: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel**](../Model/STCTicketsApiModelsTicketCodeModel.md)| The ticketcode data |
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **performance_key** | [**string**](../Model/.md)| The key of the performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |
 **performance_section_key** | [**string**](../Model/.md)| The key of the section within a performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCheckTicketCodeByPartner**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo ticketApiCheckTicketCodeByPartner($body$partner_key, $language_code)

Check the ticketcode by partner

Check if a ticket code of a specific partner is still valid

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel(); // \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel | The ticketcode data
$partner_key = "partner_key_example"; // string | The key of the partner. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiCheckTicketCodeByPartner($body$partner_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCheckTicketCodeByPartner: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel**](../Model/STCTicketsApiModelsTicketCodeModel.md)| The ticketcode data |
 **partner_key** | [**string**](../Model/.md)| The key of the partner. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCheckTicketCodeByPerformance**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo ticketApiCheckTicketCodeByPerformance($body$performance_key, $language_code)

Check the ticketcode by performance

Check if a ticket code of a specific performance is still valid

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel(); // \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel | The ticketcode data
$performance_key = "performance_key_example"; // string | The key of the performance. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiCheckTicketCodeByPerformance($body$performance_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCheckTicketCodeByPerformance: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel**](../Model/STCTicketsApiModelsTicketCodeModel.md)| The ticketcode data |
 **performance_key** | [**string**](../Model/.md)| The key of the performance. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCheckTicketCodeBySection**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo ticketApiCheckTicketCodeBySection($body$performance_section_key, $language_code)

Check the ticketcode by performance section

Check if a ticket code of a specific performance section is still valid

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel(); // \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel | The ticketcode data
$performance_section_key = "performance_section_key_example"; // string | The key of the performance section. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiCheckTicketCodeBySection($body$performance_section_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCheckTicketCodeBySection: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel**](../Model/STCTicketsApiModelsTicketCodeModel.md)| The ticketcode data |
 **performance_section_key** | [**string**](../Model/.md)| The key of the performance section. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiClaimInvitationCodes**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSInvitationInfo ticketApiClaimInvitationCodes($body$partner_key, $reservation_key)

Claims a list of invitation codes for a specific reservation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = array("body_example"); // string[] | The list of codes that must be claimed
$partner_key = "partner_key_example"; // string | The key of the partner that we want to check the invitation for
$reservation_key = "reservation_key_example"; // string | The key of the reservation

try {
    $result = $apiInstance->ticketApiClaimInvitationCodes($body$partner_key, $reservation_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiClaimInvitationCodes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**string[]**](../Model/string.md)| The list of codes that must be claimed |
 **partner_key** | [**string**](../Model/.md)| The key of the partner that we want to check the invitation for |
 **reservation_key** | [**string**](../Model/.md)| The key of the reservation |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSInvitationInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSInvitationInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiClaimTicketCodeByEvent**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo ticketApiClaimTicketCodeByEvent($body$event_key, $language_code)

Claim/scan a ticket code for a specific event

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel(); // \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel | The ticketcode data
$event_key = "event_key_example"; // string | The key of the event. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiClaimTicketCodeByEvent($body$event_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiClaimTicketCodeByEvent: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel**](../Model/STCTicketsApiModelsTicketCodeModel.md)| The ticketcode data |
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiClaimTicketCodeByEventAndPerformance**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo ticketApiClaimTicketCodeByEventAndPerformance($body$event_key, $performance_key, $language_code)

Claim/scan a ticket code for a specific event and performance

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel(); // \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel | The ticketcode data
$event_key = "event_key_example"; // string | The key of the event. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$performance_key = "performance_key_example"; // string | The key of the performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiClaimTicketCodeByEventAndPerformance($body$event_key, $performance_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiClaimTicketCodeByEventAndPerformance: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel**](../Model/STCTicketsApiModelsTicketCodeModel.md)| The ticketcode data |
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **performance_key** | [**string**](../Model/.md)| The key of the performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiClaimTicketCodeByEventAndPerformanceAndSection**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo ticketApiClaimTicketCodeByEventAndPerformanceAndSection($body$event_key, $performance_key, $performance_section_key, $language_code)

Claim/scan a ticket code for a specific event, performance and performance section

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel(); // \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel | The ticketcode data
$event_key = "event_key_example"; // string | The key of the event. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$performance_key = "performance_key_example"; // string | The key of the performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.
$performance_section_key = "performance_section_key_example"; // string | The key of the section within a performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiClaimTicketCodeByEventAndPerformanceAndSection($body$event_key, $performance_key, $performance_section_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiClaimTicketCodeByEventAndPerformanceAndSection: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel**](../Model/STCTicketsApiModelsTicketCodeModel.md)| The ticketcode data |
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **performance_key** | [**string**](../Model/.md)| The key of the performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |
 **performance_section_key** | [**string**](../Model/.md)| The key of the section within a performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiClaimTicketCodeByPartner**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo ticketApiClaimTicketCodeByPartner($body$partner_key, $language_code)

Claim/scan a ticket code for a specific partner

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel(); // \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel | The ticketcode data
$partner_key = "partner_key_example"; // string | The key of the partner. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiClaimTicketCodeByPartner($body$partner_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiClaimTicketCodeByPartner: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel**](../Model/STCTicketsApiModelsTicketCodeModel.md)| The ticketcode data |
 **partner_key** | [**string**](../Model/.md)| The key of the partner. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiClaimTicketCodeByPerformance**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo ticketApiClaimTicketCodeByPerformance($body$performance_key, $language_code)

Claim/scan a ticket code for a specific performance

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel(); // \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel | The ticketcode data
$performance_key = "performance_key_example"; // string | The key of the performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiClaimTicketCodeByPerformance($body$performance_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiClaimTicketCodeByPerformance: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel**](../Model/STCTicketsApiModelsTicketCodeModel.md)| The ticketcode data |
 **performance_key** | [**string**](../Model/.md)| The key of the performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiClaimTicketCodeBySection**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo ticketApiClaimTicketCodeBySection($body$performance_section_key, $language_code)

Claim/scan a ticket code for a specific performance

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel(); // \Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel | The ticketcode data
$performance_section_key = "performance_section_key_example"; // string | The key of the performance. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiClaimTicketCodeBySection($body$performance_section_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiClaimTicketCodeBySection: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsTicketCodeModel**](../Model/STCTicketsApiModelsTicketCodeModel.md)| The ticketcode data |
 **performance_section_key** | [**string**](../Model/.md)| The key of the performance. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiConfirmBasket**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo ticketApiConfirmBasket($body$basket_key)

Confirm a basket

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsConfirmReservationModel(); // \Swagger\Client\Model\STCTicketsApiModelsConfirmReservationModel | The confirmation data
$basket_key = "basket_key_example"; // string | The key of the basket. Example: <code>5242A995-D891-4DDB-AB97-5387BCE638CC</code>.

try {
    $result = $apiInstance->ticketApiConfirmBasket($body$basket_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiConfirmBasket: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsConfirmReservationModel**](../Model/STCTicketsApiModelsConfirmReservationModel.md)| The confirmation data |
 **basket_key** | [**string**](../Model/.md)| The key of the basket. Example: &lt;code&gt;5242A995-D891-4DDB-AB97-5387BCE638CC&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiConfirmReservation**
> \Swagger\Client\Model\STCTicketsApiModelsConfirmReservationResponse ticketApiConfirmReservation($body$reservation_key)

Confirm a reservation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsConfirmReservationModel(); // \Swagger\Client\Model\STCTicketsApiModelsConfirmReservationModel | The confirmation data
$reservation_key = "reservation_key_example"; // string | The key of the reservation. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.

try {
    $result = $apiInstance->ticketApiConfirmReservation($body$reservation_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiConfirmReservation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsConfirmReservationModel**](../Model/STCTicketsApiModelsConfirmReservationModel.md)| The confirmation data |
 **reservation_key** | [**string**](../Model/.md)| The key of the reservation. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\STCTicketsApiModelsConfirmReservationResponse**](../Model/STCTicketsApiModelsConfirmReservationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCreateBasket**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo ticketApiCreateBasket($body$currency_code, $language_code, $external_basket_number, $include_logged_in_contact)

Create a basket

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = array("body_example"); // string[] | Optional: a list of keys of reservations that need to be added to this basket
$currency_code = "currency_code_example"; // string | The code of the currency that this basket will be paid in. Example: <code>EUR</code>.
$language_code = "language_code_example"; // string | Language code of the language the reservations that will be put in this basket. Example: <code>nl-NL</code>.
$external_basket_number = "external_basket_number_example"; // string | Optional: an external id for this basket.
$include_logged_in_contact = True; // bool | Optional: inlcude logged in user as FK constraint that will be the owner of the basket.

try {
    $result = $apiInstance->ticketApiCreateBasket($body$currency_code, $language_code, $external_basket_number, $include_logged_in_contact);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCreateBasket: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**string[]**](../Model/string.md)| Optional: a list of keys of reservations that need to be added to this basket |
 **currency_code** | [**string**](../Model/.md)| The code of the currency that this basket will be paid in. Example: &lt;code&gt;EUR&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the reservations that will be put in this basket. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |
 **external_basket_number** | [**string**](../Model/.md)| Optional: an external id for this basket. | [optional]
 **include_logged_in_contact** | [**bool**](../Model/.md)| Optional: inlcude logged in user as FK constraint that will be the owner of the basket. | [optional]

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCreateEventReview**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo ticketApiCreateEventReview($body$event_key)

Post a review for a specific event

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo(); // \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo | The review to add.
$event_key = "event_key_example"; // string | The key of the event. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.

try {
    $result = $apiInstance->ticketApiCreateEventReview($body$event_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCreateEventReview: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo.md)| The review to add. |
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCreateLocationReview**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo ticketApiCreateLocationReview($body$location_key)

Post a review for a specific location

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo(); // \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo | The review to add
$location_key = "location_key_example"; // string | The key of the location. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.

try {
    $result = $apiInstance->ticketApiCreateLocationReview($body$location_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCreateLocationReview: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo.md)| The review to add |
 **location_key** | [**string**](../Model/.md)| The key of the location. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCreateOnePageCheckOutReservation**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPaymentRequest ticketApiCreateOnePageCheckOutReservation($body$event_key, $performance_key, $performance_section_key, $language_code)

Create a reservation with a payment for the onepage checkout

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsOnePageCheckoutReservationModel(); // \Swagger\Client\Model\STCTicketsApiModelsOnePageCheckoutReservationModel | The reservation data
$event_key = "event_key_example"; // string | The key of the event. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$performance_key = "performance_key_example"; // string | The key of the performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.
$performance_section_key = "performance_section_key_example"; // string | The key of the section within a performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiCreateOnePageCheckOutReservation($body$event_key, $performance_key, $performance_section_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCreateOnePageCheckOutReservation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsOnePageCheckoutReservationModel**](../Model/STCTicketsApiModelsOnePageCheckoutReservationModel.md)| The reservation data |
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **performance_key** | [**string**](../Model/.md)| The key of the performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |
 **performance_section_key** | [**string**](../Model/.md)| The key of the section within a performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPaymentRequest**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSPaymentRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCreateOnePageCheckOutReservationWithoutPayment**
> object ticketApiCreateOnePageCheckOutReservationWithoutPayment($body$event_key, $performance_key, $performance_section_key, $language_code)

Create a reservation without creating a payment for the onepage checkout

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsOnePageCheckoutReservationModel(); // \Swagger\Client\Model\STCTicketsApiModelsOnePageCheckoutReservationModel | The reservation data
$event_key = "event_key_example"; // string | The key of the event. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$performance_key = "performance_key_example"; // string | The key of the performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.
$performance_section_key = "performance_section_key_example"; // string | The key of the section within a performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiCreateOnePageCheckOutReservationWithoutPayment($body$event_key, $performance_key, $performance_section_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCreateOnePageCheckOutReservationWithoutPayment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsOnePageCheckoutReservationModel**](../Model/STCTicketsApiModelsOnePageCheckoutReservationModel.md)| The reservation data |
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **performance_key** | [**string**](../Model/.md)| The key of the performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |
 **performance_section_key** | [**string**](../Model/.md)| The key of the section within a performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCreatePerformanceReview**
> object ticketApiCreatePerformanceReview($body$event_key, $performance_key)

Post a review for a specific performance

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo(); // \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo | The review to add
$event_key = "event_key_example"; // string | The key of the event. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$performance_key = "performance_key_example"; // string | The key of the performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.

try {
    $result = $apiInstance->ticketApiCreatePerformanceReview($body$event_key, $performance_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCreatePerformanceReview: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo.md)| The review to add |
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **performance_key** | [**string**](../Model/.md)| The key of the performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCreateReservation**
> \Swagger\Client\Model\STCTicketsApiModelsCreateReservationResponse ticketApiCreateReservation($body$language_code)

Create a reservation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsCreateReservationViewModel(); // \Swagger\Client\Model\STCTicketsApiModelsCreateReservationViewModel | The reservation data
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiCreateReservation($body$language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCreateReservation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsCreateReservationViewModel**](../Model/STCTicketsApiModelsCreateReservationViewModel.md)| The reservation data |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\STCTicketsApiModelsCreateReservationResponse**](../Model/STCTicketsApiModelsCreateReservationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCreateReservationByPriceKeys**
> \Swagger\Client\Model\STCTicketsApiModelsCreateReservationResponse ticketApiCreateReservationByPriceKeys($body$language_code)

Create a reservation for one or more price keys

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsCreateReservationModel(); // \Swagger\Client\Model\STCTicketsApiModelsCreateReservationModel | The reservation data. All price keys should belong to the same section.
$language_code = "language_code_example"; // string | 

try {
    $result = $apiInstance->ticketApiCreateReservationByPriceKeys($body$language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCreateReservationByPriceKeys: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsCreateReservationModel**](../Model/STCTicketsApiModelsCreateReservationModel.md)| The reservation data. All price keys should belong to the same section. |
 **language_code** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\STCTicketsApiModelsCreateReservationResponse**](../Model/STCTicketsApiModelsCreateReservationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiCreateReservationBySection**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo ticketApiCreateReservationBySection($body$event_key, $performance_key, $performance_section_key, $language_code)

Create a reservation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsCreateReservationModel(); // \Swagger\Client\Model\STCTicketsApiModelsCreateReservationModel | The reservation data
$event_key = "event_key_example"; // string | The key of the event. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$performance_key = "performance_key_example"; // string | The key of the performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.
$performance_section_key = "performance_section_key_example"; // string | The key of the section within a performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiCreateReservationBySection($body$event_key, $performance_key, $performance_section_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiCreateReservationBySection: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsCreateReservationModel**](../Model/STCTicketsApiModelsCreateReservationModel.md)| The reservation data |
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **performance_key** | [**string**](../Model/.md)| The key of the performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |
 **performance_section_key** | [**string**](../Model/.md)| The key of the section within a performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetAllEvents**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo[] ticketApiGetAllEvents($language_code, $include_descriptions, $date_from)

Retrieve all events without their descriptions

All available and unavailable events without event description in the specified language

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.
$include_descriptions = True; // bool | When <code>true</code>, descriptions are included. Default <code>false</code>.
$date_from = "date_from_example"; // string | The from which to get events. Default <code>null</code>, all events from the past month and future.

try {
    $result = $apiInstance->ticketApiGetAllEvents($language_code, $include_descriptions, $date_from);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetAllEvents: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |
 **include_descriptions** | [**bool**](../Model/.md)| When &lt;code&gt;true&lt;/code&gt;, descriptions are included. Default &lt;code&gt;false&lt;/code&gt;. | [optional]
 **date_from** | [**string**](../Model/.md)| The from which to get events. Default &lt;code&gt;null&lt;/code&gt;, all events from the past month and future. | [optional]

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetAllEventsWithDescriptions**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo[] ticketApiGetAllEventsWithDescriptions($language_code, $date_from)

Retrieve all events with their descriptions

All available and unavailable events in the specified language

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.
$date_from = "date_from_example"; // string | The from which to get events. Default <code>null</code>, all events from the past month and future.

try {
    $result = $apiInstance->ticketApiGetAllEventsWithDescriptions($language_code, $date_from);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetAllEventsWithDescriptions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |
 **date_from** | [**string**](../Model/.md)| The from which to get events. Default &lt;code&gt;null&lt;/code&gt;, all events from the past month and future. | [optional]

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetAvailableEvents**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo[] ticketApiGetAvailableEvents($language_code, $include_descriptions)

Retrieve available events without their descriptions

Available events without event description in the specified language

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.
$include_descriptions = True; // bool | When <code>true</code>, descriptions are included.

try {
    $result = $apiInstance->ticketApiGetAvailableEvents($language_code, $include_descriptions);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetAvailableEvents: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |
 **include_descriptions** | [**bool**](../Model/.md)| When &lt;code&gt;true&lt;/code&gt;, descriptions are included. | [optional]

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetAvailableEventsWithDescriptions**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo[] ticketApiGetAvailableEventsWithDescriptions($language_code)

Retrieve available events with their descriptions

Available events in the specified language

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiGetAvailableEventsWithDescriptions($language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetAvailableEventsWithDescriptions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetAvailableSeats**
> int ticketApiGetAvailableSeats($event_key, $performance_key, $performance_section_key)

Retrieve number of available seats

Available seats within a specific section of a performance

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$event_key = "event_key_example"; // string | The key of the event. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$performance_key = "performance_key_example"; // string | The key of the performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.
$performance_section_key = "performance_section_key_example"; // string | The key of the section within a performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.

try {
    $result = $apiInstance->ticketApiGetAvailableSeats($event_key, $performance_key, $performance_section_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetAvailableSeats: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **performance_key** | [**string**](../Model/.md)| The key of the performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |
 **performance_section_key** | [**string**](../Model/.md)| The key of the section within a performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetAvailableSections**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPerformanceSectionInfo[] ticketApiGetAvailableSections($event_key, $performance_key, $language_code)

Retrieve sections

Available sections and their price types and prices for a specific performance in the specified language

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$event_key = "event_key_example"; // string | The key of the event. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$performance_key = "performance_key_example"; // string | The key of the performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiGetAvailableSections($event_key, $performance_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetAvailableSections: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **performance_key** | [**string**](../Model/.md)| The key of the performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPerformanceSectionInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSPerformanceSectionInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetBasket**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo ticketApiGetBasket($basket_key, $include_full_reservation_info)

Get the basket info and basic info of the reservations inside the basket

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$basket_key = "basket_key_example"; // string | The key of the basket. Example: <code>5242A995-D891-4DDB-AB97-5387BCE638CC</code>.
$include_full_reservation_info = True; // bool | Optional: get the full reservation info of the items in the basket.

try {
    $result = $apiInstance->ticketApiGetBasket($basket_key, $include_full_reservation_info);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetBasket: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **basket_key** | [**string**](../Model/.md)| The key of the basket. Example: &lt;code&gt;5242A995-D891-4DDB-AB97-5387BCE638CC&lt;/code&gt;. |
 **include_full_reservation_info** | [**bool**](../Model/.md)| Optional: get the full reservation info of the items in the basket. | [optional]

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetBasketForUser**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketSubscriptionReservationsInfo ticketApiGetBasketForUser()

Get the basket info coupled to the current logged in user, if there is any.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->ticketApiGetBasketForUser();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetBasketForUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketSubscriptionReservationsInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSBasketSubscriptionReservationsInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetCapacitySlotsByPriceKeys**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSCapacitySlotInfo[] ticketApiGetCapacitySlotsByPriceKeys($body$start_date, $end_date)

Get capacity slots for one or more price keys

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = array("body_example"); // string[] | 
$start_date = "start_date_example"; // string | Start date of capacity search
$end_date = "end_date_example"; // string | End date of capacity search (time span should be less than 3 months)

try {
    $result = $apiInstance->ticketApiGetCapacitySlotsByPriceKeys($body$start_date, $end_date);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetCapacitySlotsByPriceKeys: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**string[]**](../Model/string.md)|  |
 **start_date** | [**string**](../Model/.md)| Start date of capacity search |
 **end_date** | [**string**](../Model/.md)| End date of capacity search (time span should be less than 3 months) |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSCapacitySlotInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSCapacitySlotInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetContactByExternalContactID**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo ticketApiGetContactByExternalContactID($external_authentication_type, $external_contact_id)

Get a contact by a contact id from an external source (Facebook, Google, etc.)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$external_authentication_type = "external_authentication_type_example"; // string | The name of the external system. Example: <code>Faceboook</code>.
$external_contact_id = "external_contact_id_example"; // string | The ID of the contact in the external system.

try {
    $result = $apiInstance->ticketApiGetContactByExternalContactID($external_authentication_type, $external_contact_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetContactByExternalContactID: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **external_authentication_type** | [**string**](../Model/.md)| The name of the external system. Example: &lt;code&gt;Faceboook&lt;/code&gt;. |
 **external_contact_id** | [**string**](../Model/.md)| The ID of the contact in the external system. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetContactInfo**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo ticketApiGetContactInfo($contact_key, $language_code)

Get a contact

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$contact_key = "contact_key_example"; // string | The key of the contact. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$language_code = "language_code_example"; // string | Language code of the contact. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiGetContactInfo($contact_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetContactInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_key** | [**string**](../Model/.md)| The key of the contact. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the contact. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetContactInformation**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo[] ticketApiGetContactInformation()

Get info about the current logged in user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->ticketApiGetContactInformation();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetContactInformation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetContactLocationReviews**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo[] ticketApiGetContactLocationReviews($contact_key, $language_code)

Get location reviews of a contact

Get location reviews that a specific contact gave for locations

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$contact_key = "contact_key_example"; // string | The key of the contact. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiGetContactLocationReviews($contact_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetContactLocationReviews: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_key** | [**string**](../Model/.md)| The key of the contact. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetContactReservationReviews**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo[] ticketApiGetContactReservationReviews($contact_key, $language_code, $reservation_key, $event_key, $performance_key)

Get reviews of a specific contact

Get reviews that a specific contact gave optionally filtered by a specific reservation, event and/or performance.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$contact_key = "contact_key_example"; // string | The key of the contact. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.
$reservation_key = "reservation_key_example"; // string | The key of the reservation. Can be <code>null</code>. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$event_key = "event_key_example"; // string | The key of the event. Can be <code>null</code>. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$performance_key = "performance_key_example"; // string | The key of the performance. Can be <code>null</code>. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.

try {
    $result = $apiInstance->ticketApiGetContactReservationReviews($contact_key, $language_code, $reservation_key, $event_key, $performance_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetContactReservationReviews: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_key** | [**string**](../Model/.md)| The key of the contact. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |
 **reservation_key** | [**string**](../Model/.md)| The key of the reservation. Can be &lt;code&gt;null&lt;/code&gt;. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. | [optional]
 **event_key** | [**string**](../Model/.md)| The key of the event. Can be &lt;code&gt;null&lt;/code&gt;. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. | [optional]
 **performance_key** | [**string**](../Model/.md)| The key of the performance. Can be &lt;code&gt;null&lt;/code&gt;. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. | [optional]

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetDiscountInformation**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSDiscountInfo ticketApiGetDiscountInformation($discount_code)

Get usage information for discount code

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$discount_code = "discount_code_example"; // string | Discount code

try {
    $result = $apiInstance->ticketApiGetDiscountInformation($discount_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetDiscountInformation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **discount_code** | [**string**](../Model/.md)| Discount code |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSDiscountInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSDiscountInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetEvent**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo ticketApiGetEvent($event_key, $language_code)

Event in the specified language

Retrieve a single event in the specified language

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$event_key = "event_key_example"; // string | The key of the event. Example: <code>9fc18b99-c4a6-4e53-ab13-6c43b38a73cb</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiGetEvent($event_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetEvent: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;9fc18b99-c4a6-4e53-ab13-6c43b38a73cb&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetEventCategories**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSCategoryInfo[] ticketApiGetEventCategories($language_code)

Retrieve all event categories

Get the list of event categories in the specified language. Language codes are in the following format nl-NL, fr-FR. The event categories is a hierarchical structure that supports one level of sub-categories. Currently only the top-level list of event categories are returned. Sub categories are not returned. Category/sub category information is available per event. Based on this information the event category hierarchy of available events can be constructed.If an optional field is empty it will not be returned in the result.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$language_code = "language_code_example"; // string | Language codes are in the following format nl-NL, fr-FR.

try {
    $result = $apiInstance->ticketApiGetEventCategories($language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetEventCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language_code** | [**string**](../Model/.md)| Language codes are in the following format nl-NL, fr-FR. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSCategoryInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSCategoryInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetEventDescription**
> string ticketApiGetEventDescription($event_key, $language_code)

Event description in the specified language

Retrieve the description of a single event in the specified language

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$event_key = "event_key_example"; // string | The key of the event. Example: <code>9fc18b99-c4a6-4e53-ab13-6c43b38a73cb</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiGetEventDescription($event_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetEventDescription: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;9fc18b99-c4a6-4e53-ab13-6c43b38a73cb&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetExternalContactID**
> string ticketApiGetExternalContactID($external_authentication_type, $contact_key)

Get the external contact id from an external source (Facebook, Google, etc.) by the key of the contact

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$external_authentication_type = "external_authentication_type_example"; // string | The name of the external system. Example: <code>Faceboook</code>.
$contact_key = "contact_key_example"; // string | The key of the contact. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.

try {
    $result = $apiInstance->ticketApiGetExternalContactID($external_authentication_type, $contact_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetExternalContactID: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **external_authentication_type** | [**string**](../Model/.md)| The name of the external system. Example: &lt;code&gt;Faceboook&lt;/code&gt;. |
 **contact_key** | [**string**](../Model/.md)| The key of the contact. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetLocations**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSLocationInfo[] ticketApiGetLocations($language_code)

Get all the locations for this sales channel

Every performance takes place on a specific location. Use these methods to retrieve specific location information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiGetLocations($language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetLocations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSLocationInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSLocationInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetMaximumSeatsTogether**
> int ticketApiGetMaximumSeatsTogether($event_key, $performance_key, $performance_section_key)

Retrieve number of available seats together

Available seats together within a specific section of a performance.              This method gets all the maximum amount of seats that can be booked next to each other within a specific section of a performance. Especially for seated sections this method can be handy to verify if all the seats for a new reservation can be booked next to each other.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$event_key = "event_key_example"; // string | The key of the event. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$performance_key = "performance_key_example"; // string | The key of the performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.
$performance_section_key = "performance_section_key_example"; // string | The key of the section within a performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.

try {
    $result = $apiInstance->ticketApiGetMaximumSeatsTogether($event_key, $performance_key, $performance_section_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetMaximumSeatsTogether: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **performance_key** | [**string**](../Model/.md)| The key of the performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |
 **performance_section_key** | [**string**](../Model/.md)| The key of the section within a performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetOfflineTicketCodes**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo[] ticketApiGetOfflineTicketCodes($price_key, $count)

Get a list of tickets that can be sold offline.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$price_key = "price_key_example"; // string | PriceKey of the ticket type you want to get tickets for
$count = 56; // int | Number of tickets to get. Count cannot exceed 10.000

try {
    $result = $apiInstance->ticketApiGetOfflineTicketCodes($price_key, $count);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetOfflineTicketCodes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **price_key** | [**string**](../Model/.md)| PriceKey of the ticket type you want to get tickets for |
 **count** | [**int**](../Model/.md)| Number of tickets to get. Count cannot exceed 10.000 |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetPartnerInfo**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPartnerInfo ticketApiGetPartnerInfo($partner_key)

Get info of a partner

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$partner_key = "partner_key_example"; // string | The key of the partner. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.

try {
    $result = $apiInstance->ticketApiGetPartnerInfo($partner_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetPartnerInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **partner_key** | [**string**](../Model/.md)| The key of the partner. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPartnerInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSPartnerInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetPaymentMethods**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPaymentMethods ticketApiGetPaymentMethods($event_key)

Get available payment methods for event

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$event_key = "event_key_example"; // string | The key of the event. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.

try {
    $result = $apiInstance->ticketApiGetPaymentMethods($event_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetPaymentMethods: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPaymentMethods**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSPaymentMethods.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetPriceByBranchePriceId**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPerformanceSectionInfo ticketApiGetPriceByBranchePriceId($branche_price_id, $language_code, $moment)

Retrieve event, performance, section and price info by the supplied branche price id

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$branche_price_id = "branche_price_id_example"; // string | The branche id of the price that we're looking for within this sales channel. Example: <code>EAN998371234</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.
$moment = "moment_example"; // string | For wich date and time you want to get the price.

try {
    $result = $apiInstance->ticketApiGetPriceByBranchePriceId($branche_price_id, $language_code, $moment);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetPriceByBranchePriceId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **branche_price_id** | [**string**](../Model/.md)| The branche id of the price that we&#x27;re looking for within this sales channel. Example: &lt;code&gt;EAN998371234&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |
 **moment** | [**string**](../Model/.md)| For wich date and time you want to get the price. | [optional]

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPerformanceSectionInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSPerformanceSectionInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetProducts**
> \Swagger\Client\Model\STCTicketsApiModelsProductListViewModel ticketApiGetProducts($language_code)

Get tickets and subscription products of the event of the saleschannel of the logged in user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiGetProducts($language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\STCTicketsApiModelsProductListViewModel**](../Model/STCTicketsApiModelsProductListViewModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetProfilePicture**
> string ticketApiGetProfilePicture($partner_key, $ticket_code)

Get profile picture of a subscription holder

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$partner_key = "partner_key_example"; // string | The key of the partner. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$ticket_code = "ticket_code_example"; // string | The ticket code of the subscription product.

try {
    $result = $apiInstance->ticketApiGetProfilePicture($partner_key, $ticket_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetProfilePicture: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **partner_key** | [**string**](../Model/.md)| The key of the partner. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **ticket_code** | [**string**](../Model/.md)| The ticket code of the subscription product. |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetQoute**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSQuoteInfo ticketApiGetQoute($body$event_key, $performance_key, $performance_section_key, $language_code)

Get reservation quote

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsCreateReservationModel(); // \Swagger\Client\Model\STCTicketsApiModelsCreateReservationModel | The reservation data.
$event_key = "event_key_example"; // string | The key of the event. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$performance_key = "performance_key_example"; // string | The key of the performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.
$performance_section_key = "performance_section_key_example"; // string | The key of the section within a performance. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiGetQoute($body$event_key, $performance_key, $performance_section_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetQoute: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsCreateReservationModel**](../Model/STCTicketsApiModelsCreateReservationModel.md)| The reservation data. |
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **performance_key** | [**string**](../Model/.md)| The key of the performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |
 **performance_section_key** | [**string**](../Model/.md)| The key of the section within a performance. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSQuoteInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSQuoteInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetReservation**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo ticketApiGetReservation($reservation_key)

Get reservation and tickets

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$reservation_key = "reservation_key_example"; // string | The key of the reservation. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.

try {
    $result = $apiInstance->ticketApiGetReservation($reservation_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetReservation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reservation_key** | [**string**](../Model/.md)| The key of the reservation. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetReservationHistory**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationHistoryInfo[] ticketApiGetReservationHistory($reservation_key)

Get the history info of a reservation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$reservation_key = "reservation_key_example"; // string | The key of the reservation. Example: <code>2FCA256D-C136-4904-B903-B564AC21121E</code>.

try {
    $result = $apiInstance->ticketApiGetReservationHistory($reservation_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetReservationHistory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reservation_key** | [**string**](../Model/.md)| The key of the reservation. Example: &lt;code&gt;2FCA256D-C136-4904-B903-B564AC21121E&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationHistoryInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSReservationHistoryInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetReservationTickets**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo[] ticketApiGetReservationTickets($reservation_key)

Get the tickets of a reservation after the reservation is confirmed

When the reservation is not confirmed, this method return an error message.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$reservation_key = "reservation_key_example"; // string | The key of the reservation. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.

try {
    $result = $apiInstance->ticketApiGetReservationTickets($reservation_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetReservationTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reservation_key** | [**string**](../Model/.md)| The key of the reservation. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetReservationTicketsBeforeConfirm**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo[] ticketApiGetReservationTicketsBeforeConfirm($reservation_key)

Get the tickets of a reservation before the reservation is confirmed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$reservation_key = "reservation_key_example"; // string | The key of the reservation. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.

try {
    $result = $apiInstance->ticketApiGetReservationTicketsBeforeConfirm($reservation_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetReservationTicketsBeforeConfirm: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reservation_key** | [**string**](../Model/.md)| The key of the reservation. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSTicketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetReservations**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo[] ticketApiGetReservations($contact_key, $language_code, $confirmed_only, $include_full_reservation_info)

Get reservations for a contact

Get reservations for a contact without the tickets

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$contact_key = "contact_key_example"; // string | The key of the contact. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.
$confirmed_only = True; // bool | When <code>true</code>, only returns confirmed reservations (NOT confirmed-print-tickets reservations). Default: <code>true</code>
$include_full_reservation_info = True; // bool | When <code>true</code>, all information and ticket info is returned as well. Default: <code>false</code>

try {
    $result = $apiInstance->ticketApiGetReservations($contact_key, $language_code, $confirmed_only, $include_full_reservation_info);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetReservations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_key** | [**string**](../Model/.md)| The key of the contact. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |
 **confirmed_only** | [**bool**](../Model/.md)| When &lt;code&gt;true&lt;/code&gt;, only returns confirmed reservations (NOT confirmed-print-tickets reservations). Default: &lt;code&gt;true&lt;/code&gt; | [optional]
 **include_full_reservation_info** | [**bool**](../Model/.md)| When &lt;code&gt;true&lt;/code&gt;, all information and ticket info is returned as well. Default: &lt;code&gt;false&lt;/code&gt; | [optional]

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetScannerConfiguration**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSConfigurationInfo ticketApiGetScannerConfiguration($partner_key, $event_key, $performance_key, $performance_section_key, $language_code)

Get info of a partner

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$partner_key = "partner_key_example"; // string | The key of the partner. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$event_key = "event_key_example"; // string | The key of the event. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$performance_key = "performance_key_example"; // string | The key of the performance. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$performance_section_key = "performance_section_key_example"; // string | The key of the performance section. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$language_code = "language_code_example"; // string | The language of the requested result.

try {
    $result = $apiInstance->ticketApiGetScannerConfiguration($partner_key, $event_key, $performance_key, $performance_section_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetScannerConfiguration: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **partner_key** | [**string**](../Model/.md)| The key of the partner. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **event_key** | [**string**](../Model/.md)| The key of the event. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **performance_key** | [**string**](../Model/.md)| The key of the performance. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **performance_section_key** | [**string**](../Model/.md)| The key of the performance section. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| The language of the requested result. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSConfigurationInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSConfigurationInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetSellerForPartners**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPartnerNameInfo ticketApiGetSellerForPartners($seller_contact_key)

Gets seller information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$seller_contact_key = "seller_contact_key_example"; // string | The key of the seller contact. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.

try {
    $result = $apiInstance->ticketApiGetSellerForPartners($seller_contact_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetSellerForPartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seller_contact_key** | [**string**](../Model/.md)| The key of the seller contact. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPartnerNameInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSPartnerNameInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetShopConfiguration**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSShopConfiguration ticketApiGetShopConfiguration($slug)

Get shop configuration settings

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$slug = "slug_example"; // string | 

try {
    $result = $apiInstance->ticketApiGetShopConfiguration($slug);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetShopConfiguration: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **slug** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSShopConfiguration**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSShopConfiguration.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetShopSettings**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSShopSettings ticketApiGetShopSettings($event_key)

Get shop settings

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$event_key = "event_key_example"; // string | 

try {
    $result = $apiInstance->ticketApiGetShopSettings($event_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetShopSettings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_key** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSShopSettings**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSShopSettings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetSubscriptionInformation**
> \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionTSSubscription ticketApiGetSubscriptionInformation($reservation_key, $subscription_key, $ticket_code)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$reservation_key = "reservation_key_example"; // string | 
$subscription_key = "subscription_key_example"; // string | 
$ticket_code = "ticket_code_example"; // string | 

try {
    $result = $apiInstance->ticketApiGetSubscriptionInformation($reservation_key, $subscription_key, $ticket_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetSubscriptionInformation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reservation_key** | [**string**](../Model/.md)|  |
 **subscription_key** | [**string**](../Model/.md)|  |
 **ticket_code** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionTSSubscription**](../Model/LastSeatsGeneralObjectsSubscriptionTSSubscription.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetTop5Events**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo[] ticketApiGetTop5Events($language_code, $by_orders, $still_on_sale)

Get the top 5 events in the specified language

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.
$by_orders = True; // bool | When <code>true</code>, you get the top 5 events with the most orders for this sales channel from the system. When <code>false</code>, you get the top 5 events with the highest rating in the system.
$still_on_sale = True; // bool | When <code>true</code> to only see events that can still be bought; otherwise, <code>false</code>.

try {
    $result = $apiInstance->ticketApiGetTop5Events($language_code, $by_orders, $still_on_sale);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetTop5Events: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |
 **by_orders** | [**bool**](../Model/.md)| When &lt;code&gt;true&lt;/code&gt;, you get the top 5 events with the most orders for this sales channel from the system. When &lt;code&gt;false&lt;/code&gt;, you get the top 5 events with the highest rating in the system. | [optional]
 **still_on_sale** | [**bool**](../Model/.md)| When &lt;code&gt;true&lt;/code&gt; to only see events that can still be bought; otherwise, &lt;code&gt;false&lt;/code&gt;. | [optional]

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetTop5Locations**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSLocationInfo[] ticketApiGetTop5Locations($language_code, $by_orders)

Get the top 5 locations

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.
$by_orders = True; // bool | When <code>true</code>, you get the top 5 locations with the most orders for this sales channel from the system. When <code>false</code>, you get the top 5 locations with the highest rating in the system.

try {
    $result = $apiInstance->ticketApiGetTop5Locations($language_code, $by_orders);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetTop5Locations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |
 **by_orders** | [**bool**](../Model/.md)| When &lt;code&gt;true&lt;/code&gt;, you get the top 5 locations with the most orders for this sales channel from the system. When &lt;code&gt;false&lt;/code&gt;, you get the top 5 locations with the highest rating in the system. | [optional]

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSLocationInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSLocationInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetUnAvailableEvents**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo[] ticketApiGetUnAvailableEvents($language_code, $include_descriptions, $datefrom)

Retrieve unavailable events without their descriptions

Unavailable events without event description in the specified language

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.
$include_descriptions = True; // bool | When <code>true</code>, descriptions are included. Default <code>false</code>.
$datefrom = "datefrom_example"; // string | The from which to get events. Default <code>null</code>, all events.

try {
    $result = $apiInstance->ticketApiGetUnAvailableEvents($language_code, $include_descriptions, $datefrom);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetUnAvailableEvents: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |
 **include_descriptions** | [**bool**](../Model/.md)| When &lt;code&gt;true&lt;/code&gt;, descriptions are included. Default &lt;code&gt;false&lt;/code&gt;. | [optional]
 **datefrom** | [**string**](../Model/.md)| The from which to get events. Default &lt;code&gt;null&lt;/code&gt;, all events. | [optional]

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiGetUnAvailableEventsWithDescriptions**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo[] ticketApiGetUnAvailableEventsWithDescriptions($language_code)

Retrieve unavailable events with their descriptions

Unavailable events in the specified language

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiGetUnAvailableEventsWithDescriptions($language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiGetUnAvailableEventsWithDescriptions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiLoginContact**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo ticketApiLoginContact($mail_address, $password)

Login to the system using a mail address and a password

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$mail_address = "mail_address_example"; // string | The e-mail address of the contact
$password = "password_example"; // string | The password of the contact

try {
    $result = $apiInstance->ticketApiLoginContact($mail_address, $password);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiLoginContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mail_address** | [**string**](../Model/.md)| The e-mail address of the contact |
 **password** | [**string**](../Model/.md)| The password of the contact |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiNotifyFromSkiData**
> object ticketApiNotifyFromSkiData($partner_key, $ticket_code, $success, $error_message, $transaction_type)

Callback after a ticket code (or profile picture) has been sent to SkiData

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$partner_key = "partner_key_example"; // string | 
$ticket_code = "ticket_code_example"; // string | 
$success = True; // bool | 
$error_message = "error_message_example"; // string | 
$transaction_type = "transaction_type_example"; // string | 

try {
    $result = $apiInstance->ticketApiNotifyFromSkiData($partner_key, $ticket_code, $success, $error_message, $transaction_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiNotifyFromSkiData: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **partner_key** | [**string**](../Model/.md)|  |
 **ticket_code** | [**string**](../Model/.md)|  |
 **success** | [**bool**](../Model/.md)|  |
 **error_message** | [**string**](../Model/.md)|  | [optional]
 **transaction_type** | [**string**](../Model/.md)|  | [optional]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiPing**
> object ticketApiPing()

Use Ping only for connectionchecking

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->ticketApiPing();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiPing: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiPreConfirmReservation**
> \Swagger\Client\Model\STCTicketsApiModelsPreConfirmReservationResponse ticketApiPreConfirmReservation($body$reservation_key)

Preconfirm a reservation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsPreConfirmReservationModel(); // \Swagger\Client\Model\STCTicketsApiModelsPreConfirmReservationModel | Preconfirmation data
$reservation_key = "reservation_key_example"; // string | The key of the reservation. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.

try {
    $result = $apiInstance->ticketApiPreConfirmReservation($body$reservation_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiPreConfirmReservation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsPreConfirmReservationModel**](../Model/STCTicketsApiModelsPreConfirmReservationModel.md)| Preconfirmation data |
 **reservation_key** | [**string**](../Model/.md)| The key of the reservation. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\STCTicketsApiModelsPreConfirmReservationResponse**](../Model/STCTicketsApiModelsPreConfirmReservationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiRemovePriceFromReservation**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo ticketApiRemovePriceFromReservation($body$reservation_key)

Remove a price from an existing reservation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeRequestInfo(); // \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeRequestInfo | The data of the price type that must be removed from this reservation.
$reservation_key = "reservation_key_example"; // string | The key of the reservation. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.

try {
    $result = $apiInstance->ticketApiRemovePriceFromReservation($body$reservation_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiRemovePriceFromReservation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeRequestInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeRequestInfo.md)| The data of the price type that must be removed from this reservation. |
 **reservation_key** | [**string**](../Model/.md)| The key of the reservation. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiRemoveReservationsFromBasket**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo ticketApiRemoveReservationsFromBasket($body$basket_key, $is_subscription, $include_full_reservation_info)

Remove one or more reservations from a basket

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = array("body_example"); // string[] | The list of keys of the reservations that must be added to the list.
$basket_key = "basket_key_example"; // string | The key of the basket. Example: <code>5242A995-D891-4DDB-AB97-5387BCE638CC</code>.
$is_subscription = True; // bool | Optional: mark reservations as renewalsubscription
$include_full_reservation_info = True; // bool | Optional: get the full reservation info of the items in the basket.

try {
    $result = $apiInstance->ticketApiRemoveReservationsFromBasket($body$basket_key, $is_subscription, $include_full_reservation_info);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiRemoveReservationsFromBasket: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**string[]**](../Model/string.md)| The list of keys of the reservations that must be added to the list. |
 **basket_key** | [**string**](../Model/.md)| The key of the basket. Example: &lt;code&gt;5242A995-D891-4DDB-AB97-5387BCE638CC&lt;/code&gt;. |
 **is_subscription** | [**bool**](../Model/.md)| Optional: mark reservations as renewalsubscription | [optional]
 **include_full_reservation_info** | [**bool**](../Model/.md)| Optional: get the full reservation info of the items in the basket. | [optional]

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiRenewSubscription**
> \Swagger\Client\Model\STCTicketsApiModelsCreateReservationResponse ticketApiRenewSubscription($body$language_code)

Renew a subscription. Result is a <code>CreateReservationResponse</code>.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsApiModelsRenewSubscriptionModel(); // \Swagger\Client\Model\STCTicketsApiModelsRenewSubscriptionModel | the ticketcode information of the subscription to check
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiRenewSubscription($body$language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiRenewSubscription: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsApiModelsRenewSubscriptionModel**](../Model/STCTicketsApiModelsRenewSubscriptionModel.md)| the ticketcode information of the subscription to check |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\STCTicketsApiModelsCreateReservationResponse**](../Model/STCTicketsApiModelsCreateReservationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiRequestPaymentUrl**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPaymentUrl ticketApiRequestPaymentUrl($body$reservation_key, $language_code)

Start payment for reservation and request payment url

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSStartPaymentRequest(); // \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSStartPaymentRequest | Additional information necessary to construct payment url
$reservation_key = "reservation_key_example"; // string | The key of the reservation. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$language_code = "language_code_example"; // string | Desired language for payment process

try {
    $result = $apiInstance->ticketApiRequestPaymentUrl($body$reservation_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiRequestPaymentUrl: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSStartPaymentRequest**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSStartPaymentRequest.md)| Additional information necessary to construct payment url |
 **reservation_key** | [**string**](../Model/.md)| The key of the reservation. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Desired language for payment process |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPaymentUrl**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSPaymentUrl.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiSaveContactInfo**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo ticketApiSaveContactInfo($body$language_code)

Create contact

Each reservation is connected to a contact. Contacts are not reused over reservations.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo(); // \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo | The contact information
$language_code = "language_code_example"; // string | Language code of the contact. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiSaveContactInfo($body$language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiSaveContactInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo.md)| The contact information |
 **language_code** | [**string**](../Model/.md)| Language code of the contact. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiSaveExternalContactID**
> string ticketApiSaveExternalContactID($external_authentication_type, $contact_key, $external_contact_id)

Connects the external contact id from an external source (Facebook, Google, etc.) to the key of the contact

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$external_authentication_type = "external_authentication_type_example"; // string | The name of the external system. Example: <code>Faceboook</code>.
$contact_key = "contact_key_example"; // string | The key of the contact. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$external_contact_id = "external_contact_id_example"; // string | The ID of the contact in the external system.

try {
    $result = $apiInstance->ticketApiSaveExternalContactID($external_authentication_type, $contact_key, $external_contact_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiSaveExternalContactID: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **external_authentication_type** | [**string**](../Model/.md)| The name of the external system. Example: &lt;code&gt;Faceboook&lt;/code&gt;. |
 **contact_key** | [**string**](../Model/.md)| The key of the contact. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **external_contact_id** | [**string**](../Model/.md)| The ID of the contact in the external system. |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiSetBasketContact**
> object ticketApiSetBasketContact($body$basket_key)

Connect a contact to a basket and to all the reservations within the basket that have no contact connected (yet)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo(); // \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo | The contact data to add
$basket_key = "basket_key_example"; // string | The key of the basket. Example: <code>5242A995-D891-4DDB-AB97-5387BCE638CC</code>.

try {
    $result = $apiInstance->ticketApiSetBasketContact($body$basket_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiSetBasketContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo.md)| The contact data to add |
 **basket_key** | [**string**](../Model/.md)| The key of the basket. Example: &lt;code&gt;5242A995-D891-4DDB-AB97-5387BCE638CC&lt;/code&gt;. |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiSetBasketContactByContactKey**
> object ticketApiSetBasketContactByContactKey($basket_key, $contact_key)

Connect an existing contact to a basket and to all the reservations within the basket that have no contact connected (yet)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$basket_key = "basket_key_example"; // string | The key of the basket. Example: <code>5242A995-D891-4DDB-AB97-5387BCE638CC</code>.
$contact_key = "contact_key_example"; // string | The key of the contact to add. Example: <code>5242A995-D891-4DDB-AB97-5387BCE638CC</code>.

try {
    $result = $apiInstance->ticketApiSetBasketContactByContactKey($basket_key, $contact_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiSetBasketContactByContactKey: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **basket_key** | [**string**](../Model/.md)| The key of the basket. Example: &lt;code&gt;5242A995-D891-4DDB-AB97-5387BCE638CC&lt;/code&gt;. |
 **contact_key** | [**string**](../Model/.md)| The key of the contact to add. Example: &lt;code&gt;5242A995-D891-4DDB-AB97-5387BCE638CC&lt;/code&gt;. |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiSetReservationContact**
> object ticketApiSetReservationContact($body$reservation_key)

Connect a contact to a reservation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo(); // \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo | The contact data to add
$reservation_key = "reservation_key_example"; // string | The key of the reservation. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.

try {
    $result = $apiInstance->ticketApiSetReservationContact($body$reservation_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiSetReservationContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo.md)| The contact data to add |
 **reservation_key** | [**string**](../Model/.md)| The key of the reservation. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiSetReservationContactByKey**
> object ticketApiSetReservationContactByKey($reservation_key, $contact_key)

Connect an existing contact to a reservation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$reservation_key = "reservation_key_example"; // string | The key of the reservation. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$contact_key = "contact_key_example"; // string | The key of the contact to add. Example: <code>5242A995-D891-4DDB-AB97-5387BCE638CC</code>.

try {
    $result = $apiInstance->ticketApiSetReservationContactByKey($reservation_key, $contact_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiSetReservationContactByKey: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reservation_key** | [**string**](../Model/.md)| The key of the reservation. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **contact_key** | [**string**](../Model/.md)| The key of the contact to add. Example: &lt;code&gt;5242A995-D891-4DDB-AB97-5387BCE638CC&lt;/code&gt;. |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiSkiDataStatusUpdate**
> object ticketApiSkiDataStatusUpdate($body)

Update the status of the SkiData connection

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsAccessControlSkiDataStatusMessageDTO(); // \Swagger\Client\Model\LastSeatsGeneralObjectsAccessControlSkiDataStatusMessageDTO | 

try {
    $result = $apiInstance->ticketApiSkiDataStatusUpdate($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiSkiDataStatusUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsAccessControlSkiDataStatusMessageDTO**](../Model/LastSeatsGeneralObjectsAccessControlSkiDataStatusMessageDTO.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiStartBasketPayment**
> object ticketApiStartBasketPayment($basket_key, $payment_category, $payment_method_id, $is_subscription, $app_namespace, $language_code)

Start payment for a basket

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$basket_key = "basket_key_example"; // string | The key of the basket. Example: <code>5242A995-D891-4DDB-AB97-5387BCE638CC</code>.
$payment_category = 56; // int | 
$payment_method_id = "payment_method_id_example"; // string | 
$is_subscription = True; // bool | Optional: set to subscription reservation in basket
$app_namespace = "app_namespace_example"; // string | Optional: set the namespace
$language_code = "language_code_example"; // string | Optional: language code, default nl-NL

try {
    $result = $apiInstance->ticketApiStartBasketPayment($basket_key, $payment_category, $payment_method_id, $is_subscription, $app_namespace, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiStartBasketPayment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **basket_key** | [**string**](../Model/.md)| The key of the basket. Example: &lt;code&gt;5242A995-D891-4DDB-AB97-5387BCE638CC&lt;/code&gt;. |
 **payment_category** | [**int**](../Model/.md)|  | [optional]
 **payment_method_id** | [**string**](../Model/.md)|  | [optional]
 **is_subscription** | [**bool**](../Model/.md)| Optional: set to subscription reservation in basket | [optional]
 **app_namespace** | [**string**](../Model/.md)| Optional: set the namespace | [optional]
 **language_code** | [**string**](../Model/.md)| Optional: language code, default nl-NL | [optional]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiStartReservationPayment**
> object ticketApiStartReservationPayment($reservation_key)

Start payment for a reservation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$reservation_key = "reservation_key_example"; // string | The key of the reservation. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.

try {
    $result = $apiInstance->ticketApiStartReservationPayment($reservation_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiStartReservationPayment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reservation_key** | [**string**](../Model/.md)| The key of the reservation. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiUpdateBasket**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo ticketApiUpdateBasket($body$basket_key)

Update a basket

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketUpdateInfo(); // \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketUpdateInfo | The data of the basket that must be updated. If you don't want items to get updated, leave them empty (null).
$basket_key = "basket_key_example"; // string | The key of the basket. Example: <code>5242A995-D891-4DDB-AB97-5387BCE638CC</code>.

try {
    $result = $apiInstance->ticketApiUpdateBasket($body$basket_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiUpdateBasket: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketUpdateInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSBasketUpdateInfo.md)| The data of the basket that must be updated. If you don&#x27;t want items to get updated, leave them empty (null). |
 **basket_key** | [**string**](../Model/.md)| The key of the basket. Example: &lt;code&gt;5242A995-D891-4DDB-AB97-5387BCE638CC&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSBasketInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiUpdateContactInfo**
> object ticketApiUpdateContactInfo($body$contact_key, $language_code)

Update a contact

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo(); // \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo | The contact values
$contact_key = "contact_key_example"; // string | The key of the contact. Example: <code>D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53</code>.
$language_code = "language_code_example"; // string | Language code of the contact. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->ticketApiUpdateContactInfo($body$contact_key, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiUpdateContactInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo.md)| The contact values |
 **contact_key** | [**string**](../Model/.md)| The key of the contact. Example: &lt;code&gt;D3AF2ECA-EEF1-4BB4-96BF-48D9C9353D53&lt;/code&gt;. |
 **language_code** | [**string**](../Model/.md)| Language code of the contact. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiUpdateReservation**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo ticketApiUpdateReservation($body$reservation_key)

Change a reservation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationUpdateInfo(); // \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationUpdateInfo | The data of the reservation that must be updated. If you don't want items to get updated, leave them empty (null).
$reservation_key = "reservation_key_example"; // string | The key of the reservation. Example: <code>2FCA256D-C136-4904-B903-B564AC21121E</code>.

try {
    $result = $apiInstance->ticketApiUpdateReservation($body$reservation_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiUpdateReservation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationUpdateInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSReservationUpdateInfo.md)| The data of the reservation that must be updated. If you don&#x27;t want items to get updated, leave them empty (null). |
 **reservation_key** | [**string**](../Model/.md)| The key of the reservation. Example: &lt;code&gt;2FCA256D-C136-4904-B903-B564AC21121E&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketApiUpdateReview**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo ticketApiUpdateReview($body$review_key)

Change the rating and/or review text for a specific review

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TicketApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo(); // \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo | The changed values of the review.
$review_key = "review_key_example"; // string | The key of the review. Example: <code>0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360</code>.

try {
    $result = $apiInstance->ticketApiUpdateReview($body$review_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TicketApiApi->ticketApiUpdateReview: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSReviewInfo.md)| The changed values of the review. |
 **review_key** | [**string**](../Model/.md)| The key of the review. Example: &lt;code&gt;0EFCCF0D-2B11-45FA-ABA9-FB1F5319D360&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/jsontext/jsonapplication/xmltext/xml
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

