# Swagger\Client\ResellerApiApi

All URIs are relative to *https://ticketstest.ticketcounter.nl/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**resellerApiBundleAndDownloadTickets**](ResellerApiApi.md#resellerApiBundleAndDownloadTickets) | **POST** /api/v1/reseller/{appNamespace}/bundleanddownload/{languageCode} | 
[**resellerApiCancelTickets**](ResellerApiApi.md#resellerApiCancelTickets) | **POST** /api/v1/reseller/{appNamespace}/canceltickets | 
[**resellerApiCreateReservation**](ResellerApiApi.md#resellerApiCreateReservation) | **POST** /api/v1/reseller/{appNamespace}/createreservation/{languageCode} | Creates a reservation for a reseller
[**resellerApiDownloadInvoice**](ResellerApiApi.md#resellerApiDownloadInvoice) | **GET** /api/v1/reseller/{appNamespace}/downloadinvoice | 
[**resellerApiGetResellerCapacitySlotsByPriceKeys**](ResellerApiApi.md#resellerApiGetResellerCapacitySlotsByPriceKeys) | **POST** /api/v1/reseller/{appNamespace}/capacityslots | Get capacity slots for one or more price keys
[**resellerApiGetResellerOrders**](ResellerApiApi.md#resellerApiGetResellerOrders) | **GET** /api/v1/reseller/{appNamespace}/getresellerorders/{languageCode} | Get reseller orders
[**resellerApiGetResellerPaymentMethods**](ResellerApiApi.md#resellerApiGetResellerPaymentMethods) | **POST** /api/v1/reseller/{appNamespace}/paymentmethods/{languageCode} | Returns the payment methods
[**resellerApiGetResellerProducts**](ResellerApiApi.md#resellerApiGetResellerProducts) | **GET** /api/v1/reseller/{appNamespace}/products/{languageCode} | Returns all the events a reseller can sell
[**resellerApiGetResellerShopPartnerInfo**](ResellerApiApi.md#resellerApiGetResellerShopPartnerInfo) | **GET** /api/v1/reseller/{appNamespace}/resellershop/partnerinfo/{languageCode} | 
[**resellerApiGetResellerTotalPrice**](ResellerApiApi.md#resellerApiGetResellerTotalPrice) | **POST** /api/v1/reseller/{appNamespace}/totalprice | Returns the total price of the selected tickets
[**resellerApiGetTickets**](ResellerApiApi.md#resellerApiGetTickets) | **GET** /api/v1/reseller/{appNamespace}/gettickets/{languageCode} | 

# **resellerApiBundleAndDownloadTickets**
> \Swagger\Client\Model\STCTicketsModelsBundleAndDownloadTicketsResponse resellerApiBundleAndDownloadTickets($body$app_namespace, $language_code)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ResellerApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsModelsResellerTicketRequest(); // \Swagger\Client\Model\STCTicketsModelsResellerTicketRequest | 
$app_namespace = "app_namespace_example"; // string | 
$language_code = "language_code_example"; // string | 

try {
    $result = $apiInstance->resellerApiBundleAndDownloadTickets($body$app_namespace, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ResellerApiApi->resellerApiBundleAndDownloadTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsModelsResellerTicketRequest**](../Model/STCTicketsModelsResellerTicketRequest.md)|  |
 **app_namespace** | [**string**](../Model/.md)|  |
 **language_code** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\STCTicketsModelsBundleAndDownloadTicketsResponse**](../Model/STCTicketsModelsBundleAndDownloadTicketsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xmltext/xmlapplication/jsontext/json
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resellerApiCancelTickets**
> \Swagger\Client\Model\LastSeatsLibraryMessagesServiceResponse resellerApiCancelTickets($body$app_namespace)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ResellerApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\STCTicketsModelsResellerCancelTicketRequest(); // \Swagger\Client\Model\STCTicketsModelsResellerCancelTicketRequest | 
$app_namespace = "app_namespace_example"; // string | 

try {
    $result = $apiInstance->resellerApiCancelTickets($body$app_namespace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ResellerApiApi->resellerApiCancelTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\STCTicketsModelsResellerCancelTicketRequest**](../Model/STCTicketsModelsResellerCancelTicketRequest.md)|  |
 **app_namespace** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\LastSeatsLibraryMessagesServiceResponse**](../Model/LastSeatsLibraryMessagesServiceResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xmltext/xmlapplication/jsontext/json
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resellerApiCreateReservation**
> \Swagger\Client\Model\LastSeatsGeneralObjectsResellerMessagesResellerCreateReservationResponse resellerApiCreateReservation($body$app_namespace, $language_code, $currency_code)

Creates a reservation for a reseller

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ResellerApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsResellerMessagesResellerCreateReservationRequest(); // \Swagger\Client\Model\LastSeatsGeneralObjectsResellerMessagesResellerCreateReservationRequest | 
$app_namespace = "app_namespace_example"; // string | 
$language_code = "language_code_example"; // string | 
$currency_code = "currency_code_example"; // string | 

try {
    $result = $apiInstance->resellerApiCreateReservation($body$app_namespace, $language_code, $currency_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ResellerApiApi->resellerApiCreateReservation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsResellerMessagesResellerCreateReservationRequest**](../Model/LastSeatsGeneralObjectsResellerMessagesResellerCreateReservationRequest.md)|  |
 **app_namespace** | [**string**](../Model/.md)|  |
 **language_code** | [**string**](../Model/.md)|  |
 **currency_code** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsResellerMessagesResellerCreateReservationResponse**](../Model/LastSeatsGeneralObjectsResellerMessagesResellerCreateReservationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xmltext/xmlapplication/jsontext/json
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resellerApiDownloadInvoice**
> object resellerApiDownloadInvoice($app_namespace, $order_id, $order_number)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ResellerApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$app_namespace = "app_namespace_example"; // string | 
$order_id = 56; // int | 
$order_number = "order_number_example"; // string | 

try {
    $result = $apiInstance->resellerApiDownloadInvoice($app_namespace, $order_id, $order_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ResellerApiApi->resellerApiDownloadInvoice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_namespace** | [**string**](../Model/.md)|  |
 **order_id** | [**int**](../Model/.md)|  |
 **order_number** | [**string**](../Model/.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resellerApiGetResellerCapacitySlotsByPriceKeys**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSCapacitySlotInfo[] resellerApiGetResellerCapacitySlotsByPriceKeys($body$app_namespace, $start_date, $end_date)

Get capacity slots for one or more price keys

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ResellerApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = array("body_example"); // string[] | 
$app_namespace = "app_namespace_example"; // string | 
$start_date = "start_date_example"; // string | Start date of capacity search
$end_date = "end_date_example"; // string | End date of capacity search (time span should be less than 3 months)

try {
    $result = $apiInstance->resellerApiGetResellerCapacitySlotsByPriceKeys($body$app_namespace, $start_date, $end_date);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ResellerApiApi->resellerApiGetResellerCapacitySlotsByPriceKeys: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**string[]**](../Model/string.md)|  |
 **app_namespace** | [**string**](../Model/.md)|  |
 **start_date** | [**string**](../Model/.md)| Start date of capacity search |
 **end_date** | [**string**](../Model/.md)| End date of capacity search (time span should be less than 3 months) |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSCapacitySlotInfo[]**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSCapacitySlotInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xmltext/xmlapplication/jsontext/json
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resellerApiGetResellerOrders**
> \Swagger\Client\Model\STCTicketsModelsResellerOrderResponse resellerApiGetResellerOrders($app_namespace, $language_code)

Get reseller orders

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ResellerApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$app_namespace = "app_namespace_example"; // string | 
$language_code = "language_code_example"; // string | 

try {
    $result = $apiInstance->resellerApiGetResellerOrders($app_namespace, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ResellerApiApi->resellerApiGetResellerOrders: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_namespace** | [**string**](../Model/.md)|  |
 **language_code** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\STCTicketsModelsResellerOrderResponse**](../Model/STCTicketsModelsResellerOrderResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resellerApiGetResellerPaymentMethods**
> \Swagger\Client\Model\STCTicketsModelsPaymentMethodsResponse resellerApiGetResellerPaymentMethods($body$app_namespace, $language_code)

Returns the payment methods

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ResellerApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsResellerMessagesResellerPaymentMethodsRequest(); // \Swagger\Client\Model\LastSeatsGeneralObjectsResellerMessagesResellerPaymentMethodsRequest | 
$app_namespace = "app_namespace_example"; // string | 
$language_code = "language_code_example"; // string | 

try {
    $result = $apiInstance->resellerApiGetResellerPaymentMethods($body$app_namespace, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ResellerApiApi->resellerApiGetResellerPaymentMethods: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsResellerMessagesResellerPaymentMethodsRequest**](../Model/LastSeatsGeneralObjectsResellerMessagesResellerPaymentMethodsRequest.md)|  |
 **app_namespace** | [**string**](../Model/.md)|  |
 **language_code** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\STCTicketsModelsPaymentMethodsResponse**](../Model/STCTicketsModelsPaymentMethodsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xmltext/xmlapplication/jsontext/json
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resellerApiGetResellerProducts**
> \Swagger\Client\Model\LastSeatsGeneralObjectsResellerMessagesResellerProductsResponse resellerApiGetResellerProducts($app_namespace, $language_code)

Returns all the events a reseller can sell

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ResellerApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$app_namespace = "app_namespace_example"; // string | The selling partner of the tickets
$language_code = "language_code_example"; // string | Language codes are in the following format nl-NL, fr-FR.

try {
    $result = $apiInstance->resellerApiGetResellerProducts($app_namespace, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ResellerApiApi->resellerApiGetResellerProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_namespace** | [**string**](../Model/.md)| The selling partner of the tickets |
 **language_code** | [**string**](../Model/.md)| Language codes are in the following format nl-NL, fr-FR. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsResellerMessagesResellerProductsResponse**](../Model/LastSeatsGeneralObjectsResellerMessagesResellerProductsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resellerApiGetResellerShopPartnerInfo**
> \Swagger\Client\Model\STCTicketsModelsResellerShopPartnerInfoResponse resellerApiGetResellerShopPartnerInfo($app_namespace, $language_code)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ResellerApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$app_namespace = "app_namespace_example"; // string | 
$language_code = "language_code_example"; // string | 

try {
    $result = $apiInstance->resellerApiGetResellerShopPartnerInfo($app_namespace, $language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ResellerApiApi->resellerApiGetResellerShopPartnerInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_namespace** | [**string**](../Model/.md)|  |
 **language_code** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\STCTicketsModelsResellerShopPartnerInfoResponse**](../Model/STCTicketsModelsResellerShopPartnerInfoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resellerApiGetResellerTotalPrice**
> \Swagger\Client\Model\LastSeatsGeneralObjectsResellerMessagesResellerTotalPriceResponse resellerApiGetResellerTotalPrice($body$app_namespace)

Returns the total price of the selected tickets

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ResellerApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsResellerMessagesResellerTotalPriceRequest(); // \Swagger\Client\Model\LastSeatsGeneralObjectsResellerMessagesResellerTotalPriceRequest | 
$app_namespace = "app_namespace_example"; // string | 

try {
    $result = $apiInstance->resellerApiGetResellerTotalPrice($body$app_namespace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ResellerApiApi->resellerApiGetResellerTotalPrice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsResellerMessagesResellerTotalPriceRequest**](../Model/LastSeatsGeneralObjectsResellerMessagesResellerTotalPriceRequest.md)|  |
 **app_namespace** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsResellerMessagesResellerTotalPriceResponse**](../Model/LastSeatsGeneralObjectsResellerMessagesResellerTotalPriceResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xmltext/xmlapplication/jsontext/json
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resellerApiGetTickets**
> \Swagger\Client\Model\STCTicketsModelsResellerTicketResponse resellerApiGetTickets($app_namespace, $language_code, $basket_key)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ResellerApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$app_namespace = "app_namespace_example"; // string | 
$language_code = "language_code_example"; // string | 
$basket_key = "basket_key_example"; // string | 

try {
    $result = $apiInstance->resellerApiGetTickets($app_namespace, $language_code, $basket_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ResellerApiApi->resellerApiGetTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_namespace** | [**string**](../Model/.md)|  |
 **language_code** | [**string**](../Model/.md)|  |
 **basket_key** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\STCTicketsModelsResellerTicketResponse**](../Model/STCTicketsModelsResellerTicketResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

