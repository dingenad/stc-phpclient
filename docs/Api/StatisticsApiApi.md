# Swagger\Client\StatisticsApiApi

All URIs are relative to *https://ticketstest.ticketcounter.nl/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**statisticsApiGetSoldSubscriptions**](StatisticsApiApi.md#statisticsApiGetSoldSubscriptions) | **GET** /api/v1/statistics/soldsubscriptions/{languageCode} | Get sold subscriptions
[**statisticsApiGetSoldTickets**](StatisticsApiApi.md#statisticsApiGetSoldTickets) | **GET** /api/v1/statistics/soldtickets/{languageCode} | Get sold tickets
[**statisticsApiGetTicketScans**](StatisticsApiApi.md#statisticsApiGetTicketScans) | **GET** /api/v1/statistics/ticketScans/{languageCode} | Get ticket scans

# **statisticsApiGetSoldSubscriptions**
> \Swagger\Client\Model\LastSeatsGeneralObjectsStatisticsMessagesSoldSubscriptionsResponse statisticsApiGetSoldSubscriptions($language_code, $from_date, $to_date, $offset, $limit)

Get sold subscriptions

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\StatisticsApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$language_code = "language_code_example"; // string | Desired language for translatable information. Example: <code>nl-NL</code>
$from_date = "from_date_example"; // string | Start of date range
$to_date = "to_date_example"; // string | End of date range
$offset = 56; // int | Number of records to skip (default: 0)
$limit = 56; // int | Maximum number of records to retrieve (default: 9999999, reasonable limit is 10000)

try {
    $result = $apiInstance->statisticsApiGetSoldSubscriptions($language_code, $from_date, $to_date, $offset, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatisticsApiApi->statisticsApiGetSoldSubscriptions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language_code** | [**string**](../Model/.md)| Desired language for translatable information. Example: &lt;code&gt;nl-NL&lt;/code&gt; |
 **from_date** | [**string**](../Model/.md)| Start of date range |
 **to_date** | [**string**](../Model/.md)| End of date range |
 **offset** | [**int**](../Model/.md)| Number of records to skip (default: 0) | [optional]
 **limit** | [**int**](../Model/.md)| Maximum number of records to retrieve (default: 9999999, reasonable limit is 10000) | [optional]

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsStatisticsMessagesSoldSubscriptionsResponse**](../Model/LastSeatsGeneralObjectsStatisticsMessagesSoldSubscriptionsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **statisticsApiGetSoldTickets**
> \Swagger\Client\Model\LastSeatsGeneralObjectsStatisticsMessagesSoldTicketsResponse statisticsApiGetSoldTickets($language_code, $from_date, $to_date, $offset, $limit)

Get sold tickets

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\StatisticsApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$language_code = "language_code_example"; // string | Desired language for translatable information. Example: <code>nl-NL</code>
$from_date = "from_date_example"; // string | Start of date range
$to_date = "to_date_example"; // string | End of date range
$offset = 56; // int | Number of records to skip (default: 0)
$limit = 56; // int | Maximum number of records to retrieve (default: 9999999, reasonable limit is 10000)

try {
    $result = $apiInstance->statisticsApiGetSoldTickets($language_code, $from_date, $to_date, $offset, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatisticsApiApi->statisticsApiGetSoldTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language_code** | [**string**](../Model/.md)| Desired language for translatable information. Example: &lt;code&gt;nl-NL&lt;/code&gt; |
 **from_date** | [**string**](../Model/.md)| Start of date range |
 **to_date** | [**string**](../Model/.md)| End of date range |
 **offset** | [**int**](../Model/.md)| Number of records to skip (default: 0) | [optional]
 **limit** | [**int**](../Model/.md)| Maximum number of records to retrieve (default: 9999999, reasonable limit is 10000) | [optional]

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsStatisticsMessagesSoldTicketsResponse**](../Model/LastSeatsGeneralObjectsStatisticsMessagesSoldTicketsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **statisticsApiGetTicketScans**
> \Swagger\Client\Model\LastSeatsGeneralObjectsStatisticsMessagesTicketScanResponse statisticsApiGetTicketScans($language_code, $from_date, $to_date, $scan_type, $offset, $limit)

Get ticket scans

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\StatisticsApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$language_code = "language_code_example"; // string | Desired language for translatable information. Example: <code>nl-NL</code>
$from_date = "from_date_example"; // string | Start of date range
$to_date = "to_date_example"; // string | End of date range
$scan_type = "scan_type_example"; // string | Product types to include (default: All)
$offset = 56; // int | Number of records to skip (default: 0)
$limit = 56; // int | Maximum number of records to retrieve (default: 9999999, reasonable limit is 10000)

try {
    $result = $apiInstance->statisticsApiGetTicketScans($language_code, $from_date, $to_date, $scan_type, $offset, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatisticsApiApi->statisticsApiGetTicketScans: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language_code** | [**string**](../Model/.md)| Desired language for translatable information. Example: &lt;code&gt;nl-NL&lt;/code&gt; |
 **from_date** | [**string**](../Model/.md)| Start of date range |
 **to_date** | [**string**](../Model/.md)| End of date range |
 **scan_type** | [**string**](../Model/.md)| Product types to include (default: All) | [optional]
 **offset** | [**int**](../Model/.md)| Number of records to skip (default: 0) | [optional]
 **limit** | [**int**](../Model/.md)| Maximum number of records to retrieve (default: 9999999, reasonable limit is 10000) | [optional]

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsStatisticsMessagesTicketScanResponse**](../Model/LastSeatsGeneralObjectsStatisticsMessagesTicketScanResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/jsontext/jsonapplication/xmltext/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

