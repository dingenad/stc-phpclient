# Swagger\Client\SubscriptionApiApi

All URIs are relative to *https://ticketstest.ticketcounter.nl/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**subscriptionApiCreateSubscription**](SubscriptionApiApi.md#subscriptionApiCreateSubscription) | **POST** /api/v1/subscription/register/{languageCode} | Create a subscription registration.
[**subscriptionApiForgotPassword**](SubscriptionApiApi.md#subscriptionApiForgotPassword) | **POST** /api/v1/subscription/ForgotPassword | Send a forgot password request.
[**subscriptionApiGetCountries**](SubscriptionApiApi.md#subscriptionApiGetCountries) | **GET** /api/v1/subscription/Countries/{languageCode} | 
[**subscriptionApiGetExistingEmailAddress**](SubscriptionApiApi.md#subscriptionApiGetExistingEmailAddress) | **POST** /api/v1/subscription/ExistingEmailAddress | Check if a e-mail address already exists.
[**subscriptionApiGetPartnerAddressInfo**](SubscriptionApiApi.md#subscriptionApiGetPartnerAddressInfo) | **GET** /api/v1/subscription/PartnerAddressInfo | 
[**subscriptionApiGetPaymentMethods**](SubscriptionApiApi.md#subscriptionApiGetPaymentMethods) | **GET** /api/v1/subscription/PaymentMethods | Get the payment methods.
[**subscriptionApiGetRealValidFrom**](SubscriptionApiApi.md#subscriptionApiGetRealValidFrom) | **POST** /api/v1/subscription/GetRealValidFrom | Get the valid from date of a subscription template. This date indicates when a subscription can be used.
[**subscriptionApiGetSubscriptionAmount**](SubscriptionApiApi.md#subscriptionApiGetSubscriptionAmount) | **POST** /api/v1/subscription/subscriptionAmount/{languageCode} | Get the total amount of the selected template and aggregated fields the user needs to fill in.
[**subscriptionApiGetSubscriptionTemplates**](SubscriptionApiApi.md#subscriptionApiGetSubscriptionTemplates) | **POST** /api/v1/subscription/SubscriptionTemplates/{languageCode} | Get the available subscription templates
[**subscriptionApiLogin**](SubscriptionApiApi.md#subscriptionApiLogin) | **POST** /api/v1/subscription/Login/{languageCode} | Login a user.

# **subscriptionApiCreateSubscription**
> \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionRegistrationResponse subscriptionApiCreateSubscription($body$language_code)

Create a subscription registration.

Register one or more subscriptions. This method starts the registration process.               When everything is filled in correctly, the user should be redirected to the payment provider.              When the payment is finished, the registration status will be changed to completed and e-mails will be sent to the user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SubscriptionApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDtosSubscriptionRegistrationDto(); // \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDtosSubscriptionRegistrationDto | The registration information
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->subscriptionApiCreateSubscription($body$language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubscriptionApiApi->subscriptionApiCreateSubscription: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDtosSubscriptionRegistrationDto**](../Model/LastSeatsGeneralObjectsSubscriptionDtosSubscriptionRegistrationDto.md)| The registration information |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionRegistrationResponse**](../Model/LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionRegistrationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xmltext/xmlapplication/jsontext/json
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **subscriptionApiForgotPassword**
> \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesResponse subscriptionApiForgotPassword($body)

Send a forgot password request.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SubscriptionApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesEmailAddressRequest(); // \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesEmailAddressRequest | Specify the user to reset the password for.

try {
    $result = $apiInstance->subscriptionApiForgotPassword($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubscriptionApiApi->subscriptionApiForgotPassword: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesEmailAddressRequest**](../Model/LastSeatsGeneralObjectsSubscriptionMessagesEmailAddressRequest.md)| Specify the user to reset the password for. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesResponse**](../Model/LastSeatsGeneralObjectsSubscriptionMessagesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xmltext/xmlapplication/jsontext/json
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **subscriptionApiGetCountries**
> \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesCountriesResponse subscriptionApiGetCountries($language_code)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SubscriptionApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$language_code = "language_code_example"; // string | 

try {
    $result = $apiInstance->subscriptionApiGetCountries($language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubscriptionApiApi->subscriptionApiGetCountries: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language_code** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesCountriesResponse**](../Model/LastSeatsGeneralObjectsSubscriptionMessagesCountriesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **subscriptionApiGetExistingEmailAddress**
> bool subscriptionApiGetExistingEmailAddress($body)

Check if a e-mail address already exists.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SubscriptionApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesEmailAddressRequest(); // \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesEmailAddressRequest | The e-mail addres to check.

try {
    $result = $apiInstance->subscriptionApiGetExistingEmailAddress($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubscriptionApiApi->subscriptionApiGetExistingEmailAddress: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesEmailAddressRequest**](../Model/LastSeatsGeneralObjectsSubscriptionMessagesEmailAddressRequest.md)| The e-mail addres to check. |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xmltext/xmlapplication/jsontext/json
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **subscriptionApiGetPartnerAddressInfo**
> \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSAddressInfo subscriptionApiGetPartnerAddressInfo()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SubscriptionApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->subscriptionApiGetPartnerAddressInfo();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubscriptionApiApi->subscriptionApiGetPartnerAddressInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSAddressInfo**](../Model/LastSeatsGeneralObjectsTicketServiceClassesTSAddressInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **subscriptionApiGetPaymentMethods**
> \Swagger\Client\Model\STCTicketsModelsPaymentMethodsResponse subscriptionApiGetPaymentMethods()

Get the payment methods.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SubscriptionApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->subscriptionApiGetPaymentMethods();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubscriptionApiApi->subscriptionApiGetPaymentMethods: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\STCTicketsModelsPaymentMethodsResponse**](../Model/STCTicketsModelsPaymentMethodsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **subscriptionApiGetRealValidFrom**
> \DateTime subscriptionApiGetRealValidFrom($body)

Get the valid from date of a subscription template. This date indicates when a subscription can be used.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SubscriptionApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = 56; // int | The subscription Id

try {
    $result = $apiInstance->subscriptionApiGetRealValidFrom($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubscriptionApiApi->subscriptionApiGetRealValidFrom: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**int**](../Model/int.md)| The subscription Id |

### Return type

[**\DateTime**](../Model/\DateTime.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xmltext/xmlapplication/jsontext/json
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **subscriptionApiGetSubscriptionAmount**
> \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateTotalAmountResponse subscriptionApiGetSubscriptionAmount($body$language_code)

Get the total amount of the selected template and aggregated fields the user needs to fill in.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SubscriptionApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateTotalAmountRequest(); // \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateTotalAmountRequest | The selected template to get the amount for; the other subscription templates and discounts.
$language_code = "language_code_example"; // string | 

try {
    $result = $apiInstance->subscriptionApiGetSubscriptionAmount($body$language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubscriptionApiApi->subscriptionApiGetSubscriptionAmount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateTotalAmountRequest**](../Model/LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateTotalAmountRequest.md)| The selected template to get the amount for; the other subscription templates and discounts. |
 **language_code** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateTotalAmountResponse**](../Model/LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateTotalAmountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xmltext/xmlapplication/jsontext/json
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **subscriptionApiGetSubscriptionTemplates**
> \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateResponse subscriptionApiGetSubscriptionTemplates($body$language_code)

Get the available subscription templates

Get the templates based on the request. When the user selects templates and discounts, the pricing or available subscriptions may change based on the internal business rules.              Therefore, it is recommended to get the subscription templates each time the user changes the selection of templates or enters discount codes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SubscriptionApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateRequest(); // \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateRequest | The selected subscriptions and discounts.
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->subscriptionApiGetSubscriptionTemplates($body$language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubscriptionApiApi->subscriptionApiGetSubscriptionTemplates: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateRequest**](../Model/LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateRequest.md)| The selected subscriptions and discounts. |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateResponse**](../Model/LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xmltext/xmlapplication/jsontext/json
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **subscriptionApiLogin**
> \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesLoginResponse subscriptionApiLogin($body$language_code)

Login a user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SubscriptionApiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesLoginRequest(); // \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesLoginRequest | Credentials
$language_code = "language_code_example"; // string | Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: <code>nl-NL</code>.

try {
    $result = $apiInstance->subscriptionApiLogin($body$language_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubscriptionApiApi->subscriptionApiLogin: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesLoginRequest**](../Model/LastSeatsGeneralObjectsSubscriptionMessagesLoginRequest.md)| Credentials |
 **language_code** | [**string**](../Model/.md)| Language code of the language the event information like locations, descriptions, city, state and country names must be returned in. Example: &lt;code&gt;nl-NL&lt;/code&gt;. |

### Return type

[**\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionMessagesLoginResponse**](../Model/LastSeatsGeneralObjectsSubscriptionMessagesLoginResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xmltext/xmlapplication/jsontext/json
 - **Accept**: application/xmltext/xmlapplication/jsontext/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

