<?php
/**
 * LastSeatsGeneralObjectsTicketServiceClassesTSBasketSubscriptionReservationsInfo
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * LastSeatsGeneralObjectsTicketServiceClassesTSBasketSubscriptionReservationsInfo Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class LastSeatsGeneralObjectsTicketServiceClassesTSBasketSubscriptionReservationsInfo implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'LastSeats.GeneralObjects.TicketServiceClasses.TSBasketSubscriptionReservationsInfo';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'basket_key' => 'string',
'currency_code' => 'string',
'basket_number' => 'string',
'external_basket_number' => 'string',
'status' => 'string',
'confirmed_date' => '\DateTime',
'payment_method' => 'string',
'reservation_amount' => 'double',
'total_amount' => 'double',
'transaction_costs' => 'double',
'payment_costs' => 'double',
'discount' => 'double',
'reservations' => '\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo[]',
'contact_id' => 'int',
'reservation_subscriptions' => '\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionTSReservationSubscriptionInfo[]'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'basket_key' => 'uuid',
'currency_code' => null,
'basket_number' => null,
'external_basket_number' => null,
'status' => null,
'confirmed_date' => 'date-time',
'payment_method' => null,
'reservation_amount' => 'double',
'total_amount' => 'double',
'transaction_costs' => 'double',
'payment_costs' => 'double',
'discount' => 'double',
'reservations' => null,
'contact_id' => 'int32',
'reservation_subscriptions' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'basket_key' => 'BasketKey',
'currency_code' => 'CurrencyCode',
'basket_number' => 'BasketNumber',
'external_basket_number' => 'ExternalBasketNumber',
'status' => 'Status',
'confirmed_date' => 'ConfirmedDate',
'payment_method' => 'PaymentMethod',
'reservation_amount' => 'ReservationAmount',
'total_amount' => 'TotalAmount',
'transaction_costs' => 'TransactionCosts',
'payment_costs' => 'PaymentCosts',
'discount' => 'Discount',
'reservations' => 'Reservations',
'contact_id' => 'ContactID',
'reservation_subscriptions' => 'ReservationSubscriptions'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'basket_key' => 'setBasketKey',
'currency_code' => 'setCurrencyCode',
'basket_number' => 'setBasketNumber',
'external_basket_number' => 'setExternalBasketNumber',
'status' => 'setStatus',
'confirmed_date' => 'setConfirmedDate',
'payment_method' => 'setPaymentMethod',
'reservation_amount' => 'setReservationAmount',
'total_amount' => 'setTotalAmount',
'transaction_costs' => 'setTransactionCosts',
'payment_costs' => 'setPaymentCosts',
'discount' => 'setDiscount',
'reservations' => 'setReservations',
'contact_id' => 'setContactId',
'reservation_subscriptions' => 'setReservationSubscriptions'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'basket_key' => 'getBasketKey',
'currency_code' => 'getCurrencyCode',
'basket_number' => 'getBasketNumber',
'external_basket_number' => 'getExternalBasketNumber',
'status' => 'getStatus',
'confirmed_date' => 'getConfirmedDate',
'payment_method' => 'getPaymentMethod',
'reservation_amount' => 'getReservationAmount',
'total_amount' => 'getTotalAmount',
'transaction_costs' => 'getTransactionCosts',
'payment_costs' => 'getPaymentCosts',
'discount' => 'getDiscount',
'reservations' => 'getReservations',
'contact_id' => 'getContactId',
'reservation_subscriptions' => 'getReservationSubscriptions'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['basket_key'] = isset($data['basket_key']) ? $data['basket_key'] : null;
        $this->container['currency_code'] = isset($data['currency_code']) ? $data['currency_code'] : null;
        $this->container['basket_number'] = isset($data['basket_number']) ? $data['basket_number'] : null;
        $this->container['external_basket_number'] = isset($data['external_basket_number']) ? $data['external_basket_number'] : null;
        $this->container['status'] = isset($data['status']) ? $data['status'] : null;
        $this->container['confirmed_date'] = isset($data['confirmed_date']) ? $data['confirmed_date'] : null;
        $this->container['payment_method'] = isset($data['payment_method']) ? $data['payment_method'] : null;
        $this->container['reservation_amount'] = isset($data['reservation_amount']) ? $data['reservation_amount'] : null;
        $this->container['total_amount'] = isset($data['total_amount']) ? $data['total_amount'] : null;
        $this->container['transaction_costs'] = isset($data['transaction_costs']) ? $data['transaction_costs'] : null;
        $this->container['payment_costs'] = isset($data['payment_costs']) ? $data['payment_costs'] : null;
        $this->container['discount'] = isset($data['discount']) ? $data['discount'] : null;
        $this->container['reservations'] = isset($data['reservations']) ? $data['reservations'] : null;
        $this->container['contact_id'] = isset($data['contact_id']) ? $data['contact_id'] : null;
        $this->container['reservation_subscriptions'] = isset($data['reservation_subscriptions']) ? $data['reservation_subscriptions'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        return true;
    }


    /**
     * Gets basket_key
     *
     * @return string
     */
    public function getBasketKey()
    {
        return $this->container['basket_key'];
    }

    /**
     * Sets basket_key
     *
     * @param string $basket_key The unique key of this basket
     *
     * @return $this
     */
    public function setBasketKey($basket_key)
    {
        $this->container['basket_key'] = $basket_key;

        return $this;
    }

    /**
     * Gets currency_code
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->container['currency_code'];
    }

    /**
     * Sets currency_code
     *
     * @param string $currency_code The currency that this basket must be paid in
     *
     * @return $this
     */
    public function setCurrencyCode($currency_code)
    {
        $this->container['currency_code'] = $currency_code;

        return $this;
    }

    /**
     * Gets basket_number
     *
     * @return string
     */
    public function getBasketNumber()
    {
        return $this->container['basket_number'];
    }

    /**
     * Sets basket_number
     *
     * @param string $basket_number The (customer) number that this basket has
     *
     * @return $this
     */
    public function setBasketNumber($basket_number)
    {
        $this->container['basket_number'] = $basket_number;

        return $this;
    }

    /**
     * Gets external_basket_number
     *
     * @return string
     */
    public function getExternalBasketNumber()
    {
        return $this->container['external_basket_number'];
    }

    /**
     * Sets external_basket_number
     *
     * @param string $external_basket_number The external number, as supplied by another system
     *
     * @return $this
     */
    public function setExternalBasketNumber($external_basket_number)
    {
        $this->container['external_basket_number'] = $external_basket_number;

        return $this;
    }

    /**
     * Gets status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->container['status'];
    }

    /**
     * Sets status
     *
     * @param string $status The status of the basket
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->container['status'] = $status;

        return $this;
    }

    /**
     * Gets confirmed_date
     *
     * @return \DateTime
     */
    public function getConfirmedDate()
    {
        return $this->container['confirmed_date'];
    }

    /**
     * Sets confirmed_date
     *
     * @param \DateTime $confirmed_date The date this basket was confirmed
     *
     * @return $this
     */
    public function setConfirmedDate($confirmed_date)
    {
        $this->container['confirmed_date'] = $confirmed_date;

        return $this;
    }

    /**
     * Gets payment_method
     *
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->container['payment_method'];
    }

    /**
     * Sets payment_method
     *
     * @param string $payment_method The date this basket was confirmed
     *
     * @return $this
     */
    public function setPaymentMethod($payment_method)
    {
        $this->container['payment_method'] = $payment_method;

        return $this;
    }

    /**
     * Gets reservation_amount
     *
     * @return double
     */
    public function getReservationAmount()
    {
        return $this->container['reservation_amount'];
    }

    /**
     * Sets reservation_amount
     *
     * @param double $reservation_amount The total amount of the reservations in the basket
     *
     * @return $this
     */
    public function setReservationAmount($reservation_amount)
    {
        $this->container['reservation_amount'] = $reservation_amount;

        return $this;
    }

    /**
     * Gets total_amount
     *
     * @return double
     */
    public function getTotalAmount()
    {
        return $this->container['total_amount'];
    }

    /**
     * Sets total_amount
     *
     * @param double $total_amount The total 'value' of the basket (including costs and discount)
     *
     * @return $this
     */
    public function setTotalAmount($total_amount)
    {
        $this->container['total_amount'] = $total_amount;

        return $this;
    }

    /**
     * Gets transaction_costs
     *
     * @return double
     */
    public function getTransactionCosts()
    {
        return $this->container['transaction_costs'];
    }

    /**
     * Sets transaction_costs
     *
     * @param double $transaction_costs The transaction costs for this basket
     *
     * @return $this
     */
    public function setTransactionCosts($transaction_costs)
    {
        $this->container['transaction_costs'] = $transaction_costs;

        return $this;
    }

    /**
     * Gets payment_costs
     *
     * @return double
     */
    public function getPaymentCosts()
    {
        return $this->container['payment_costs'];
    }

    /**
     * Sets payment_costs
     *
     * @param double $payment_costs The payment costs for the basket
     *
     * @return $this
     */
    public function setPaymentCosts($payment_costs)
    {
        $this->container['payment_costs'] = $payment_costs;

        return $this;
    }

    /**
     * Gets discount
     *
     * @return double
     */
    public function getDiscount()
    {
        return $this->container['discount'];
    }

    /**
     * Sets discount
     *
     * @param double $discount The discount that applies
     *
     * @return $this
     */
    public function setDiscount($discount)
    {
        $this->container['discount'] = $discount;

        return $this;
    }

    /**
     * Gets reservations
     *
     * @return \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo[]
     */
    public function getReservations()
    {
        return $this->container['reservations'];
    }

    /**
     * Sets reservations
     *
     * @param \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationInfo[] $reservations A list with information about the reservations in the basket
     *
     * @return $this
     */
    public function setReservations($reservations)
    {
        $this->container['reservations'] = $reservations;

        return $this;
    }

    /**
     * Gets contact_id
     *
     * @return int
     */
    public function getContactId()
    {
        return $this->container['contact_id'];
    }

    /**
     * Sets contact_id
     *
     * @param int $contact_id Optional: contact that is linked to the basket
     *
     * @return $this
     */
    public function setContactId($contact_id)
    {
        $this->container['contact_id'] = $contact_id;

        return $this;
    }

    /**
     * Gets reservation_subscriptions
     *
     * @return \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionTSReservationSubscriptionInfo[]
     */
    public function getReservationSubscriptions()
    {
        return $this->container['reservation_subscriptions'];
    }

    /**
     * Sets reservation_subscriptions
     *
     * @param \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionTSReservationSubscriptionInfo[] $reservation_subscriptions A list with information about the reservations in the basket
     *
     * @return $this
     */
    public function setReservationSubscriptions($reservation_subscriptions)
    {
        $this->container['reservation_subscriptions'] = $reservation_subscriptions;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
