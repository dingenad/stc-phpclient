<?php
/**
 * LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'LastSeats.GeneralObjects.TicketServiceClasses.TSContactInfo';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'id' => 'int',
'contact_key' => 'string',
'company_name' => 'string',
'vat_number' => 'string',
'gender' => 'string',
'first_name' => 'string',
'middle' => 'string',
'last_name' => 'string',
'birth_date' => '\DateTime',
'phone_number' => 'string',
'mobile_number' => 'string',
'mail_address' => 'string',
'receive_newsletter' => 'bool',
'receive_invoice' => 'bool',
'optin1' => 'bool',
'optin2' => 'bool',
'optin3' => 'bool',
'password' => 'string',
'address' => '\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSAddressInfo',
'full_name' => 'string',
'language_code' => 'string',
'receive_passbooks' => 'bool'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'id' => 'int32',
'contact_key' => 'uuid',
'company_name' => null,
'vat_number' => null,
'gender' => null,
'first_name' => null,
'middle' => null,
'last_name' => null,
'birth_date' => 'date-time',
'phone_number' => null,
'mobile_number' => null,
'mail_address' => null,
'receive_newsletter' => null,
'receive_invoice' => null,
'optin1' => null,
'optin2' => null,
'optin3' => null,
'password' => null,
'address' => null,
'full_name' => null,
'language_code' => null,
'receive_passbooks' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'Id',
'contact_key' => 'ContactKey',
'company_name' => 'CompanyName',
'vat_number' => 'VatNumber',
'gender' => 'Gender',
'first_name' => 'FirstName',
'middle' => 'Middle',
'last_name' => 'LastName',
'birth_date' => 'BirthDate',
'phone_number' => 'PhoneNumber',
'mobile_number' => 'MobileNumber',
'mail_address' => 'MailAddress',
'receive_newsletter' => 'ReceiveNewsletter',
'receive_invoice' => 'ReceiveInvoice',
'optin1' => 'Optin1',
'optin2' => 'Optin2',
'optin3' => 'Optin3',
'password' => 'Password',
'address' => 'Address',
'full_name' => 'FullName',
'language_code' => 'LanguageCode',
'receive_passbooks' => 'ReceivePassbooks'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
'contact_key' => 'setContactKey',
'company_name' => 'setCompanyName',
'vat_number' => 'setVatNumber',
'gender' => 'setGender',
'first_name' => 'setFirstName',
'middle' => 'setMiddle',
'last_name' => 'setLastName',
'birth_date' => 'setBirthDate',
'phone_number' => 'setPhoneNumber',
'mobile_number' => 'setMobileNumber',
'mail_address' => 'setMailAddress',
'receive_newsletter' => 'setReceiveNewsletter',
'receive_invoice' => 'setReceiveInvoice',
'optin1' => 'setOptin1',
'optin2' => 'setOptin2',
'optin3' => 'setOptin3',
'password' => 'setPassword',
'address' => 'setAddress',
'full_name' => 'setFullName',
'language_code' => 'setLanguageCode',
'receive_passbooks' => 'setReceivePassbooks'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
'contact_key' => 'getContactKey',
'company_name' => 'getCompanyName',
'vat_number' => 'getVatNumber',
'gender' => 'getGender',
'first_name' => 'getFirstName',
'middle' => 'getMiddle',
'last_name' => 'getLastName',
'birth_date' => 'getBirthDate',
'phone_number' => 'getPhoneNumber',
'mobile_number' => 'getMobileNumber',
'mail_address' => 'getMailAddress',
'receive_newsletter' => 'getReceiveNewsletter',
'receive_invoice' => 'getReceiveInvoice',
'optin1' => 'getOptin1',
'optin2' => 'getOptin2',
'optin3' => 'getOptin3',
'password' => 'getPassword',
'address' => 'getAddress',
'full_name' => 'getFullName',
'language_code' => 'getLanguageCode',
'receive_passbooks' => 'getReceivePassbooks'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['contact_key'] = isset($data['contact_key']) ? $data['contact_key'] : null;
        $this->container['company_name'] = isset($data['company_name']) ? $data['company_name'] : null;
        $this->container['vat_number'] = isset($data['vat_number']) ? $data['vat_number'] : null;
        $this->container['gender'] = isset($data['gender']) ? $data['gender'] : null;
        $this->container['first_name'] = isset($data['first_name']) ? $data['first_name'] : null;
        $this->container['middle'] = isset($data['middle']) ? $data['middle'] : null;
        $this->container['last_name'] = isset($data['last_name']) ? $data['last_name'] : null;
        $this->container['birth_date'] = isset($data['birth_date']) ? $data['birth_date'] : null;
        $this->container['phone_number'] = isset($data['phone_number']) ? $data['phone_number'] : null;
        $this->container['mobile_number'] = isset($data['mobile_number']) ? $data['mobile_number'] : null;
        $this->container['mail_address'] = isset($data['mail_address']) ? $data['mail_address'] : null;
        $this->container['receive_newsletter'] = isset($data['receive_newsletter']) ? $data['receive_newsletter'] : null;
        $this->container['receive_invoice'] = isset($data['receive_invoice']) ? $data['receive_invoice'] : null;
        $this->container['optin1'] = isset($data['optin1']) ? $data['optin1'] : null;
        $this->container['optin2'] = isset($data['optin2']) ? $data['optin2'] : null;
        $this->container['optin3'] = isset($data['optin3']) ? $data['optin3'] : null;
        $this->container['password'] = isset($data['password']) ? $data['password'] : null;
        $this->container['address'] = isset($data['address']) ? $data['address'] : null;
        $this->container['full_name'] = isset($data['full_name']) ? $data['full_name'] : null;
        $this->container['language_code'] = isset($data['language_code']) ? $data['language_code'] : null;
        $this->container['receive_passbooks'] = isset($data['receive_passbooks']) ? $data['receive_passbooks'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        return true;
    }


    /**
     * Gets id
     *
     * @return int
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int $id Contact ID.
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets contact_key
     *
     * @return string
     */
    public function getContactKey()
    {
        return $this->container['contact_key'];
    }

    /**
     * Sets contact_key
     *
     * @param string $contact_key The key of the contact that must be used for further communication.
     *
     * @return $this
     */
    public function setContactKey($contact_key)
    {
        $this->container['contact_key'] = $contact_key;

        return $this;
    }

    /**
     * Gets company_name
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->container['company_name'];
    }

    /**
     * Sets company_name
     *
     * @param string $company_name The name of the company
     *
     * @return $this
     */
    public function setCompanyName($company_name)
    {
        $this->container['company_name'] = $company_name;

        return $this;
    }

    /**
     * Gets vat_number
     *
     * @return string
     */
    public function getVatNumber()
    {
        return $this->container['vat_number'];
    }

    /**
     * Sets vat_number
     *
     * @param string $vat_number The VAT number
     *
     * @return $this
     */
    public function setVatNumber($vat_number)
    {
        $this->container['vat_number'] = $vat_number;

        return $this;
    }

    /**
     * Gets gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->container['gender'];
    }

    /**
     * Sets gender
     *
     * @param string $gender The gender ('M', male or 'F', female)
     *
     * @return $this
     */
    public function setGender($gender)
    {
        $this->container['gender'] = $gender;

        return $this;
    }

    /**
     * Gets first_name
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->container['first_name'];
    }

    /**
     * Sets first_name
     *
     * @param string $first_name The first name
     *
     * @return $this
     */
    public function setFirstName($first_name)
    {
        $this->container['first_name'] = $first_name;

        return $this;
    }

    /**
     * Gets middle
     *
     * @return string
     */
    public function getMiddle()
    {
        return $this->container['middle'];
    }

    /**
     * Sets middle
     *
     * @param string $middle The middle name
     *
     * @return $this
     */
    public function setMiddle($middle)
    {
        $this->container['middle'] = $middle;

        return $this;
    }

    /**
     * Gets last_name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->container['last_name'];
    }

    /**
     * Sets last_name
     *
     * @param string $last_name The last name
     *
     * @return $this
     */
    public function setLastName($last_name)
    {
        $this->container['last_name'] = $last_name;

        return $this;
    }

    /**
     * Gets birth_date
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->container['birth_date'];
    }

    /**
     * Sets birth_date
     *
     * @param \DateTime $birth_date The date of birth
     *
     * @return $this
     */
    public function setBirthDate($birth_date)
    {
        $this->container['birth_date'] = $birth_date;

        return $this;
    }

    /**
     * Gets phone_number
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->container['phone_number'];
    }

    /**
     * Sets phone_number
     *
     * @param string $phone_number The phone number
     *
     * @return $this
     */
    public function setPhoneNumber($phone_number)
    {
        $this->container['phone_number'] = $phone_number;

        return $this;
    }

    /**
     * Gets mobile_number
     *
     * @return string
     */
    public function getMobileNumber()
    {
        return $this->container['mobile_number'];
    }

    /**
     * Sets mobile_number
     *
     * @param string $mobile_number The mobile phone number
     *
     * @return $this
     */
    public function setMobileNumber($mobile_number)
    {
        $this->container['mobile_number'] = $mobile_number;

        return $this;
    }

    /**
     * Gets mail_address
     *
     * @return string
     */
    public function getMailAddress()
    {
        return $this->container['mail_address'];
    }

    /**
     * Sets mail_address
     *
     * @param string $mail_address The e-mail address
     *
     * @return $this
     */
    public function setMailAddress($mail_address)
    {
        $this->container['mail_address'] = $mail_address;

        return $this;
    }

    /**
     * Gets receive_newsletter
     *
     * @return bool
     */
    public function getReceiveNewsletter()
    {
        return $this->container['receive_newsletter'];
    }

    /**
     * Sets receive_newsletter
     *
     * @param bool $receive_newsletter <code>true</code> when the contact wants to receive a newsletter; otherwise, <code>false</code>.
     *
     * @return $this
     */
    public function setReceiveNewsletter($receive_newsletter)
    {
        $this->container['receive_newsletter'] = $receive_newsletter;

        return $this;
    }

    /**
     * Gets receive_invoice
     *
     * @return bool
     */
    public function getReceiveInvoice()
    {
        return $this->container['receive_invoice'];
    }

    /**
     * Sets receive_invoice
     *
     * @param bool $receive_invoice <code>true</code> when the contact wants to receive the invoice; otherwise, <code>false</code>.
     *
     * @return $this
     */
    public function setReceiveInvoice($receive_invoice)
    {
        $this->container['receive_invoice'] = $receive_invoice;

        return $this;
    }

    /**
     * Gets optin1
     *
     * @return bool
     */
    public function getOptin1()
    {
        return $this->container['optin1'];
    }

    /**
     * Sets optin1
     *
     * @param bool $optin1 <code>true</code> when the contact opts in for custom option 1; otherwise, <code>false</code>.
     *
     * @return $this
     */
    public function setOptin1($optin1)
    {
        $this->container['optin1'] = $optin1;

        return $this;
    }

    /**
     * Gets optin2
     *
     * @return bool
     */
    public function getOptin2()
    {
        return $this->container['optin2'];
    }

    /**
     * Sets optin2
     *
     * @param bool $optin2 <code>true</code> when the contact opts in for custom option 2; otherwise, <code>false</code>.
     *
     * @return $this
     */
    public function setOptin2($optin2)
    {
        $this->container['optin2'] = $optin2;

        return $this;
    }

    /**
     * Gets optin3
     *
     * @return bool
     */
    public function getOptin3()
    {
        return $this->container['optin3'];
    }

    /**
     * Sets optin3
     *
     * @param bool $optin3 <code>true</code> when the contact opts in for custom option 3; otherwise, <code>false</code>.
     *
     * @return $this
     */
    public function setOptin3($optin3)
    {
        $this->container['optin3'] = $optin3;

        return $this;
    }

    /**
     * Gets password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->container['password'];
    }

    /**
     * Sets password
     *
     * @param string $password The password (only if an account must be created)
     *
     * @return $this
     */
    public function setPassword($password)
    {
        $this->container['password'] = $password;

        return $this;
    }

    /**
     * Gets address
     *
     * @return \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSAddressInfo
     */
    public function getAddress()
    {
        return $this->container['address'];
    }

    /**
     * Sets address
     *
     * @param \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSAddressInfo $address address
     *
     * @return $this
     */
    public function setAddress($address)
    {
        $this->container['address'] = $address;

        return $this;
    }

    /**
     * Gets full_name
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->container['full_name'];
    }

    /**
     * Sets full_name
     *
     * @param string $full_name The full name of a contact
     *
     * @return $this
     */
    public function setFullName($full_name)
    {
        $this->container['full_name'] = $full_name;

        return $this;
    }

    /**
     * Gets language_code
     *
     * @return string
     */
    public function getLanguageCode()
    {
        return $this->container['language_code'];
    }

    /**
     * Sets language_code
     *
     * @param string $language_code The languagecode of a contact
     *
     * @return $this
     */
    public function setLanguageCode($language_code)
    {
        $this->container['language_code'] = $language_code;

        return $this;
    }

    /**
     * Gets receive_passbooks
     *
     * @return bool
     */
    public function getReceivePassbooks()
    {
        return $this->container['receive_passbooks'];
    }

    /**
     * Sets receive_passbooks
     *
     * @param bool $receive_passbooks <code>true</code> if the contact wants to receive Passbook tickets, otherwise <code>false</code>
     *
     * @return $this
     */
    public function setReceivePassbooks($receive_passbooks)
    {
        $this->container['receive_passbooks'] = $receive_passbooks;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
