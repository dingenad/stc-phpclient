<?php
/**
 * LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class LastSeatsGeneralObjectsTicketServiceClassesTSEventInfo implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'LastSeats.GeneralObjects.TicketServiceClasses.TSEventInfo';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'event_key' => 'string',
'event_name' => 'string',
'performer' => 'string',
'tagline' => 'string',
'description' => 'string',
'image_urls' => 'string[]',
'image_tags' => 'string[]',
'movie_link' => 'string',
'blocked_countries_for_sales' => 'string',
'rating' => 'double',
'review_count' => 'int',
'currency' => 'string',
'currency_code' => 'string',
'lowest_price' => 'double',
'categories' => '\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSCategoryInfo[]',
'performances' => '\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPerformanceInfo[]'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'event_key' => 'uuid',
'event_name' => null,
'performer' => null,
'tagline' => null,
'description' => null,
'image_urls' => null,
'image_tags' => null,
'movie_link' => null,
'blocked_countries_for_sales' => null,
'rating' => 'double',
'review_count' => 'int32',
'currency' => null,
'currency_code' => null,
'lowest_price' => 'double',
'categories' => null,
'performances' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'event_key' => 'EventKey',
'event_name' => 'EventName',
'performer' => 'Performer',
'tagline' => 'Tagline',
'description' => 'Description',
'image_urls' => 'ImageUrls',
'image_tags' => 'ImageTags',
'movie_link' => 'MovieLink',
'blocked_countries_for_sales' => 'BlockedCountriesForSales',
'rating' => 'Rating',
'review_count' => 'ReviewCount',
'currency' => 'Currency',
'currency_code' => 'CurrencyCode',
'lowest_price' => 'LowestPrice',
'categories' => 'Categories',
'performances' => 'Performances'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'event_key' => 'setEventKey',
'event_name' => 'setEventName',
'performer' => 'setPerformer',
'tagline' => 'setTagline',
'description' => 'setDescription',
'image_urls' => 'setImageUrls',
'image_tags' => 'setImageTags',
'movie_link' => 'setMovieLink',
'blocked_countries_for_sales' => 'setBlockedCountriesForSales',
'rating' => 'setRating',
'review_count' => 'setReviewCount',
'currency' => 'setCurrency',
'currency_code' => 'setCurrencyCode',
'lowest_price' => 'setLowestPrice',
'categories' => 'setCategories',
'performances' => 'setPerformances'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'event_key' => 'getEventKey',
'event_name' => 'getEventName',
'performer' => 'getPerformer',
'tagline' => 'getTagline',
'description' => 'getDescription',
'image_urls' => 'getImageUrls',
'image_tags' => 'getImageTags',
'movie_link' => 'getMovieLink',
'blocked_countries_for_sales' => 'getBlockedCountriesForSales',
'rating' => 'getRating',
'review_count' => 'getReviewCount',
'currency' => 'getCurrency',
'currency_code' => 'getCurrencyCode',
'lowest_price' => 'getLowestPrice',
'categories' => 'getCategories',
'performances' => 'getPerformances'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['event_key'] = isset($data['event_key']) ? $data['event_key'] : null;
        $this->container['event_name'] = isset($data['event_name']) ? $data['event_name'] : null;
        $this->container['performer'] = isset($data['performer']) ? $data['performer'] : null;
        $this->container['tagline'] = isset($data['tagline']) ? $data['tagline'] : null;
        $this->container['description'] = isset($data['description']) ? $data['description'] : null;
        $this->container['image_urls'] = isset($data['image_urls']) ? $data['image_urls'] : null;
        $this->container['image_tags'] = isset($data['image_tags']) ? $data['image_tags'] : null;
        $this->container['movie_link'] = isset($data['movie_link']) ? $data['movie_link'] : null;
        $this->container['blocked_countries_for_sales'] = isset($data['blocked_countries_for_sales']) ? $data['blocked_countries_for_sales'] : null;
        $this->container['rating'] = isset($data['rating']) ? $data['rating'] : null;
        $this->container['review_count'] = isset($data['review_count']) ? $data['review_count'] : null;
        $this->container['currency'] = isset($data['currency']) ? $data['currency'] : null;
        $this->container['currency_code'] = isset($data['currency_code']) ? $data['currency_code'] : null;
        $this->container['lowest_price'] = isset($data['lowest_price']) ? $data['lowest_price'] : null;
        $this->container['categories'] = isset($data['categories']) ? $data['categories'] : null;
        $this->container['performances'] = isset($data['performances']) ? $data['performances'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        return true;
    }


    /**
     * Gets event_key
     *
     * @return string
     */
    public function getEventKey()
    {
        return $this->container['event_key'];
    }

    /**
     * Sets event_key
     *
     * @param string $event_key The key of the event
     *
     * @return $this
     */
    public function setEventKey($event_key)
    {
        $this->container['event_key'] = $event_key;

        return $this;
    }

    /**
     * Gets event_name
     *
     * @return string
     */
    public function getEventName()
    {
        return $this->container['event_name'];
    }

    /**
     * Sets event_name
     *
     * @param string $event_name The name of the event
     *
     * @return $this
     */
    public function setEventName($event_name)
    {
        $this->container['event_name'] = $event_name;

        return $this;
    }

    /**
     * Gets performer
     *
     * @return string
     */
    public function getPerformer()
    {
        return $this->container['performer'];
    }

    /**
     * Sets performer
     *
     * @param string $performer The performer of the event (empty if same as name)
     *
     * @return $this
     */
    public function setPerformer($performer)
    {
        $this->container['performer'] = $performer;

        return $this;
    }

    /**
     * Gets tagline
     *
     * @return string
     */
    public function getTagline()
    {
        return $this->container['tagline'];
    }

    /**
     * Sets tagline
     *
     * @param string $tagline Specific title or words for promotional purposes
     *
     * @return $this
     */
    public function setTagline($tagline)
    {
        $this->container['tagline'] = $tagline;

        return $this;
    }

    /**
     * Gets description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->container['description'];
    }

    /**
     * Sets description
     *
     * @param string $description The description of the event (if requested)
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->container['description'] = $description;

        return $this;
    }

    /**
     * Gets image_urls
     *
     * @return string[]
     */
    public function getImageUrls()
    {
        return $this->container['image_urls'];
    }

    /**
     * Sets image_urls
     *
     * @param string[] $image_urls An array of url's for all the images of this event
     *
     * @return $this
     */
    public function setImageUrls($image_urls)
    {
        $this->container['image_urls'] = $image_urls;

        return $this;
    }

    /**
     * Gets image_tags
     *
     * @return string[]
     */
    public function getImageTags()
    {
        return $this->container['image_tags'];
    }

    /**
     * Sets image_tags
     *
     * @param string[] $image_tags An array of tags for all the images of this event (i.e. credits, photographer, etc.)
     *
     * @return $this
     */
    public function setImageTags($image_tags)
    {
        $this->container['image_tags'] = $image_tags;

        return $this;
    }

    /**
     * Gets movie_link
     *
     * @return string
     */
    public function getMovieLink()
    {
        return $this->container['movie_link'];
    }

    /**
     * Sets movie_link
     *
     * @param string $movie_link Code for YouTube movie
     *
     * @return $this
     */
    public function setMovieLink($movie_link)
    {
        $this->container['movie_link'] = $movie_link;

        return $this;
    }

    /**
     * Gets blocked_countries_for_sales
     *
     * @return string
     */
    public function getBlockedCountriesForSales()
    {
        return $this->container['blocked_countries_for_sales'];
    }

    /**
     * Sets blocked_countries_for_sales
     *
     * @param string $blocked_countries_for_sales The ISO codes of the countries where sales are NOT allowed
     *
     * @return $this
     */
    public function setBlockedCountriesForSales($blocked_countries_for_sales)
    {
        $this->container['blocked_countries_for_sales'] = $blocked_countries_for_sales;

        return $this;
    }

    /**
     * Gets rating
     *
     * @return double
     */
    public function getRating()
    {
        return $this->container['rating'];
    }

    /**
     * Sets rating
     *
     * @param double $rating The average rating of this event (empty if no ratings available yet)
     *
     * @return $this
     */
    public function setRating($rating)
    {
        $this->container['rating'] = $rating;

        return $this;
    }

    /**
     * Gets review_count
     *
     * @return int
     */
    public function getReviewCount()
    {
        return $this->container['review_count'];
    }

    /**
     * Sets review_count
     *
     * @param int $review_count The number of ratings/reviews for this event (empty if no ratings/reviews available yet)
     *
     * @return $this
     */
    public function setReviewCount($review_count)
    {
        $this->container['review_count'] = $review_count;

        return $this;
    }

    /**
     * Gets currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->container['currency'];
    }

    /**
     * Sets currency
     *
     * @param string $currency The currency symbol for the amount
     *
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->container['currency'] = $currency;

        return $this;
    }

    /**
     * Gets currency_code
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->container['currency_code'];
    }

    /**
     * Sets currency_code
     *
     * @param string $currency_code The currency code for the amount
     *
     * @return $this
     */
    public function setCurrencyCode($currency_code)
    {
        $this->container['currency_code'] = $currency_code;

        return $this;
    }

    /**
     * Gets lowest_price
     *
     * @return double
     */
    public function getLowestPrice()
    {
        return $this->container['lowest_price'];
    }

    /**
     * Sets lowest_price
     *
     * @param double $lowest_price The overall lowest price for a performance
     *
     * @return $this
     */
    public function setLowestPrice($lowest_price)
    {
        $this->container['lowest_price'] = $lowest_price;

        return $this;
    }

    /**
     * Gets categories
     *
     * @return \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSCategoryInfo[]
     */
    public function getCategories()
    {
        return $this->container['categories'];
    }

    /**
     * Sets categories
     *
     * @param \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSCategoryInfo[] $categories List of categories of this event (at least one)
     *
     * @return $this
     */
    public function setCategories($categories)
    {
        $this->container['categories'] = $categories;

        return $this;
    }

    /**
     * Gets performances
     *
     * @return \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPerformanceInfo[]
     */
    public function getPerformances()
    {
        return $this->container['performances'];
    }

    /**
     * Sets performances
     *
     * @param \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSPerformanceInfo[] $performances List of performances of this event
     *
     * @return $this
     */
    public function setPerformances($performances)
    {
        $this->container['performances'] = $performances;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
