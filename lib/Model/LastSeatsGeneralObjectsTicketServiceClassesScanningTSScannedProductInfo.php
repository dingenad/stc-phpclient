<?php
/**
 * LastSeatsGeneralObjectsTicketServiceClassesScanningTSScannedProductInfo
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * LastSeatsGeneralObjectsTicketServiceClassesScanningTSScannedProductInfo Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class LastSeatsGeneralObjectsTicketServiceClassesScanningTSScannedProductInfo implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'LastSeats.GeneralObjects.TicketServiceClasses.Scanning.TSScannedProductInfo';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'valid_from' => '\DateTime',
'valid_to' => '\DateTime',
'last_claim_date' => '\DateTime',
'currency_code' => 'string',
'price' => 'double',
'sales_channel_name' => 'string',
'scanning_display_message' => 'string',
'ticket_info' => '\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScannedTicketInfo',
'subscription_info' => '\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScannedSubscriptionInfo'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'valid_from' => 'date-time',
'valid_to' => 'date-time',
'last_claim_date' => 'date-time',
'currency_code' => null,
'price' => 'double',
'sales_channel_name' => null,
'scanning_display_message' => null,
'ticket_info' => null,
'subscription_info' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'valid_from' => 'ValidFrom',
'valid_to' => 'ValidTo',
'last_claim_date' => 'LastClaimDate',
'currency_code' => 'CurrencyCode',
'price' => 'Price',
'sales_channel_name' => 'SalesChannelName',
'scanning_display_message' => 'ScanningDisplayMessage',
'ticket_info' => 'TicketInfo',
'subscription_info' => 'SubscriptionInfo'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'valid_from' => 'setValidFrom',
'valid_to' => 'setValidTo',
'last_claim_date' => 'setLastClaimDate',
'currency_code' => 'setCurrencyCode',
'price' => 'setPrice',
'sales_channel_name' => 'setSalesChannelName',
'scanning_display_message' => 'setScanningDisplayMessage',
'ticket_info' => 'setTicketInfo',
'subscription_info' => 'setSubscriptionInfo'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'valid_from' => 'getValidFrom',
'valid_to' => 'getValidTo',
'last_claim_date' => 'getLastClaimDate',
'currency_code' => 'getCurrencyCode',
'price' => 'getPrice',
'sales_channel_name' => 'getSalesChannelName',
'scanning_display_message' => 'getScanningDisplayMessage',
'ticket_info' => 'getTicketInfo',
'subscription_info' => 'getSubscriptionInfo'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['valid_from'] = isset($data['valid_from']) ? $data['valid_from'] : null;
        $this->container['valid_to'] = isset($data['valid_to']) ? $data['valid_to'] : null;
        $this->container['last_claim_date'] = isset($data['last_claim_date']) ? $data['last_claim_date'] : null;
        $this->container['currency_code'] = isset($data['currency_code']) ? $data['currency_code'] : null;
        $this->container['price'] = isset($data['price']) ? $data['price'] : null;
        $this->container['sales_channel_name'] = isset($data['sales_channel_name']) ? $data['sales_channel_name'] : null;
        $this->container['scanning_display_message'] = isset($data['scanning_display_message']) ? $data['scanning_display_message'] : null;
        $this->container['ticket_info'] = isset($data['ticket_info']) ? $data['ticket_info'] : null;
        $this->container['subscription_info'] = isset($data['subscription_info']) ? $data['subscription_info'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        return true;
    }


    /**
     * Gets valid_from
     *
     * @return \DateTime
     */
    public function getValidFrom()
    {
        return $this->container['valid_from'];
    }

    /**
     * Sets valid_from
     *
     * @param \DateTime $valid_from valid_from
     *
     * @return $this
     */
    public function setValidFrom($valid_from)
    {
        $this->container['valid_from'] = $valid_from;

        return $this;
    }

    /**
     * Gets valid_to
     *
     * @return \DateTime
     */
    public function getValidTo()
    {
        return $this->container['valid_to'];
    }

    /**
     * Sets valid_to
     *
     * @param \DateTime $valid_to valid_to
     *
     * @return $this
     */
    public function setValidTo($valid_to)
    {
        $this->container['valid_to'] = $valid_to;

        return $this;
    }

    /**
     * Gets last_claim_date
     *
     * @return \DateTime
     */
    public function getLastClaimDate()
    {
        return $this->container['last_claim_date'];
    }

    /**
     * Sets last_claim_date
     *
     * @param \DateTime $last_claim_date last_claim_date
     *
     * @return $this
     */
    public function setLastClaimDate($last_claim_date)
    {
        $this->container['last_claim_date'] = $last_claim_date;

        return $this;
    }

    /**
     * Gets currency_code
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->container['currency_code'];
    }

    /**
     * Sets currency_code
     *
     * @param string $currency_code currency_code
     *
     * @return $this
     */
    public function setCurrencyCode($currency_code)
    {
        $this->container['currency_code'] = $currency_code;

        return $this;
    }

    /**
     * Gets price
     *
     * @return double
     */
    public function getPrice()
    {
        return $this->container['price'];
    }

    /**
     * Sets price
     *
     * @param double $price price
     *
     * @return $this
     */
    public function setPrice($price)
    {
        $this->container['price'] = $price;

        return $this;
    }

    /**
     * Gets sales_channel_name
     *
     * @return string
     */
    public function getSalesChannelName()
    {
        return $this->container['sales_channel_name'];
    }

    /**
     * Sets sales_channel_name
     *
     * @param string $sales_channel_name sales_channel_name
     *
     * @return $this
     */
    public function setSalesChannelName($sales_channel_name)
    {
        $this->container['sales_channel_name'] = $sales_channel_name;

        return $this;
    }

    /**
     * Gets scanning_display_message
     *
     * @return string
     */
    public function getScanningDisplayMessage()
    {
        return $this->container['scanning_display_message'];
    }

    /**
     * Sets scanning_display_message
     *
     * @param string $scanning_display_message scanning_display_message
     *
     * @return $this
     */
    public function setScanningDisplayMessage($scanning_display_message)
    {
        $this->container['scanning_display_message'] = $scanning_display_message;

        return $this;
    }

    /**
     * Gets ticket_info
     *
     * @return \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScannedTicketInfo
     */
    public function getTicketInfo()
    {
        return $this->container['ticket_info'];
    }

    /**
     * Sets ticket_info
     *
     * @param \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScannedTicketInfo $ticket_info ticket_info
     *
     * @return $this
     */
    public function setTicketInfo($ticket_info)
    {
        $this->container['ticket_info'] = $ticket_info;

        return $this;
    }

    /**
     * Gets subscription_info
     *
     * @return \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScannedSubscriptionInfo
     */
    public function getSubscriptionInfo()
    {
        return $this->container['subscription_info'];
    }

    /**
     * Sets subscription_info
     *
     * @param \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesScanningTSScannedSubscriptionInfo $subscription_info subscription_info
     *
     * @return $this
     */
    public function setSubscriptionInfo($subscription_info)
    {
        $this->container['subscription_info'] = $subscription_info;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
