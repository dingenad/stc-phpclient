<?php
/**
 * LastSeatsGeneralObjectsSubscriptionTSReservationSubscription
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * LastSeatsGeneralObjectsSubscriptionTSReservationSubscription Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class LastSeatsGeneralObjectsSubscriptionTSReservationSubscription implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'LastSeats.GeneralObjects.Subscription.TSReservationSubscription';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'reservation_key' => 'string',
'subscription_key' => 'string',
'subscription_template_key' => 'string',
'price' => 'double',
'original_price' => 'double'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'reservation_key' => 'uuid',
'subscription_key' => 'uuid',
'subscription_template_key' => 'uuid',
'price' => 'double',
'original_price' => 'double'    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'reservation_key' => 'ReservationKey',
'subscription_key' => 'SubscriptionKey',
'subscription_template_key' => 'SubscriptionTemplateKey',
'price' => 'Price',
'original_price' => 'OriginalPrice'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'reservation_key' => 'setReservationKey',
'subscription_key' => 'setSubscriptionKey',
'subscription_template_key' => 'setSubscriptionTemplateKey',
'price' => 'setPrice',
'original_price' => 'setOriginalPrice'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'reservation_key' => 'getReservationKey',
'subscription_key' => 'getSubscriptionKey',
'subscription_template_key' => 'getSubscriptionTemplateKey',
'price' => 'getPrice',
'original_price' => 'getOriginalPrice'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['reservation_key'] = isset($data['reservation_key']) ? $data['reservation_key'] : null;
        $this->container['subscription_key'] = isset($data['subscription_key']) ? $data['subscription_key'] : null;
        $this->container['subscription_template_key'] = isset($data['subscription_template_key']) ? $data['subscription_template_key'] : null;
        $this->container['price'] = isset($data['price']) ? $data['price'] : null;
        $this->container['original_price'] = isset($data['original_price']) ? $data['original_price'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        return true;
    }


    /**
     * Gets reservation_key
     *
     * @return string
     */
    public function getReservationKey()
    {
        return $this->container['reservation_key'];
    }

    /**
     * Sets reservation_key
     *
     * @param string $reservation_key reservation_key
     *
     * @return $this
     */
    public function setReservationKey($reservation_key)
    {
        $this->container['reservation_key'] = $reservation_key;

        return $this;
    }

    /**
     * Gets subscription_key
     *
     * @return string
     */
    public function getSubscriptionKey()
    {
        return $this->container['subscription_key'];
    }

    /**
     * Sets subscription_key
     *
     * @param string $subscription_key subscription_key
     *
     * @return $this
     */
    public function setSubscriptionKey($subscription_key)
    {
        $this->container['subscription_key'] = $subscription_key;

        return $this;
    }

    /**
     * Gets subscription_template_key
     *
     * @return string
     */
    public function getSubscriptionTemplateKey()
    {
        return $this->container['subscription_template_key'];
    }

    /**
     * Sets subscription_template_key
     *
     * @param string $subscription_template_key subscription_template_key
     *
     * @return $this
     */
    public function setSubscriptionTemplateKey($subscription_template_key)
    {
        $this->container['subscription_template_key'] = $subscription_template_key;

        return $this;
    }

    /**
     * Gets price
     *
     * @return double
     */
    public function getPrice()
    {
        return $this->container['price'];
    }

    /**
     * Sets price
     *
     * @param double $price price
     *
     * @return $this
     */
    public function setPrice($price)
    {
        $this->container['price'] = $price;

        return $this;
    }

    /**
     * Gets original_price
     *
     * @return double
     */
    public function getOriginalPrice()
    {
        return $this->container['original_price'];
    }

    /**
     * Sets original_price
     *
     * @param double $original_price original_price
     *
     * @return $this
     */
    public function setOriginalPrice($original_price)
    {
        $this->container['original_price'] = $original_price;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
