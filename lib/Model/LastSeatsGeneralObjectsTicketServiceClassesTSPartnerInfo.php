<?php
/**
 * LastSeatsGeneralObjectsTicketServiceClassesTSPartnerInfo
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * LastSeatsGeneralObjectsTicketServiceClassesTSPartnerInfo Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class LastSeatsGeneralObjectsTicketServiceClassesTSPartnerInfo implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'LastSeats.GeneralObjects.TicketServiceClasses.TSPartnerInfo';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'id' => 'int',
'partner_key' => 'string',
'name' => 'string',
'access_control' => 'string',
'access_control_setting1' => 'string',
'access_control_setting2' => 'string',
'access_control_setting3' => 'string',
'access_control_setting4' => 'string',
'access_control_expire_days' => 'int',
'send_profile_photo_to_ski_data' => 'bool'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'id' => 'int32',
'partner_key' => 'uuid',
'name' => null,
'access_control' => null,
'access_control_setting1' => null,
'access_control_setting2' => null,
'access_control_setting3' => null,
'access_control_setting4' => null,
'access_control_expire_days' => 'int32',
'send_profile_photo_to_ski_data' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'ID',
'partner_key' => 'PartnerKey',
'name' => 'Name',
'access_control' => 'AccessControl',
'access_control_setting1' => 'AccessControlSetting1',
'access_control_setting2' => 'AccessControlSetting2',
'access_control_setting3' => 'AccessControlSetting3',
'access_control_setting4' => 'AccessControlSetting4',
'access_control_expire_days' => 'AccessControlExpireDays',
'send_profile_photo_to_ski_data' => 'SendProfilePhotoToSkiData'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
'partner_key' => 'setPartnerKey',
'name' => 'setName',
'access_control' => 'setAccessControl',
'access_control_setting1' => 'setAccessControlSetting1',
'access_control_setting2' => 'setAccessControlSetting2',
'access_control_setting3' => 'setAccessControlSetting3',
'access_control_setting4' => 'setAccessControlSetting4',
'access_control_expire_days' => 'setAccessControlExpireDays',
'send_profile_photo_to_ski_data' => 'setSendProfilePhotoToSkiData'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
'partner_key' => 'getPartnerKey',
'name' => 'getName',
'access_control' => 'getAccessControl',
'access_control_setting1' => 'getAccessControlSetting1',
'access_control_setting2' => 'getAccessControlSetting2',
'access_control_setting3' => 'getAccessControlSetting3',
'access_control_setting4' => 'getAccessControlSetting4',
'access_control_expire_days' => 'getAccessControlExpireDays',
'send_profile_photo_to_ski_data' => 'getSendProfilePhotoToSkiData'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['partner_key'] = isset($data['partner_key']) ? $data['partner_key'] : null;
        $this->container['name'] = isset($data['name']) ? $data['name'] : null;
        $this->container['access_control'] = isset($data['access_control']) ? $data['access_control'] : null;
        $this->container['access_control_setting1'] = isset($data['access_control_setting1']) ? $data['access_control_setting1'] : null;
        $this->container['access_control_setting2'] = isset($data['access_control_setting2']) ? $data['access_control_setting2'] : null;
        $this->container['access_control_setting3'] = isset($data['access_control_setting3']) ? $data['access_control_setting3'] : null;
        $this->container['access_control_setting4'] = isset($data['access_control_setting4']) ? $data['access_control_setting4'] : null;
        $this->container['access_control_expire_days'] = isset($data['access_control_expire_days']) ? $data['access_control_expire_days'] : null;
        $this->container['send_profile_photo_to_ski_data'] = isset($data['send_profile_photo_to_ski_data']) ? $data['send_profile_photo_to_ski_data'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        return true;
    }


    /**
     * Gets id
     *
     * @return int
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int $id The Id of the partner
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets partner_key
     *
     * @return string
     */
    public function getPartnerKey()
    {
        return $this->container['partner_key'];
    }

    /**
     * Sets partner_key
     *
     * @param string $partner_key The unique key of the partner
     *
     * @return $this
     */
    public function setPartnerKey($partner_key)
    {
        $this->container['partner_key'] = $partner_key;

        return $this;
    }

    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     *
     * @param string $name The name of the partner
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->container['name'] = $name;

        return $this;
    }

    /**
     * Gets access_control
     *
     * @return string
     */
    public function getAccessControl()
    {
        return $this->container['access_control'];
    }

    /**
     * Sets access_control
     *
     * @param string $access_control One of the following values:<br /><code>NoAccessControl</code>,<br /><code>SkiData</code><br />  See also enumeration AccessControl
     *
     * @return $this
     */
    public function setAccessControl($access_control)
    {
        $this->container['access_control'] = $access_control;

        return $this;
    }

    /**
     * Gets access_control_setting1
     *
     * @return string
     */
    public function getAccessControlSetting1()
    {
        return $this->container['access_control_setting1'];
    }

    /**
     * Sets access_control_setting1
     *
     * @param string $access_control_setting1 Access control setting 1 of the partner
     *
     * @return $this
     */
    public function setAccessControlSetting1($access_control_setting1)
    {
        $this->container['access_control_setting1'] = $access_control_setting1;

        return $this;
    }

    /**
     * Gets access_control_setting2
     *
     * @return string
     */
    public function getAccessControlSetting2()
    {
        return $this->container['access_control_setting2'];
    }

    /**
     * Sets access_control_setting2
     *
     * @param string $access_control_setting2 Access control setting 2 of the partner
     *
     * @return $this
     */
    public function setAccessControlSetting2($access_control_setting2)
    {
        $this->container['access_control_setting2'] = $access_control_setting2;

        return $this;
    }

    /**
     * Gets access_control_setting3
     *
     * @return string
     */
    public function getAccessControlSetting3()
    {
        return $this->container['access_control_setting3'];
    }

    /**
     * Sets access_control_setting3
     *
     * @param string $access_control_setting3 Access control setting 3 of the partner
     *
     * @return $this
     */
    public function setAccessControlSetting3($access_control_setting3)
    {
        $this->container['access_control_setting3'] = $access_control_setting3;

        return $this;
    }

    /**
     * Gets access_control_setting4
     *
     * @return string
     */
    public function getAccessControlSetting4()
    {
        return $this->container['access_control_setting4'];
    }

    /**
     * Sets access_control_setting4
     *
     * @param string $access_control_setting4 Access control setting 4 of the partner
     *
     * @return $this
     */
    public function setAccessControlSetting4($access_control_setting4)
    {
        $this->container['access_control_setting4'] = $access_control_setting4;

        return $this;
    }

    /**
     * Gets access_control_expire_days
     *
     * @return int
     */
    public function getAccessControlExpireDays()
    {
        return $this->container['access_control_expire_days'];
    }

    /**
     * Sets access_control_expire_days
     *
     * @param int $access_control_expire_days The number of days before the access control expires
     *
     * @return $this
     */
    public function setAccessControlExpireDays($access_control_expire_days)
    {
        $this->container['access_control_expire_days'] = $access_control_expire_days;

        return $this;
    }

    /**
     * Gets send_profile_photo_to_ski_data
     *
     * @return bool
     */
    public function getSendProfilePhotoToSkiData()
    {
        return $this->container['send_profile_photo_to_ski_data'];
    }

    /**
     * Sets send_profile_photo_to_ski_data
     *
     * @param bool $send_profile_photo_to_ski_data Send photos to skidata
     *
     * @return $this
     */
    public function setSendProfilePhotoToSkiData($send_profile_photo_to_ski_data)
    {
        $this->container['send_profile_photo_to_ski_data'] = $send_profile_photo_to_ski_data;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
