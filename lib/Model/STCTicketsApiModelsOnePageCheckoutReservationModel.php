<?php
/**
 * STCTicketsApiModelsOnePageCheckoutReservationModel
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * STCTicketsApiModelsOnePageCheckoutReservationModel Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class STCTicketsApiModelsOnePageCheckoutReservationModel implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'STC.Tickets.ApiModels.OnePageCheckoutReservationModel';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'price_type_list' => '\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeInfo[]',
'contact_info' => '\Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo',
'discount_code' => 'string',
'affiliate' => 'string',
'external_reservation_number' => 'string',
'extra_info1' => 'string',
'extra_info2' => 'string',
'extra_info3' => 'string',
'ip_address' => 'string',
'capacity_date' => '\DateTime',
'capacity_time' => 'int',
'payment_brand' => 'string',
'payment_method_id' => 'int',
'amount_paid' => 'double',
'app_namespace' => 'string',
'result_url' => 'string',
'notify_url' => 'string',
'succes_url' => 'string',
'partner_key' => 'string'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'price_type_list' => null,
'contact_info' => null,
'discount_code' => null,
'affiliate' => null,
'external_reservation_number' => null,
'extra_info1' => null,
'extra_info2' => null,
'extra_info3' => null,
'ip_address' => null,
'capacity_date' => 'date-time',
'capacity_time' => 'int32',
'payment_brand' => null,
'payment_method_id' => 'int32',
'amount_paid' => 'double',
'app_namespace' => null,
'result_url' => null,
'notify_url' => null,
'succes_url' => null,
'partner_key' => 'uuid'    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'price_type_list' => 'PriceTypeList',
'contact_info' => 'ContactInfo',
'discount_code' => 'DiscountCode',
'affiliate' => 'Affiliate',
'external_reservation_number' => 'ExternalReservationNumber',
'extra_info1' => 'ExtraInfo1',
'extra_info2' => 'ExtraInfo2',
'extra_info3' => 'ExtraInfo3',
'ip_address' => 'IPAddress',
'capacity_date' => 'CapacityDate',
'capacity_time' => 'CapacityTime',
'payment_brand' => 'PaymentBrand',
'payment_method_id' => 'PaymentMethodId',
'amount_paid' => 'AmountPaid',
'app_namespace' => 'AppNamespace',
'result_url' => 'ResultUrl',
'notify_url' => 'NotifyUrl',
'succes_url' => 'SuccesUrl',
'partner_key' => 'PartnerKey'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'price_type_list' => 'setPriceTypeList',
'contact_info' => 'setContactInfo',
'discount_code' => 'setDiscountCode',
'affiliate' => 'setAffiliate',
'external_reservation_number' => 'setExternalReservationNumber',
'extra_info1' => 'setExtraInfo1',
'extra_info2' => 'setExtraInfo2',
'extra_info3' => 'setExtraInfo3',
'ip_address' => 'setIpAddress',
'capacity_date' => 'setCapacityDate',
'capacity_time' => 'setCapacityTime',
'payment_brand' => 'setPaymentBrand',
'payment_method_id' => 'setPaymentMethodId',
'amount_paid' => 'setAmountPaid',
'app_namespace' => 'setAppNamespace',
'result_url' => 'setResultUrl',
'notify_url' => 'setNotifyUrl',
'succes_url' => 'setSuccesUrl',
'partner_key' => 'setPartnerKey'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'price_type_list' => 'getPriceTypeList',
'contact_info' => 'getContactInfo',
'discount_code' => 'getDiscountCode',
'affiliate' => 'getAffiliate',
'external_reservation_number' => 'getExternalReservationNumber',
'extra_info1' => 'getExtraInfo1',
'extra_info2' => 'getExtraInfo2',
'extra_info3' => 'getExtraInfo3',
'ip_address' => 'getIpAddress',
'capacity_date' => 'getCapacityDate',
'capacity_time' => 'getCapacityTime',
'payment_brand' => 'getPaymentBrand',
'payment_method_id' => 'getPaymentMethodId',
'amount_paid' => 'getAmountPaid',
'app_namespace' => 'getAppNamespace',
'result_url' => 'getResultUrl',
'notify_url' => 'getNotifyUrl',
'succes_url' => 'getSuccesUrl',
'partner_key' => 'getPartnerKey'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['price_type_list'] = isset($data['price_type_list']) ? $data['price_type_list'] : null;
        $this->container['contact_info'] = isset($data['contact_info']) ? $data['contact_info'] : null;
        $this->container['discount_code'] = isset($data['discount_code']) ? $data['discount_code'] : null;
        $this->container['affiliate'] = isset($data['affiliate']) ? $data['affiliate'] : null;
        $this->container['external_reservation_number'] = isset($data['external_reservation_number']) ? $data['external_reservation_number'] : null;
        $this->container['extra_info1'] = isset($data['extra_info1']) ? $data['extra_info1'] : null;
        $this->container['extra_info2'] = isset($data['extra_info2']) ? $data['extra_info2'] : null;
        $this->container['extra_info3'] = isset($data['extra_info3']) ? $data['extra_info3'] : null;
        $this->container['ip_address'] = isset($data['ip_address']) ? $data['ip_address'] : null;
        $this->container['capacity_date'] = isset($data['capacity_date']) ? $data['capacity_date'] : null;
        $this->container['capacity_time'] = isset($data['capacity_time']) ? $data['capacity_time'] : null;
        $this->container['payment_brand'] = isset($data['payment_brand']) ? $data['payment_brand'] : null;
        $this->container['payment_method_id'] = isset($data['payment_method_id']) ? $data['payment_method_id'] : null;
        $this->container['amount_paid'] = isset($data['amount_paid']) ? $data['amount_paid'] : null;
        $this->container['app_namespace'] = isset($data['app_namespace']) ? $data['app_namespace'] : null;
        $this->container['result_url'] = isset($data['result_url']) ? $data['result_url'] : null;
        $this->container['notify_url'] = isset($data['notify_url']) ? $data['notify_url'] : null;
        $this->container['succes_url'] = isset($data['succes_url']) ? $data['succes_url'] : null;
        $this->container['partner_key'] = isset($data['partner_key']) ? $data['partner_key'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        return true;
    }


    /**
     * Gets price_type_list
     *
     * @return \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeInfo[]
     */
    public function getPriceTypeList()
    {
        return $this->container['price_type_list'];
    }

    /**
     * Sets price_type_list
     *
     * @param \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeInfo[] $price_type_list price_type_list
     *
     * @return $this
     */
    public function setPriceTypeList($price_type_list)
    {
        $this->container['price_type_list'] = $price_type_list;

        return $this;
    }

    /**
     * Gets contact_info
     *
     * @return \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo
     */
    public function getContactInfo()
    {
        return $this->container['contact_info'];
    }

    /**
     * Sets contact_info
     *
     * @param \Swagger\Client\Model\LastSeatsGeneralObjectsTicketServiceClassesTSContactInfo $contact_info contact_info
     *
     * @return $this
     */
    public function setContactInfo($contact_info)
    {
        $this->container['contact_info'] = $contact_info;

        return $this;
    }

    /**
     * Gets discount_code
     *
     * @return string
     */
    public function getDiscountCode()
    {
        return $this->container['discount_code'];
    }

    /**
     * Sets discount_code
     *
     * @param string $discount_code discount_code
     *
     * @return $this
     */
    public function setDiscountCode($discount_code)
    {
        $this->container['discount_code'] = $discount_code;

        return $this;
    }

    /**
     * Gets affiliate
     *
     * @return string
     */
    public function getAffiliate()
    {
        return $this->container['affiliate'];
    }

    /**
     * Sets affiliate
     *
     * @param string $affiliate affiliate
     *
     * @return $this
     */
    public function setAffiliate($affiliate)
    {
        $this->container['affiliate'] = $affiliate;

        return $this;
    }

    /**
     * Gets external_reservation_number
     *
     * @return string
     */
    public function getExternalReservationNumber()
    {
        return $this->container['external_reservation_number'];
    }

    /**
     * Sets external_reservation_number
     *
     * @param string $external_reservation_number external_reservation_number
     *
     * @return $this
     */
    public function setExternalReservationNumber($external_reservation_number)
    {
        $this->container['external_reservation_number'] = $external_reservation_number;

        return $this;
    }

    /**
     * Gets extra_info1
     *
     * @return string
     */
    public function getExtraInfo1()
    {
        return $this->container['extra_info1'];
    }

    /**
     * Sets extra_info1
     *
     * @param string $extra_info1 extra_info1
     *
     * @return $this
     */
    public function setExtraInfo1($extra_info1)
    {
        $this->container['extra_info1'] = $extra_info1;

        return $this;
    }

    /**
     * Gets extra_info2
     *
     * @return string
     */
    public function getExtraInfo2()
    {
        return $this->container['extra_info2'];
    }

    /**
     * Sets extra_info2
     *
     * @param string $extra_info2 extra_info2
     *
     * @return $this
     */
    public function setExtraInfo2($extra_info2)
    {
        $this->container['extra_info2'] = $extra_info2;

        return $this;
    }

    /**
     * Gets extra_info3
     *
     * @return string
     */
    public function getExtraInfo3()
    {
        return $this->container['extra_info3'];
    }

    /**
     * Sets extra_info3
     *
     * @param string $extra_info3 extra_info3
     *
     * @return $this
     */
    public function setExtraInfo3($extra_info3)
    {
        $this->container['extra_info3'] = $extra_info3;

        return $this;
    }

    /**
     * Gets ip_address
     *
     * @return string
     */
    public function getIpAddress()
    {
        return $this->container['ip_address'];
    }

    /**
     * Sets ip_address
     *
     * @param string $ip_address ip_address
     *
     * @return $this
     */
    public function setIpAddress($ip_address)
    {
        $this->container['ip_address'] = $ip_address;

        return $this;
    }

    /**
     * Gets capacity_date
     *
     * @return \DateTime
     */
    public function getCapacityDate()
    {
        return $this->container['capacity_date'];
    }

    /**
     * Sets capacity_date
     *
     * @param \DateTime $capacity_date capacity_date
     *
     * @return $this
     */
    public function setCapacityDate($capacity_date)
    {
        $this->container['capacity_date'] = $capacity_date;

        return $this;
    }

    /**
     * Gets capacity_time
     *
     * @return int
     */
    public function getCapacityTime()
    {
        return $this->container['capacity_time'];
    }

    /**
     * Sets capacity_time
     *
     * @param int $capacity_time capacity_time
     *
     * @return $this
     */
    public function setCapacityTime($capacity_time)
    {
        $this->container['capacity_time'] = $capacity_time;

        return $this;
    }

    /**
     * Gets payment_brand
     *
     * @return string
     */
    public function getPaymentBrand()
    {
        return $this->container['payment_brand'];
    }

    /**
     * Sets payment_brand
     *
     * @param string $payment_brand payment_brand
     *
     * @return $this
     */
    public function setPaymentBrand($payment_brand)
    {
        $this->container['payment_brand'] = $payment_brand;

        return $this;
    }

    /**
     * Gets payment_method_id
     *
     * @return int
     */
    public function getPaymentMethodId()
    {
        return $this->container['payment_method_id'];
    }

    /**
     * Sets payment_method_id
     *
     * @param int $payment_method_id payment_method_id
     *
     * @return $this
     */
    public function setPaymentMethodId($payment_method_id)
    {
        $this->container['payment_method_id'] = $payment_method_id;

        return $this;
    }

    /**
     * Gets amount_paid
     *
     * @return double
     */
    public function getAmountPaid()
    {
        return $this->container['amount_paid'];
    }

    /**
     * Sets amount_paid
     *
     * @param double $amount_paid amount_paid
     *
     * @return $this
     */
    public function setAmountPaid($amount_paid)
    {
        $this->container['amount_paid'] = $amount_paid;

        return $this;
    }

    /**
     * Gets app_namespace
     *
     * @return string
     */
    public function getAppNamespace()
    {
        return $this->container['app_namespace'];
    }

    /**
     * Sets app_namespace
     *
     * @param string $app_namespace app_namespace
     *
     * @return $this
     */
    public function setAppNamespace($app_namespace)
    {
        $this->container['app_namespace'] = $app_namespace;

        return $this;
    }

    /**
     * Gets result_url
     *
     * @return string
     */
    public function getResultUrl()
    {
        return $this->container['result_url'];
    }

    /**
     * Sets result_url
     *
     * @param string $result_url result_url
     *
     * @return $this
     */
    public function setResultUrl($result_url)
    {
        $this->container['result_url'] = $result_url;

        return $this;
    }

    /**
     * Gets notify_url
     *
     * @return string
     */
    public function getNotifyUrl()
    {
        return $this->container['notify_url'];
    }

    /**
     * Sets notify_url
     *
     * @param string $notify_url notify_url
     *
     * @return $this
     */
    public function setNotifyUrl($notify_url)
    {
        $this->container['notify_url'] = $notify_url;

        return $this;
    }

    /**
     * Gets succes_url
     *
     * @return string
     */
    public function getSuccesUrl()
    {
        return $this->container['succes_url'];
    }

    /**
     * Sets succes_url
     *
     * @param string $succes_url succes_url
     *
     * @return $this
     */
    public function setSuccesUrl($succes_url)
    {
        $this->container['succes_url'] = $succes_url;

        return $this;
    }

    /**
     * Gets partner_key
     *
     * @return string
     */
    public function getPartnerKey()
    {
        return $this->container['partner_key'];
    }

    /**
     * Sets partner_key
     *
     * @param string $partner_key partner_key
     *
     * @return $this
     */
    public function setPartnerKey($partner_key)
    {
        $this->container['partner_key'] = $partner_key;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
