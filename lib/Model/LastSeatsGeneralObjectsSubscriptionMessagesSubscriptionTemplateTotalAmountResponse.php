<?php
/**
 * LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateTotalAmountResponse
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateTotalAmountResponse Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class LastSeatsGeneralObjectsSubscriptionMessagesSubscriptionTemplateTotalAmountResponse implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'LastSeats.GeneralObjects.Subscription.Messages.SubscriptionTemplateTotalAmountResponse';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'subscription_template_id' => 'int',
'subscription_total_amount' => 'double',
'holder_form_flags' => '\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDtosFormFlagsSubscriptionHolder',
'dynamic_fields' => '\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionSubscriptionTemplateDynamicField[]',
'subscription_templates' => '\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionSubscriptionTemplate[]',
'discount_pricing' => '\Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDtosDiscountPricingDto[]',
'reservation_costs_amount' => 'double'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'subscription_template_id' => 'int32',
'subscription_total_amount' => 'double',
'holder_form_flags' => null,
'dynamic_fields' => null,
'subscription_templates' => null,
'discount_pricing' => null,
'reservation_costs_amount' => 'double'    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'subscription_template_id' => 'SubscriptionTemplateId',
'subscription_total_amount' => 'SubscriptionTotalAmount',
'holder_form_flags' => 'HolderFormFlags',
'dynamic_fields' => 'DynamicFields',
'subscription_templates' => 'SubscriptionTemplates',
'discount_pricing' => 'DiscountPricing',
'reservation_costs_amount' => 'ReservationCostsAmount'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'subscription_template_id' => 'setSubscriptionTemplateId',
'subscription_total_amount' => 'setSubscriptionTotalAmount',
'holder_form_flags' => 'setHolderFormFlags',
'dynamic_fields' => 'setDynamicFields',
'subscription_templates' => 'setSubscriptionTemplates',
'discount_pricing' => 'setDiscountPricing',
'reservation_costs_amount' => 'setReservationCostsAmount'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'subscription_template_id' => 'getSubscriptionTemplateId',
'subscription_total_amount' => 'getSubscriptionTotalAmount',
'holder_form_flags' => 'getHolderFormFlags',
'dynamic_fields' => 'getDynamicFields',
'subscription_templates' => 'getSubscriptionTemplates',
'discount_pricing' => 'getDiscountPricing',
'reservation_costs_amount' => 'getReservationCostsAmount'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['subscription_template_id'] = isset($data['subscription_template_id']) ? $data['subscription_template_id'] : null;
        $this->container['subscription_total_amount'] = isset($data['subscription_total_amount']) ? $data['subscription_total_amount'] : null;
        $this->container['holder_form_flags'] = isset($data['holder_form_flags']) ? $data['holder_form_flags'] : null;
        $this->container['dynamic_fields'] = isset($data['dynamic_fields']) ? $data['dynamic_fields'] : null;
        $this->container['subscription_templates'] = isset($data['subscription_templates']) ? $data['subscription_templates'] : null;
        $this->container['discount_pricing'] = isset($data['discount_pricing']) ? $data['discount_pricing'] : null;
        $this->container['reservation_costs_amount'] = isset($data['reservation_costs_amount']) ? $data['reservation_costs_amount'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        return true;
    }


    /**
     * Gets subscription_template_id
     *
     * @return int
     */
    public function getSubscriptionTemplateId()
    {
        return $this->container['subscription_template_id'];
    }

    /**
     * Sets subscription_template_id
     *
     * @param int $subscription_template_id The Id of the requested subscription template.
     *
     * @return $this
     */
    public function setSubscriptionTemplateId($subscription_template_id)
    {
        $this->container['subscription_template_id'] = $subscription_template_id;

        return $this;
    }

    /**
     * Gets subscription_total_amount
     *
     * @return double
     */
    public function getSubscriptionTotalAmount()
    {
        return $this->container['subscription_total_amount'];
    }

    /**
     * Sets subscription_total_amount
     *
     * @param double $subscription_total_amount The total amount of all subscriptions with this template in the current currency.
     *
     * @return $this
     */
    public function setSubscriptionTotalAmount($subscription_total_amount)
    {
        $this->container['subscription_total_amount'] = $subscription_total_amount;

        return $this;
    }

    /**
     * Gets holder_form_flags
     *
     * @return \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDtosFormFlagsSubscriptionHolder
     */
    public function getHolderFormFlags()
    {
        return $this->container['holder_form_flags'];
    }

    /**
     * Sets holder_form_flags
     *
     * @param \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDtosFormFlagsSubscriptionHolder $holder_form_flags holder_form_flags
     *
     * @return $this
     */
    public function setHolderFormFlags($holder_form_flags)
    {
        $this->container['holder_form_flags'] = $holder_form_flags;

        return $this;
    }

    /**
     * Gets dynamic_fields
     *
     * @return \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionSubscriptionTemplateDynamicField[]
     */
    public function getDynamicFields()
    {
        return $this->container['dynamic_fields'];
    }

    /**
     * Sets dynamic_fields
     *
     * @param \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionSubscriptionTemplateDynamicField[] $dynamic_fields The merged dynamic fields of the chosen subscription templates
     *
     * @return $this
     */
    public function setDynamicFields($dynamic_fields)
    {
        $this->container['dynamic_fields'] = $dynamic_fields;

        return $this;
    }

    /**
     * Gets subscription_templates
     *
     * @return \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionSubscriptionTemplate[]
     */
    public function getSubscriptionTemplates()
    {
        return $this->container['subscription_templates'];
    }

    /**
     * Sets subscription_templates
     *
     * @param \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionSubscriptionTemplate[] $subscription_templates The new list of subscription templates
     *
     * @return $this
     */
    public function setSubscriptionTemplates($subscription_templates)
    {
        $this->container['subscription_templates'] = $subscription_templates;

        return $this;
    }

    /**
     * Gets discount_pricing
     *
     * @return \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDtosDiscountPricingDto[]
     */
    public function getDiscountPricing()
    {
        return $this->container['discount_pricing'];
    }

    /**
     * Sets discount_pricing
     *
     * @param \Swagger\Client\Model\LastSeatsGeneralObjectsSubscriptionDtosDiscountPricingDto[] $discount_pricing The list of discounts and amounts.
     *
     * @return $this
     */
    public function setDiscountPricing($discount_pricing)
    {
        $this->container['discount_pricing'] = $discount_pricing;

        return $this;
    }

    /**
     * Gets reservation_costs_amount
     *
     * @return double
     */
    public function getReservationCostsAmount()
    {
        return $this->container['reservation_costs_amount'];
    }

    /**
     * Sets reservation_costs_amount
     *
     * @param double $reservation_costs_amount The reservation costs in the currency of the subscription template.
     *
     * @return $this
     */
    public function setReservationCostsAmount($reservation_costs_amount)
    {
        $this->container['reservation_costs_amount'] = $reservation_costs_amount;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
