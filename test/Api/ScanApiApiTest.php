<?php
/**
 * ScanApiApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Swagger\Client;

use Swagger\Client\Configuration;
use Swagger\Client\ApiException;
use Swagger\Client\ObjectSerializer;

/**
 * ScanApiApiTest Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ScanApiApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for scanApiClaimTicketCode
     *
     * Claim ticket code.
     *
     */
    public function testScanApiClaimTicketCode()
    {
    }

    /**
     * Test case for scanApiClaimTicketCode_0
     *
     * Check ticket code.
     *
     */
    public function testScanApiClaimTicketCode0()
    {
    }

    /**
     * Test case for scanApiGetPartnerScanAmounts
     *
     * Get today's scan amounts for partner (across all sources).
     *
     */
    public function testScanApiGetPartnerScanAmounts()
    {
    }

    /**
     * Test case for scanApiGetScanConfiguration
     *
     * Connect scanner to configuration and get configuration details.
     *
     */
    public function testScanApiGetScanConfiguration()
    {
    }

    /**
     * Test case for scanApiGetTicketCodeScanInfo
     *
     * Retrieve ticket code data for local validation.
     *
     */
    public function testScanApiGetTicketCodeScanInfo()
    {
    }

    /**
     * Test case for scanApiInsertTicketScan
     *
     * Inform system about locally validated ticket scan.
     *
     */
    public function testScanApiInsertTicketScan()
    {
    }

    /**
     * Test case for scanApiScanDeviceCheckTicketCode
     *
     * Check ticket code using scan device.
     *
     */
    public function testScanApiScanDeviceCheckTicketCode()
    {
    }

    /**
     * Test case for scanApiScanDeviceClaimTicketCode
     *
     * Claim ticket code using scan device.
     *
     */
    public function testScanApiScanDeviceClaimTicketCode()
    {
    }

    /**
     * Test case for scanApiUpdateScanDeviceInfo
     *
     * Send scan device information.
     *
     */
    public function testScanApiUpdateScanDeviceInfo()
    {
    }
}
