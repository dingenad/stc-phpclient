<?php
/**
 * SubscriptionApiApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Swagger\Client;

use Swagger\Client\Configuration;
use Swagger\Client\ApiException;
use Swagger\Client\ObjectSerializer;

/**
 * SubscriptionApiApiTest Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class SubscriptionApiApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for subscriptionApiCreateSubscription
     *
     * Create a subscription registration..
     *
     */
    public function testSubscriptionApiCreateSubscription()
    {
    }

    /**
     * Test case for subscriptionApiForgotPassword
     *
     * Send a forgot password request..
     *
     */
    public function testSubscriptionApiForgotPassword()
    {
    }

    /**
     * Test case for subscriptionApiGetCountries
     *
     * .
     *
     */
    public function testSubscriptionApiGetCountries()
    {
    }

    /**
     * Test case for subscriptionApiGetExistingEmailAddress
     *
     * Check if a e-mail address already exists..
     *
     */
    public function testSubscriptionApiGetExistingEmailAddress()
    {
    }

    /**
     * Test case for subscriptionApiGetPartnerAddressInfo
     *
     * .
     *
     */
    public function testSubscriptionApiGetPartnerAddressInfo()
    {
    }

    /**
     * Test case for subscriptionApiGetPaymentMethods
     *
     * Get the payment methods..
     *
     */
    public function testSubscriptionApiGetPaymentMethods()
    {
    }

    /**
     * Test case for subscriptionApiGetRealValidFrom
     *
     * Get the valid from date of a subscription template. This date indicates when a subscription can be used..
     *
     */
    public function testSubscriptionApiGetRealValidFrom()
    {
    }

    /**
     * Test case for subscriptionApiGetSubscriptionAmount
     *
     * Get the total amount of the selected template and aggregated fields the user needs to fill in..
     *
     */
    public function testSubscriptionApiGetSubscriptionAmount()
    {
    }

    /**
     * Test case for subscriptionApiGetSubscriptionTemplates
     *
     * Get the available subscription templates.
     *
     */
    public function testSubscriptionApiGetSubscriptionTemplates()
    {
    }

    /**
     * Test case for subscriptionApiLogin
     *
     * Login a user..
     *
     */
    public function testSubscriptionApiLogin()
    {
    }
}
