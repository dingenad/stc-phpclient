<?php
/**
 * LastSeatsGeneralObjectsTicketServiceClassesTSResellerProductInfoTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * LastSeatsGeneralObjectsTicketServiceClassesTSResellerProductInfoTest Class Doc Comment
 *
 * @category    Class
 * @description This type has all the information about a specific capacity slot.
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class LastSeatsGeneralObjectsTicketServiceClassesTSResellerProductInfoTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "LastSeatsGeneralObjectsTicketServiceClassesTSResellerProductInfo"
     */
    public function testLastSeatsGeneralObjectsTicketServiceClassesTSResellerProductInfo()
    {
    }

    /**
     * Test attribute "reseller_product_key"
     */
    public function testPropertyResellerProductKey()
    {
    }

    /**
     * Test attribute "price_type_name"
     */
    public function testPropertyPriceTypeName()
    {
    }

    /**
     * Test attribute "buying_price"
     */
    public function testPropertyBuyingPrice()
    {
    }

    /**
     * Test attribute "selling_price"
     */
    public function testPropertySellingPrice()
    {
    }

    /**
     * Test attribute "event_name"
     */
    public function testPropertyEventName()
    {
    }

    /**
     * Test attribute "performance_name"
     */
    public function testPropertyPerformanceName()
    {
    }

    /**
     * Test attribute "performance_section_key"
     */
    public function testPropertyPerformanceSectionKey()
    {
    }

    /**
     * Test attribute "performance_section_name"
     */
    public function testPropertyPerformanceSectionName()
    {
    }

    /**
     * Test attribute "reseller_volume_discount_info_list"
     */
    public function testPropertyResellerVolumeDiscountInfoList()
    {
    }

    /**
     * Test attribute "available_seats"
     */
    public function testPropertyAvailableSeats()
    {
    }

    /**
     * Test attribute "available_seats_in_section"
     */
    public function testPropertyAvailableSeatsInSection()
    {
    }

    /**
     * Test attribute "ticket_valid_from"
     */
    public function testPropertyTicketValidFrom()
    {
    }

    /**
     * Test attribute "ticket_valid_to"
     */
    public function testPropertyTicketValidTo()
    {
    }

    /**
     * Test attribute "minimum_amount"
     */
    public function testPropertyMinimumAmount()
    {
    }

    /**
     * Test attribute "maximum_amount"
     */
    public function testPropertyMaximumAmount()
    {
    }

    /**
     * Test attribute "step"
     */
    public function testPropertyStep()
    {
    }

    /**
     * Test attribute "requires_capacity_slot"
     */
    public function testPropertyRequiresCapacitySlot()
    {
    }

    /**
     * Test attribute "sales_channel_performance_section_price_type_key"
     */
    public function testPropertySalesChannelPerformanceSectionPriceTypeKey()
    {
    }
}
