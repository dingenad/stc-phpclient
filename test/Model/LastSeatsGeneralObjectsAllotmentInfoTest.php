<?php
/**
 * LastSeatsGeneralObjectsAllotmentInfoTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * LastSeatsGeneralObjectsAllotmentInfoTest Class Doc Comment
 *
 * @category    Class
 * @description LastSeatsGeneralObjectsAllotmentInfo
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class LastSeatsGeneralObjectsAllotmentInfoTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "LastSeatsGeneralObjectsAllotmentInfo"
     */
    public function testLastSeatsGeneralObjectsAllotmentInfo()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "performance_section_id"
     */
    public function testPropertyPerformanceSectionId()
    {
    }

    /**
     * Test attribute "allotment_nr"
     */
    public function testPropertyAllotmentNr()
    {
    }

    /**
     * Test attribute "row_number"
     */
    public function testPropertyRowNumber()
    {
    }

    /**
     * Test attribute "seat_number"
     */
    public function testPropertySeatNumber()
    {
    }

    /**
     * Test attribute "ticket_code"
     */
    public function testPropertyTicketCode()
    {
    }

    /**
     * Test attribute "blocked"
     */
    public function testPropertyBlocked()
    {
    }

    /**
     * Test attribute "reserved"
     */
    public function testPropertyReserved()
    {
    }

    /**
     * Test attribute "reservation_id"
     */
    public function testPropertyReservationId()
    {
    }

    /**
     * Test attribute "max_claim_amount"
     */
    public function testPropertyMaxClaimAmount()
    {
    }

    /**
     * Test attribute "cached_claim_amount"
     */
    public function testPropertyCachedClaimAmount()
    {
    }

    /**
     * Test attribute "last_claim_date"
     */
    public function testPropertyLastClaimDate()
    {
    }

    /**
     * Test attribute "reservation_number"
     */
    public function testPropertyReservationNumber()
    {
    }

    /**
     * Test attribute "first_name"
     */
    public function testPropertyFirstName()
    {
    }

    /**
     * Test attribute "middle"
     */
    public function testPropertyMiddle()
    {
    }

    /**
     * Test attribute "last_name"
     */
    public function testPropertyLastName()
    {
    }

    /**
     * Test attribute "mail_address"
     */
    public function testPropertyMailAddress()
    {
    }

    /**
     * Test attribute "phone_number"
     */
    public function testPropertyPhoneNumber()
    {
    }

    /**
     * Test attribute "mobile_number"
     */
    public function testPropertyMobileNumber()
    {
    }
}
