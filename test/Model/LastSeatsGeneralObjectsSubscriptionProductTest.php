<?php
/**
 * LastSeatsGeneralObjectsSubscriptionProductTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * LastSeatsGeneralObjectsSubscriptionProductTest Class Doc Comment
 *
 * @category    Class
 * @description A generic product used for a subscription template product
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class LastSeatsGeneralObjectsSubscriptionProductTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "LastSeatsGeneralObjectsSubscriptionProduct"
     */
    public function testLastSeatsGeneralObjectsSubscriptionProduct()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "product_key"
     */
    public function testPropertyProductKey()
    {
    }

    /**
     * Test attribute "partner_id"
     */
    public function testPropertyPartnerId()
    {
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
    }

    /**
     * Test attribute "plural_name"
     */
    public function testPropertyPluralName()
    {
    }

    /**
     * Test attribute "create_contact"
     */
    public function testPropertyCreateContact()
    {
    }

    /**
     * Test attribute "cash_register_product"
     */
    public function testPropertyCashRegisterProduct()
    {
    }

    /**
     * Test attribute "translations"
     */
    public function testPropertyTranslations()
    {
    }

    /**
     * Test attribute "dynamic_fields"
     */
    public function testPropertyDynamicFields()
    {
    }

    /**
     * Test attribute "description"
     */
    public function testPropertyDescription()
    {
    }

    /**
     * Test attribute "subscription_layout_id"
     */
    public function testPropertySubscriptionLayoutId()
    {
    }

    /**
     * Test attribute "external_id"
     */
    public function testPropertyExternalId()
    {
    }

    /**
     * Test attribute "area_code"
     */
    public function testPropertyAreaCode()
    {
    }

    /**
     * Test attribute "ticket_code_batch_id"
     */
    public function testPropertyTicketCodeBatchId()
    {
    }

    /**
     * Test attribute "minimum_age"
     */
    public function testPropertyMinimumAge()
    {
    }

    /**
     * Test attribute "maximum_age"
     */
    public function testPropertyMaximumAge()
    {
    }

    /**
     * Test attribute "internal_id"
     */
    public function testPropertyInternalId()
    {
    }

    /**
     * Test attribute "max_claim_amount"
     */
    public function testPropertyMaxClaimAmount()
    {
    }

    /**
     * Test attribute "max_claim_amount_period"
     */
    public function testPropertyMaxClaimAmountPeriod()
    {
    }

    /**
     * Test attribute "max_claim_amount_period_duration"
     */
    public function testPropertyMaxClaimAmountPeriodDuration()
    {
    }

    /**
     * Test attribute "max_claim_amount_period_duration_type"
     */
    public function testPropertyMaxClaimAmountPeriodDurationType()
    {
    }

    /**
     * Test attribute "claim_interval"
     */
    public function testPropertyClaimInterval()
    {
    }

    /**
     * Test attribute "claim_interval_type"
     */
    public function testPropertyClaimIntervalType()
    {
    }
}
