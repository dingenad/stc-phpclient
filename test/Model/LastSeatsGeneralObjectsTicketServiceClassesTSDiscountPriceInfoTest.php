<?php
/**
 * LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPriceInfoTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPriceInfoTest Class Doc Comment
 *
 * @category    Class
 * @description LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPriceInfo
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPriceInfoTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "LastSeatsGeneralObjectsTicketServiceClassesTSDiscountPriceInfo"
     */
    public function testLastSeatsGeneralObjectsTicketServiceClassesTSDiscountPriceInfo()
    {
    }

    /**
     * Test attribute "price_key"
     */
    public function testPropertyPriceKey()
    {
    }

    /**
     * Test attribute "discount_price"
     */
    public function testPropertyDiscountPrice()
    {
    }
}
