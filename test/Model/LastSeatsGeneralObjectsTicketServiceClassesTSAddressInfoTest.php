<?php
/**
 * LastSeatsGeneralObjectsTicketServiceClassesTSAddressInfoTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * LastSeatsGeneralObjectsTicketServiceClassesTSAddressInfoTest Class Doc Comment
 *
 * @category    Class
 * @description This type has info about a specific address.
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class LastSeatsGeneralObjectsTicketServiceClassesTSAddressInfoTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "LastSeatsGeneralObjectsTicketServiceClassesTSAddressInfo"
     */
    public function testLastSeatsGeneralObjectsTicketServiceClassesTSAddressInfo()
    {
    }

    /**
     * Test attribute "street"
     */
    public function testPropertyStreet()
    {
    }

    /**
     * Test attribute "number"
     */
    public function testPropertyNumber()
    {
    }

    /**
     * Test attribute "extra_address_line"
     */
    public function testPropertyExtraAddressLine()
    {
    }

    /**
     * Test attribute "postal_code"
     */
    public function testPropertyPostalCode()
    {
    }

    /**
     * Test attribute "city_name"
     */
    public function testPropertyCityName()
    {
    }

    /**
     * Test attribute "state_name"
     */
    public function testPropertyStateName()
    {
    }

    /**
     * Test attribute "country_name"
     */
    public function testPropertyCountryName()
    {
    }

    /**
     * Test attribute "country_id"
     */
    public function testPropertyCountryId()
    {
    }

    /**
     * Test attribute "lat"
     */
    public function testPropertyLat()
    {
    }

    /**
     * Test attribute "lon"
     */
    public function testPropertyLon()
    {
    }
}
