<?php
/**
 * LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeRequestInfoTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeRequestInfoTest Class Doc Comment
 *
 * @category    Class
 * @description This type has all the information about a specific price type within a request to create or update a reservation
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeRequestInfoTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "LastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeRequestInfo"
     */
    public function testLastSeatsGeneralObjectsTicketServiceClassesTSReservationPriceTypeRequestInfo()
    {
    }

    /**
     * Test attribute "price_key"
     */
    public function testPropertyPriceKey()
    {
    }

    /**
     * Test attribute "nr_of_seats"
     */
    public function testPropertyNrOfSeats()
    {
    }

    /**
     * Test attribute "pass_number"
     */
    public function testPropertyPassNumber()
    {
    }

    /**
     * Test attribute "capacity_date"
     */
    public function testPropertyCapacityDate()
    {
    }

    /**
     * Test attribute "start_time_minutes_after_midnight"
     */
    public function testPropertyStartTimeMinutesAfterMidnight()
    {
    }

    /**
     * Test attribute "custom_price"
     */
    public function testPropertyCustomPrice()
    {
    }

    /**
     * Test attribute "custom_valid_from"
     */
    public function testPropertyCustomValidFrom()
    {
    }

    /**
     * Test attribute "custom_valid_to"
     */
    public function testPropertyCustomValidTo()
    {
    }
}
