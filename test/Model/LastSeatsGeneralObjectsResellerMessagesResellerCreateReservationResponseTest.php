<?php
/**
 * LastSeatsGeneralObjectsResellerMessagesResellerCreateReservationResponseTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * LastSeatsGeneralObjectsResellerMessagesResellerCreateReservationResponseTest Class Doc Comment
 *
 * @category    Class
 * @description LastSeatsGeneralObjectsResellerMessagesResellerCreateReservationResponse
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class LastSeatsGeneralObjectsResellerMessagesResellerCreateReservationResponseTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "LastSeatsGeneralObjectsResellerMessagesResellerCreateReservationResponse"
     */
    public function testLastSeatsGeneralObjectsResellerMessagesResellerCreateReservationResponse()
    {
    }

    /**
     * Test attribute "redirect_url"
     */
    public function testPropertyRedirectUrl()
    {
    }

    /**
     * Test attribute "errors"
     */
    public function testPropertyErrors()
    {
    }

    /**
     * Test attribute "success"
     */
    public function testPropertySuccess()
    {
    }

    /**
     * Test attribute "registration_id"
     */
    public function testPropertyRegistrationId()
    {
    }

    /**
     * Test attribute "total_amount"
     */
    public function testPropertyTotalAmount()
    {
    }

    /**
     * Test attribute "payment_values"
     */
    public function testPropertyPaymentValues()
    {
    }

    /**
     * Test attribute "submit_url"
     */
    public function testPropertySubmitUrl()
    {
    }
}
