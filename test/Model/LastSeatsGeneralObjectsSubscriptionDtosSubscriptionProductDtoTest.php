<?php
/**
 * LastSeatsGeneralObjectsSubscriptionDtosSubscriptionProductDtoTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * LastSeatsGeneralObjectsSubscriptionDtosSubscriptionProductDtoTest Class Doc Comment
 *
 * @category    Class
 * @description A subscription product.
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class LastSeatsGeneralObjectsSubscriptionDtosSubscriptionProductDtoTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "LastSeatsGeneralObjectsSubscriptionDtosSubscriptionProductDto"
     */
    public function testLastSeatsGeneralObjectsSubscriptionDtosSubscriptionProductDto()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "product_id"
     */
    public function testPropertyProductId()
    {
    }

    /**
     * Test attribute "dynamic_field_values"
     */
    public function testPropertyDynamicFieldValues()
    {
    }

    /**
     * Test attribute "form_values"
     */
    public function testPropertyFormValues()
    {
    }

    /**
     * Test attribute "picture_url"
     */
    public function testPropertyPictureUrl()
    {
    }

    /**
     * Test attribute "picture_id"
     */
    public function testPropertyPictureId()
    {
    }

    /**
     * Test attribute "picture_crop_points"
     */
    public function testPropertyPictureCropPoints()
    {
    }

    /**
     * Test attribute "copied_id"
     */
    public function testPropertyCopiedId()
    {
    }
}
