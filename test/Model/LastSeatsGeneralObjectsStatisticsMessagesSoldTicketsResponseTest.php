<?php
/**
 * LastSeatsGeneralObjectsStatisticsMessagesSoldTicketsResponseTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * LastSeatsGeneralObjectsStatisticsMessagesSoldTicketsResponseTest Class Doc Comment
 *
 * @category    Class
 * @description LastSeatsGeneralObjectsStatisticsMessagesSoldTicketsResponse
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class LastSeatsGeneralObjectsStatisticsMessagesSoldTicketsResponseTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "LastSeatsGeneralObjectsStatisticsMessagesSoldTicketsResponse"
     */
    public function testLastSeatsGeneralObjectsStatisticsMessagesSoldTicketsResponse()
    {
    }

    /**
     * Test attribute "sold_tickets_info"
     */
    public function testPropertySoldTicketsInfo()
    {
    }

    /**
     * Test attribute "offset"
     */
    public function testPropertyOffset()
    {
    }

    /**
     * Test attribute "result_count"
     */
    public function testPropertyResultCount()
    {
    }

    /**
     * Test attribute "succeeded"
     */
    public function testPropertySucceeded()
    {
    }

    /**
     * Test attribute "error_message"
     */
    public function testPropertyErrorMessage()
    {
    }

    /**
     * Test attribute "is_redirect"
     */
    public function testPropertyIsRedirect()
    {
    }

    /**
     * Test attribute "redirect_url"
     */
    public function testPropertyRedirectUrl()
    {
    }

    /**
     * Test attribute "display_error"
     */
    public function testPropertyDisplayError()
    {
    }
}
