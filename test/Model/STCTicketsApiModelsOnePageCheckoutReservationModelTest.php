<?php
/**
 * STCTicketsApiModelsOnePageCheckoutReservationModelTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * STCTicketsApiModelsOnePageCheckoutReservationModelTest Class Doc Comment
 *
 * @category    Class
 * @description STCTicketsApiModelsOnePageCheckoutReservationModel
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class STCTicketsApiModelsOnePageCheckoutReservationModelTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "STCTicketsApiModelsOnePageCheckoutReservationModel"
     */
    public function testSTCTicketsApiModelsOnePageCheckoutReservationModel()
    {
    }

    /**
     * Test attribute "price_type_list"
     */
    public function testPropertyPriceTypeList()
    {
    }

    /**
     * Test attribute "contact_info"
     */
    public function testPropertyContactInfo()
    {
    }

    /**
     * Test attribute "discount_code"
     */
    public function testPropertyDiscountCode()
    {
    }

    /**
     * Test attribute "affiliate"
     */
    public function testPropertyAffiliate()
    {
    }

    /**
     * Test attribute "external_reservation_number"
     */
    public function testPropertyExternalReservationNumber()
    {
    }

    /**
     * Test attribute "extra_info1"
     */
    public function testPropertyExtraInfo1()
    {
    }

    /**
     * Test attribute "extra_info2"
     */
    public function testPropertyExtraInfo2()
    {
    }

    /**
     * Test attribute "extra_info3"
     */
    public function testPropertyExtraInfo3()
    {
    }

    /**
     * Test attribute "ip_address"
     */
    public function testPropertyIpAddress()
    {
    }

    /**
     * Test attribute "capacity_date"
     */
    public function testPropertyCapacityDate()
    {
    }

    /**
     * Test attribute "capacity_time"
     */
    public function testPropertyCapacityTime()
    {
    }

    /**
     * Test attribute "payment_brand"
     */
    public function testPropertyPaymentBrand()
    {
    }

    /**
     * Test attribute "payment_method_id"
     */
    public function testPropertyPaymentMethodId()
    {
    }

    /**
     * Test attribute "amount_paid"
     */
    public function testPropertyAmountPaid()
    {
    }

    /**
     * Test attribute "app_namespace"
     */
    public function testPropertyAppNamespace()
    {
    }

    /**
     * Test attribute "result_url"
     */
    public function testPropertyResultUrl()
    {
    }

    /**
     * Test attribute "notify_url"
     */
    public function testPropertyNotifyUrl()
    {
    }

    /**
     * Test attribute "succes_url"
     */
    public function testPropertySuccesUrl()
    {
    }

    /**
     * Test attribute "partner_key"
     */
    public function testPropertyPartnerKey()
    {
    }
}
