<?php
/**
 * LastSeatsGeneralObjectsPaymentPaymentCategoryTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * LastSeatsGeneralObjectsPaymentPaymentCategoryTest Class Doc Comment
 *
 * @category    Class
 * @description LastSeatsGeneralObjectsPaymentPaymentCategory
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class LastSeatsGeneralObjectsPaymentPaymentCategoryTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "LastSeatsGeneralObjectsPaymentPaymentCategory"
     */
    public function testLastSeatsGeneralObjectsPaymentPaymentCategory()
    {
    }

    /**
     * Test attribute "method_category"
     */
    public function testPropertyMethodCategory()
    {
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
    }

    /**
     * Test attribute "image"
     */
    public function testPropertyImage()
    {
    }

    /**
     * Test attribute "sort_order"
     */
    public function testPropertySortOrder()
    {
    }

    /**
     * Test attribute "methods"
     */
    public function testPropertyMethods()
    {
    }

    /**
     * Test attribute "budget"
     */
    public function testPropertyBudget()
    {
    }
}
