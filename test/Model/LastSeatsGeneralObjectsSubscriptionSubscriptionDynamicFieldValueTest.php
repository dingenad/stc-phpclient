<?php
/**
 * LastSeatsGeneralObjectsSubscriptionSubscriptionDynamicFieldValueTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * LastSeatsGeneralObjectsSubscriptionSubscriptionDynamicFieldValueTest Class Doc Comment
 *
 * @category    Class
 * @description A filled in sybscription dynamic field value.
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class LastSeatsGeneralObjectsSubscriptionSubscriptionDynamicFieldValueTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "LastSeatsGeneralObjectsSubscriptionSubscriptionDynamicFieldValue"
     */
    public function testLastSeatsGeneralObjectsSubscriptionSubscriptionDynamicFieldValue()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "subscription_id"
     */
    public function testPropertySubscriptionId()
    {
    }

    /**
     * Test attribute "subscription_dynamic_field_id"
     */
    public function testPropertySubscriptionDynamicFieldId()
    {
    }

    /**
     * Test attribute "subscription_dynamic_field"
     */
    public function testPropertySubscriptionDynamicField()
    {
    }

    /**
     * Test attribute "string_value"
     */
    public function testPropertyStringValue()
    {
    }

    /**
     * Test attribute "bool_value"
     */
    public function testPropertyBoolValue()
    {
    }

    /**
     * Test attribute "date_value"
     */
    public function testPropertyDateValue()
    {
    }

    /**
     * Test attribute "numeric_value"
     */
    public function testPropertyNumericValue()
    {
    }
}
