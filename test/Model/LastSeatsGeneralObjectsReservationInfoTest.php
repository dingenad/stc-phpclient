<?php
/**
 * LastSeatsGeneralObjectsReservationInfoTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
* STC.Tickets
 *
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
* OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.4
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * LastSeatsGeneralObjectsReservationInfoTest Class Doc Comment
 *
 * @category    Class
 * @description LastSeatsGeneralObjectsReservationInfo
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class LastSeatsGeneralObjectsReservationInfoTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "LastSeatsGeneralObjectsReservationInfo"
     */
    public function testLastSeatsGeneralObjectsReservationInfo()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "reservation_key"
     */
    public function testPropertyReservationKey()
    {
    }

    /**
     * Test attribute "basket_id"
     */
    public function testPropertyBasketId()
    {
    }

    /**
     * Test attribute "basket_key"
     */
    public function testPropertyBasketKey()
    {
    }

    /**
     * Test attribute "language_id"
     */
    public function testPropertyLanguageId()
    {
    }

    /**
     * Test attribute "sales_channel_id"
     */
    public function testPropertySalesChannelId()
    {
    }

    /**
     * Test attribute "reservation_number"
     */
    public function testPropertyReservationNumber()
    {
    }

    /**
     * Test attribute "reservation_status_id"
     */
    public function testPropertyReservationStatusId()
    {
    }

    /**
     * Test attribute "reservation_confirmed"
     */
    public function testPropertyReservationConfirmed()
    {
    }

    /**
     * Test attribute "reservation_method_id"
     */
    public function testPropertyReservationMethodId()
    {
    }

    /**
     * Test attribute "performance_key"
     */
    public function testPropertyPerformanceKey()
    {
    }

    /**
     * Test attribute "performance_id"
     */
    public function testPropertyPerformanceId()
    {
    }

    /**
     * Test attribute "performance_section_key"
     */
    public function testPropertyPerformanceSectionKey()
    {
    }

    /**
     * Test attribute "performance_section_id"
     */
    public function testPropertyPerformanceSectionId()
    {
    }

    /**
     * Test attribute "nr_of_seats"
     */
    public function testPropertyNrOfSeats()
    {
    }

    /**
     * Test attribute "selected_seats"
     */
    public function testPropertySelectedSeats()
    {
    }

    /**
     * Test attribute "currency_id"
     */
    public function testPropertyCurrencyId()
    {
    }

    /**
     * Test attribute "total_amount"
     */
    public function testPropertyTotalAmount()
    {
    }

    /**
     * Test attribute "discount"
     */
    public function testPropertyDiscount()
    {
    }

    /**
     * Test attribute "discount_code"
     */
    public function testPropertyDiscountCode()
    {
    }

    /**
     * Test attribute "discount_title"
     */
    public function testPropertyDiscountTitle()
    {
    }

    /**
     * Test attribute "external_reservation_number"
     */
    public function testPropertyExternalReservationNumber()
    {
    }

    /**
     * Test attribute "contact_id"
     */
    public function testPropertyContactId()
    {
    }

    /**
     * Test attribute "contact_key"
     */
    public function testPropertyContactKey()
    {
    }

    /**
     * Test attribute "payment_method"
     */
    public function testPropertyPaymentMethod()
    {
    }

    /**
     * Test attribute "is_paid"
     */
    public function testPropertyIsPaid()
    {
    }

    /**
     * Test attribute "reservation_costs_per_ticket"
     */
    public function testPropertyReservationCostsPerTicket()
    {
    }

    /**
     * Test attribute "reservation_costs_per_transaction"
     */
    public function testPropertyReservationCostsPerTransaction()
    {
    }

    /**
     * Test attribute "payment_costs"
     */
    public function testPropertyPaymentCosts()
    {
    }

    /**
     * Test attribute "refund_costs_contact"
     */
    public function testPropertyRefundCostsContact()
    {
    }

    /**
     * Test attribute "refund_costs_partner"
     */
    public function testPropertyRefundCostsPartner()
    {
    }

    /**
     * Test attribute "reservation_created"
     */
    public function testPropertyReservationCreated()
    {
    }

    /**
     * Test attribute "reservation_cancelled"
     */
    public function testPropertyReservationCancelled()
    {
    }

    /**
     * Test attribute "reservation_blocked"
     */
    public function testPropertyReservationBlocked()
    {
    }

    /**
     * Test attribute "reservation_expires"
     */
    public function testPropertyReservationExpires()
    {
    }

    /**
     * Test attribute "reservation_partner_id"
     */
    public function testPropertyReservationPartnerId()
    {
    }

    /**
     * Test attribute "reseller_partner_id"
     */
    public function testPropertyResellerPartnerId()
    {
    }

    /**
     * Test attribute "reservation_contact_id"
     */
    public function testPropertyReservationContactId()
    {
    }

    /**
     * Test attribute "ip_address"
     */
    public function testPropertyIpAddress()
    {
    }

    /**
     * Test attribute "extra_info1"
     */
    public function testPropertyExtraInfo1()
    {
    }

    /**
     * Test attribute "extra_info2"
     */
    public function testPropertyExtraInfo2()
    {
    }

    /**
     * Test attribute "extra_info3"
     */
    public function testPropertyExtraInfo3()
    {
    }

    /**
     * Test attribute "different_confirmation_mail"
     */
    public function testPropertyDifferentConfirmationMail()
    {
    }

    /**
     * Test attribute "different_confirmation_mail_subject"
     */
    public function testPropertyDifferentConfirmationMailSubject()
    {
    }

    /**
     * Test attribute "external_transaction_id"
     */
    public function testPropertyExternalTransactionId()
    {
    }

    /**
     * Test attribute "affiliate"
     */
    public function testPropertyAffiliate()
    {
    }

    /**
     * Test attribute "credits_used"
     */
    public function testPropertyCreditsUsed()
    {
    }

    /**
     * Test attribute "codes_claimed"
     */
    public function testPropertyCodesClaimed()
    {
    }

    /**
     * Test attribute "last_updated_by"
     */
    public function testPropertyLastUpdatedBy()
    {
    }

    /**
     * Test attribute "last_updated_on"
     */
    public function testPropertyLastUpdatedOn()
    {
    }

    /**
     * Test attribute "basket_number"
     */
    public function testPropertyBasketNumber()
    {
    }

    /**
     * Test attribute "event_key"
     */
    public function testPropertyEventKey()
    {
    }

    /**
     * Test attribute "event_id"
     */
    public function testPropertyEventId()
    {
    }

    /**
     * Test attribute "partner_id"
     */
    public function testPropertyPartnerId()
    {
    }

    /**
     * Test attribute "allotments"
     */
    public function testPropertyAllotments()
    {
    }

    /**
     * Test attribute "ticket_codes"
     */
    public function testPropertyTicketCodes()
    {
    }

    /**
     * Test attribute "ticket_reservation_i_ds"
     */
    public function testPropertyTicketReservationIDs()
    {
    }

    /**
     * Test attribute "reservation_price_type_info_list"
     */
    public function testPropertyReservationPriceTypeInfoList()
    {
    }

    /**
     * Test attribute "reservation_subscription_list"
     */
    public function testPropertyReservationSubscriptionList()
    {
    }

    /**
     * Test attribute "visit_date"
     */
    public function testPropertyVisitDate()
    {
    }

    /**
     * Test attribute "continuation_link"
     */
    public function testPropertyContinuationLink()
    {
    }

    /**
     * Test attribute "receive_invoice_on_confirm"
     */
    public function testPropertyReceiveInvoiceOnConfirm()
    {
    }

    /**
     * Test attribute "discount_codes"
     */
    public function testPropertyDiscountCodes()
    {
    }

    /**
     * Test attribute "has_partial_cancellations"
     */
    public function testPropertyHasPartialCancellations()
    {
    }
}
